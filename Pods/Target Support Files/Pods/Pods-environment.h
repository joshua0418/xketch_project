
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CDRTranslucentSideBar
#define COCOAPODS_POD_AVAILABLE_CDRTranslucentSideBar
#define COCOAPODS_VERSION_MAJOR_CDRTranslucentSideBar 1
#define COCOAPODS_VERSION_MINOR_CDRTranslucentSideBar 0
#define COCOAPODS_VERSION_PATCH_CDRTranslucentSideBar 2

// WSCoachMarksView
#define COCOAPODS_POD_AVAILABLE_WSCoachMarksView
#define COCOAPODS_VERSION_MAJOR_WSCoachMarksView 0
#define COCOAPODS_VERSION_MINOR_WSCoachMarksView 2
#define COCOAPODS_VERSION_PATCH_WSCoachMarksView 0

// ZLSwipeableView
#define COCOAPODS_POD_AVAILABLE_ZLSwipeableView
#define COCOAPODS_VERSION_MAJOR_ZLSwipeableView 0
#define COCOAPODS_VERSION_MINOR_ZLSwipeableView 0
#define COCOAPODS_VERSION_PATCH_ZLSwipeableView 6

