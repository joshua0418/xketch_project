//
//  ViewController.swift
//  Xketch
//
//  Created by JoshuaChang on 2015/10/5.
//  Copyright © 2015年 Toby Hsu. All rights reserved.
//

import Foundation

import UIKit

class ViewController: UIViewController {
    let kKeychainItemName = "Xketch"
    let kClientID = "1086520788823-s26r8vqhkcuhm9h06gnl91hgqs91t0d5.apps.googleusercontent.com"
    let kClientSecret = "9fyqc06soFtflUNn1uYKcP7B"
    let scopes = [kGTLAuthScopeDrive]
    let service = GTLServiceDrive()

    var accesstoken:String = ""
    var refreshtoken:String = ""
    var id:String = ""
    var save = ""
    var newfile:GTLDriveFile = GTLDriveFile()
    var saveauth:GTMOAuth2Authentication = GTMOAuth2Authentication()
    
    

    
    let output = UITextView()
    var credentials: GTMOAuth2Authentication = GTMOAuth2Authentication()
    
    // When the view loads, create necessary subviews
    // and initialize the Drive API service
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(
            kKeychainItemName,
            clientID: kClientID,
            clientSecret: kClientSecret
        )
        
        
        self.service.authorizer = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(
            kKeychainItemName,
            clientID: kClientID,
            clientSecret: kClientSecret
        )
    }
    
    // When the view appears, ensure that the Drive API service is authorized
    // and perform API calls
    override func viewDidAppear(animated: Bool) {
//        if let authorizer = service.authorizer,
//            canAuth = authorizer.canAuthorize where canAuth {
//                loginalert()
//        }
        
        if(isAuthorize()){
            print("go to login alert")
            loginalert()
        }
        else {
            presentViewController(createAuthController(), animated: true, completion: nil)
        }
    }

    // Construct a query to get names and IDs of 10 files using the Google Drive API
    func updatefile(newstring: Array<Array<String>>) {
        self.service.authorizer = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(
            kKeychainItemName,
            clientID: kClientID,
            clientSecret: kClientSecret
        )

        if(isAuthorize()) {
            print("fetching files that string is \(newstring)")
            
            let count = newstring.count
            
            for(var i=0; i<count; i++){
                let content_new:String = newstring[i][0] + "," + newstring[i][1] + "\n"
                save = save + content_new
            }
            
            //print("save content is \(save)")
            let search:NSString = "title = 'xketch'"
            let query = GTLQueryDrive.queryForFilesList()
            query.q = search as String
            query.maxResults = 1;
            service.executeQuery(
                query,
                delegate: self,
                didFinishSelector: "displayResultWithTicket:finishedWithObject:error:"
            )
        }
        
        else {
            print("error")
        }
        
    }

    
    
    // Parse results and display
    func displayResultWithTicket(ticket : GTLServiceTicket,
        finishedWithObject files : GTLDriveFileList,
        error : NSError?) {
            if let error = error {
                showAlert("Error", message: error.localizedDescription)
                return
            }
            
            var filesid = ""
            var downloadurl:AnyObject = ""
            var searchfile:GTLDriveFile = GTLDriveFile()
            
            
            if let items = files.items() where !items.isEmpty {
                for file in items as! [GTLDriveFile] {
                    filesid = file.identifier
                    let link:NSMutableDictionary = file.exportLinks.JSON
                    downloadurl = link["text/csv"]!
                    searchfile = file
                }
            } else {
                filesid = "No files found."
            }
            
            
            
            var fetcher:GTMHTTPFetcher = service.fetcherService.fetcherWithURLString(downloadurl as! String)
            
            var searchdata: NSData = NSData()
            fetcher.beginFetchWithCompletionHandler(){
                (olddata:NSData!, error:NSError!) in
                if(error == nil) {
                    let oldString = NSString(data:olddata, encoding: NSUTF8StringEncoding) as! String
                    //print("olddata is \(oldString) and newdata is \(self.save)")
                    let newcontent:String = oldString + "\n\n" + self.save + "\n\n"
                    let mimeType:String = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    let data = newcontent.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
                    let uploadParameters = GTLUploadParameters(data: data!, MIMEType: mimeType)
                    
                    let query = GTLQueryDrive.queryForFilesUpdateWithObject(searchfile, fileId: filesid, uploadParameters: uploadParameters)
                    query.convert=true
                    
                    let serviceTicket = self.service.executeQuery(query, completionHandler: {(ticket, file, error) -> Void in
                        
                        if(error == nil){
                            print("update2 complete")
                        }
                            
                        else {
                            NSLog("An error occurred: %@", error)
                        }
                        
                    })
                    
                    serviceTicket.uploadProgressBlock = {(ticket, written, total) in
                        print("making updating progress")
                    }
                }
                
                else {
                    print("error is \(error)")
                }
            }
            
        
            
    }
    
    
    func isAuthorize() -> Bool{
        return (self.service.authorizer).canAuthorize!
    }
    
    // Creates the auth controller for authorizing access to Drive API
    private func createAuthController() -> GTMOAuth2ViewControllerTouch {
        NSLog("creating authortize")
        let scopeString = scopes.joinWithSeparator(" ")
        return GTMOAuth2ViewControllerTouch(
            scope: scopeString,
            clientID: kClientID,
            clientSecret: kClientSecret,
            keychainItemName: kKeychainItemName,
            delegate: self,
            finishedSelector: "viewController:finishedWithAuth:error:"
        )
    }
    
    // Handle completion of the authorization process, and update the Drive API
    // with the new credentials.
    func viewController(vc : UIViewController,finishedWithAuth authResult : GTMOAuth2Authentication, error : NSError?) {
            print("viewController!!")
            if let error = error {
                service.authorizer = nil
                showAlert("Authentication Error", message: error.localizedDescription)
                return
            }
        
            service.authorizer = authResult
            credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(
                kKeychainItemName,
                clientID: kClientID,
                clientSecret: kClientSecret
            )
            accesstoken = authResult.accessToken
            refreshtoken = authResult.refreshToken
        
            saveauth = authResult

            createFile()
            dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertView(
            title: title,
            message: message,
            delegate: nil,
            cancelButtonTitle: "OK"
        )
        alert.show()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let x:String = "Hello User"
    
    func loginalert() {
        //createFile()
        showAlert("success", message: x)
        toHomeView()
    }

    func createFile() {
        print("creating file!!")
        let title:String = "xketch"
        let content:String = "hello user from Xketch\n"
        let mimeType:String = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        
        let metadata = GTLDriveFile()
        metadata.title = title
        

        
        let data = content.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        let uploadParameters = GTLUploadParameters(data: data!, MIMEType: mimeType)
        let query = GTLQueryDrive.queryForFilesInsertWithObject(metadata, uploadParameters: uploadParameters)
        
        query.convert=true
        
        
        let serviceTicket = service.executeQuery(query, completionHandler: {(ticket, file, error) -> Void in
            let myFile = file as! GTLDriveFile
            
            if(error == nil){
                self.id = myFile.identifier
                print("complete")
            }
                
            else {
                NSLog("An error occurred: %@", error)
            }
            
        })
        
        serviceTicket.uploadProgressBlock = {(ticket, written, total) in
            print("making progress")
        }
        
    }

    func toHomeView() {
        performSegueWithIdentifier("LoginViewToHome", sender: self)
        NSLog("change page")
    }
    
}
