//
//  ProjectCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/7.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class ProjectCell: UICollectionViewCell {
    
    @IBOutlet var highlightView: UIView!
    @IBOutlet var projectPreview: UIImageView!
    @IBOutlet var projectIcon: UIImageView!
    @IBOutlet var projectName: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.95
        self.layer.shadowColor = UIColor.blackColor().CGColor
        self.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 570, height: 481)).CGPath        
    }
}
