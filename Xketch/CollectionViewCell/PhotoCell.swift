//
//  PhotoCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/2/7.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    var photo: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        photo = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: frame.size))
        photo.contentMode = UIViewContentMode.ScaleAspectFill
        photo.clipsToBounds = true
        
        if #available(iOS 8.0, *) {
            let blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
            effectView.frame = photo.frame
            effectView.hidden = true
            photo.addSubview(effectView)
        } else {
            // Fallback on earlier versions
        }
        self.contentView.addSubview(photo)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
