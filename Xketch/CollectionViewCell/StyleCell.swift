//
//  StyleCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/29.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class StyleCell: UICollectionViewCell {

    @IBOutlet var styleItem: UIButton!
    @IBOutlet var animateImage: UIImageView!
    @IBOutlet var descLabel: UILabel!
    
    
}
