//
//  EditorViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/11/20.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

class EditorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, CDRTranslucentSideBarDelegate, GMImagePickerControllerDelegate, UIPopoverControllerDelegate, XKViewProtocol, ZoomAnimatorProtocol, WSCoachMarksViewDelegate {
    
    @IBOutlet var rightSegmentControl: UISegmentedControl!
    @IBOutlet var menuView: UIView!
    @IBOutlet var borderView: UIImageView!
    @IBOutlet var widgetTableView: UITableView!
    @IBOutlet var typeTableView: UITableView!
    @IBOutlet var actionTableView: UIView!
    @IBOutlet var propertyTableView: UIView!
    @IBOutlet var containerView: ScreenView!
    @IBOutlet var gestureView: GestureView!
    @IBOutlet var modeControl: UISegmentedControl!
    @IBOutlet var prevButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var strokeImage: UIImageView!
    @IBOutlet var undoButton: UIButton!
    @IBOutlet var redoButton: UIButton!
    
    @IBOutlet var copyButton: UIButton!
    @IBOutlet var pasteButton: UIButton!
    @IBOutlet var cutButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var duplicateButton: UIButton!
    
    @IBOutlet var toolsView: UIView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var dollarPGestureRecognizer:DollarPGestureRecognizer!
    var currentProject: PROJECT!
    var currentPage: PAGE!
    var propertyVC: PropertyTableViewController?
    var actionVC: ActionTableViewController?
    var pickerDataSource: Array<Dictionary<String, AnyObject>> = [[:]]
    var tableCellStyle: Int = 0
//    var tableViewDataSource: Array<String> = []
    
    var stagingWidgets: Array<Dictionary<String, AnyObject>> = []
    
    var animatorManager: ZoomAnimator?

    override func viewDidLoad() {
        super.viewDidLoad()
        let manager = NSUndoManager()
        manager.levelsOfUndo = 10
        CoreDataUtil.sharedInstance.managedObjectContext!.undoManager = manager
        
        dollarPGestureRecognizer = DollarPGestureRecognizer(target: self, action: Selector("gestureRecognized:"))
        dollarPGestureRecognizer.delaysTouchesEnded=false
        dollarPGestureRecognizer.pointClouds = DollarDefaultGestures.defaultPointClouds()
        
        gestureView.addGestureRecognizer(dollarPGestureRecognizer)
        gestureView.containerView = self.containerView
        gestureView.delegate = self
        
        widgetTableView.editing = true
        widgetTableView.lastIndex = -1
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "popOverImagePicker", name: "popOverImagePicker", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshTable", name: "refreshTable", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updatePickerDataSource:", name: "updatePickerDataSource", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateTableViewDataSource:", name: "updateTableViewDataSource", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateUndoState", name: "updateUndoState", object: nil)
        
        var pages = currentProject.pages.allObjects as! [PAGE]
        pages.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        currentPage = pages[0]

        WidgetUtil.sharedInstance.currentPage = currentPage
        ActionUtil.sharedInstance.controller = self
        
        propertyVC = self.childViewControllers[0].childViewControllers[0] as? PropertyTableViewController
        actionVC = self.childViewControllers[1].childViewControllers[0] as? ActionTableViewController
        actionVC?.delegate = self
        actionVC?.datasource = self
        
        self.loadWidgetsInPage()
        
        if let navigationController = self.navigationController {
            animatorManager = ZoomAnimator(navigationController: navigationController)
        }
        self.navigationController?.delegate = animatorManager
        
        if currentProject.name == "Tutorial" {
            prevButton.hidden = false
            nextButton.hidden = false
            refreshTutorial()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        viewDidLayoutSubviews()
    }
    
    func refreshTutorial() {
        
        if NSUserDefaults.standardUserDefaults().integerForKey("TaskCount") < 4 {
            for view in self.containerView.subviews as! [XKView] {
                CoreDataUtil.sharedInstance.deleteCDObject(view)
                view.removeFromSuperview()
            }
            refreshTable()
        }
        
        switch NSUserDefaults.standardUserDefaults().integerForKey("TaskCount") {
        case 0:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“Line” is the first stroke you can use.\n You can draw on specific region to generate widget."],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 44)), "caption": "Draw on the “Top” to generate Naivgation Bar"],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 622, width: 259, height: 44)), "caption": "or “Bottom” to generate Tab Bar"],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 267, width: 259, height: 355)), "caption": "Other region can generate some widgets\n e.g. Label(Default), Button, TextField, Horizontal-Line"],
                ["rect": NSValue(CGRect: CGRect(x: 918, y: 64, width: 106, height: 29)), "caption": "Click here."],
                ["rect": NSValue(CGRect: CGRect(x: 704, y: 93, width: 320, height: 575)), "caption": "You can change widget's type here."],
                ["rect": NSValue(CGRect: CGRect(x: 625, y: 502, width: 50, height: 50)), "caption": "Try it!\nIf you want to next tutorial, please press the 'Next' button."]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        case 1:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“Rectangle” is the second stroke you can use."],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 443)), "caption": "You can generate some widgets\n e.g. Canvas(Default), TextView, Image"],
                ["rect": NSValue(CGRect: CGRect(x: 704, y: 64, width: 106, height: 29)), "caption": "Click here."],
                ["rect": NSValue(CGRect: CGRect(x: 704, y: 93, width: 320, height: 575)), "caption": "You can edit widget's property here."]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        case 2:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“Cross” is the third stroke you can use."],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 443)), "caption": "You can generate some widgets\n e.g. Segment Control(Default), Switch, Slider"]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        case 3:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 443)), "caption": "It's very easy to use, right?\nNext, we want to introduce “Expert Mode”"],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 443)), "caption": "You can generate exactly widget and don't need to choose type."],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“Image”"],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“Slider”"],
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 443)), "caption": "We also have some advanced widget.\n"],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“TableView”"],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“WebView”"],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“MapView”"],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“PickerView”"],
                ["rect": NSValue(CGRect: CGRect(x: 625, y: 502, width: 50, height: 50)), "caption": "Try it, next tutorial we want to show you some features."]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        case 4:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 263, y: 64, width: 374, height: 50)), "caption": "When you generate a widget, you can find these operation."],
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "Almost widget you can drag directly by one touch."],
                ["rect": NSValue(CGRect: CGRect(x: 100, y: 26, width: 32, height: 32)), "caption": "Click here to see all you own pages, and create a new page by “＋”."],
                ["rect": NSValue(CGRect: CGRect(x: 625, y: 502, width: 50, height: 50)), "caption": "Try to create a new page, next tutorial we will show you how to connect to each page."]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        case 5:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 321, y: 223, width: 259, height: 443)), "caption": "Please generate a button first."],
                ["rect": NSValue(CGRect: CGRect(x: 810, y: 64, width: 106, height: 29)), "caption": "Click here."],
                ["rect": NSValue(CGRect: CGRect(x: 704, y: 93, width: 320, height: 575)), "caption": "You can find some touch event, just to choose target page."],
                ["rect": NSValue(CGRect: CGRect(x: 704, y: 93, width: 320, height: 575)), "caption": "And you will see the “Animation” option, try to choose transition style."],
                ["rect": NSValue(CGRect: CGRect(x: 882, y: 27, width: 122, height: 29)), "caption": "Now, you can switch “TEST” mode to try your programming."],
                ["rect": NSValue(CGRect: CGRect(x: 625, y: 502, width: 50, height: 50)), "caption": "The last one, ”Storyboard Mode“."]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        case 6:
            let coachMarks: Array<Dictionary<String, AnyObject>> = [
                ["rect": NSValue(CGRect: CGRect(x: 361, y: 350, width: 180, height: 180)), "caption": "“Make a pinch gesture to “Storyboard Mode”."],
                ["rect": NSValue(CGRect: CGRect(x: 512, y: 384, width: 0, height: 0)), "caption": "Next"]
            ]
            let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
            self.navigationController!.view.addSubview(coachMarksView)
            coachMarksView.enableContinueLabel = true
            coachMarksView.maxLblWidth = 400
            coachMarksView.delegate = self
            coachMarksView.start()
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        let touch = touches.first as UITouch!
        let startPoint = touch.locationInView(self.view)
        let underView = self.view.getUnderViewByPoint(self.view, point: startPoint, subviews: self.view.subviews)
        if underView == nil {
            if let view = self.gestureView.currentWidget {
                appDelegate.insertLog(command: "UnSelect", object: view.CDObject.objectWithID(), project: currentProject, page: currentPage)
            }

            self.gestureView.currentWidget = nil
            self.propertyVC?.currentWidget = nil
            self.gestureView.clearSelectedWidgets()
            self.gestureView.currentMode = .Sketch
            refreshTable()
        }
    }
    
    override func viewDidLayoutSubviews() {
        for view in self.containerView.subviews as! [XKView] {
            for widget in view.contentView.subviews {
                switch widget {
                case is UINavigationBar:
                    (widget as! UINavigationBar).frame = CGRect(x: 0, y: 0, width: screenWidth, height: navigationBarHeight)
                    (widget as! UINavigationBar).setTitleVerticalPositionAdjustment(5.0, forBarMetrics: .Default)
                    let bar = (widget as! UINavigationBar).items![0] as UINavigationItem
                    bar.rightBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 5), forBarMetrics: .Default)
                    bar.leftBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 5), forBarMetrics: .Default)
                case is UITabBar:
                    (widget as! UITabBar).frame = CGRect(x: 0, y: 0, width: screenWidth, height: tabBarHeight)
                case is UISlider:
                    (widget as! UISlider).frame = CGRect(x: 0, y: 0, width: widget.frame.size.width, height: sliderHeight)
                case is UISwitch:
                    (widget as! UISwitch).frame = CGRect(x: 0, y: 0, width: widget.frame.size.width, height: switchHeight)
                default:
                    break
                }
            }
        }
    }
    
    
    // MARK: - XKViewProtocol
    // MARK: DataSourcce
    
    func currentWidget() -> XKView? {
        return self.gestureView.currentWidget
    }
    
    func allPAGEs() -> Array<PAGE> {
        var pages = currentProject.pages.allObjects as! [PAGE]
        pages.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        return pages
    }
    
    func _currentPage() -> PAGE? {
        return self.currentPage
    }
    
    // MARK: Delegate
    
    func changePage(number number: Int) {
        var pages = currentProject.pages.allObjects as! [PAGE]
        pages.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        currentPage = pages[number] as PAGE
        WidgetUtil.sharedInstance.currentPage = currentPage
        print("currentPage:\(WidgetUtil.sharedInstance.currentPage.name)")
        self.gestureView.currentWidget = nil
        self.propertyVC?.currentWidget = nil
        self.gestureView.clearSelectedWidgets()
        self.gestureView.currentMode = .Sketch
        self.loadWidgetsInPage()
    }
    
    func updatePreviewImage(page: PAGE) {
        let image = UIImage.imageWithView(self.containerView.superview!)
        page.preview = UIImageJPEGRepresentation(image, 1)!
        CoreDataUtil.sharedInstance.save()
    }
    
    func toStoryboardMode() {
        self.performSegueWithIdentifier("ToStoryboard", sender: nil)
    }
    
    func showMenuView() {
        
        if let _ = currentWidget() {
            copyButton.enabled = true
            cutButton.enabled = true
            deleteButton.enabled = true
            duplicateButton.enabled = true
        }
        else {
            copyButton.enabled = false
            cutButton.enabled = false
            deleteButton.enabled = false
            duplicateButton.enabled = false
        }
        pasteButton.enabled = stagingWidgets.count == 0 ? false : true
        
        self.menuView.hidden = false
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.menuView.frame.origin.y = 64
        })
    }
    
    func hideMenuView() {
        if stagingWidgets.count == 0 {
            self.menuView.hidden = true
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.menuView.frame.origin.y = 14
            })
        }
    }
    
    // MARK: - ZoomAnimatorProtocol
    
    func viewForTransition() -> UIView {
        return self.containerView
    }
    
    // MARK: - GMImagePicker Delegate
    
    func assetsPickerController(picker: GMImagePickerController!, didFinishPickingAssets assets: [AnyObject]!) {
        var count = assets.count
        var images: Array<NSData> = []
        
        if #available(iOS 8.0, *) {
            for asset in assets as! [PHAsset] {
                PHImageManager.defaultManager().requestImageDataForAsset(asset, options: PHImageRequestOptions(), resultHandler: { (imageData, dataUTI, orientation, info) -> Void in
                    images.append(imageData! as NSData)
                    count--
                    if count == 0 {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            picker.dismissViewControllerAnimated(true, completion: { () -> Void in
                                NSNotificationCenter.defaultCenter().postNotificationName("getImageFromLibrary", object: nil, userInfo: ["images":images])
                            })
                        })
                    }
                })
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    // MARK: - Customize Functions
    
    func popOverImagePicker() {
        let imagePickerVC = GMImagePickerController()
        imagePickerVC.delegate = self
        imagePickerVC.colsInLandscape = 5
        imagePickerVC.minimumInteritemSpacing = 2
        
        let popOver:UIPopoverController = UIPopoverController(contentViewController: imagePickerVC)
        popOver.delegate = self
        popOver.presentPopoverFromRect(CGRect(x: 100, y: 100, width: 824, height: 568), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection(), animated: true)
    }
    
    func gestureRecognized(sender:DollarPGestureRecognizer) {
        let result:DollarResult = sender.result
        print("count:\(result.stroke_count)")
        NSLog("%@ (%.2f)", result.name, result.score)
        switch result.name {
        case "tableview", "picker":
            result.stroke_count > 1 ? self.generateWidget(result) : print("stroke count error.")
        default:
            self.generateWidget(result)
        }
    }
    
    func generateWidget(result:DollarResult) {
        WidgetUtil.sharedInstance.result = result
        var view:XKView = XKView()
        
        switch (result.name) {
        case "line":
            if result.origin.y <= (navigationBarHeight + statusBarHeight) && result.size.width >= 162 {  // 64=52px, 200=162px
                if !self.containerView.isExistWidget("Navigation") {
                    view = WidgetUtil.sharedInstance.generateNavigationBar()
                    rightSegmentControl.selectedSegmentIndex = 2
                }
            }
            else if result.origin.y >= (tabBarOriginY - statusBarHeight) && result.size.width >= 162 {
                if !self.containerView.isExistWidget("Tab") {
                    view = WidgetUtil.sharedInstance.generateTabBar(3)
                    rightSegmentControl.selectedSegmentIndex = 0
                }
            }
            else {
                let belowWidget = self.containerView.getBelowWidget(result)
                if (belowWidget != nil) {
                    if belowWidget?.widgetName == "Navigation" {
                        var nav: UINavigationItem!
                        for item in belowWidget!.contentView.subviews {
                            if item is UINavigationBar {
                                nav = (item as! UINavigationBar).items![0] as UINavigationItem
                            }
                        }
                        
                        if result.origin.x < 97 && result.origin.y <= 52 { // 120=97px
                            WidgetUtil.sharedInstance.generateNavigationBarItem(nav, element:belowWidget?.CDObject as! XKNAV, side: "left")
                            appDelegate.insertLog(command: result.name, object: "NavItem(Left))", project: currentProject, page: currentPage)
                        }
                        else if result.origin.x > 162 && result.origin.y <= 52 {
                            WidgetUtil.sharedInstance.generateNavigationBarItem(nav, element:belowWidget?.CDObject as! XKNAV,side: "right")
                            appDelegate.insertLog(command: result.name, object: "NavItem(Right))", project: currentProject, page: currentPage)
                        }
                        self.viewDidLayoutSubviews()
                        rightSegmentControl.selectedSegmentIndex = 0

                        // refocus in navigation bar
                        belowWidget?.isResize == true ? belowWidget?.showResize() : belowWidget?.hideResize()
                        belowWidget?.showSelection()
                        self.gestureView.clearSelectedWidgets()
                        self.gestureView.selectedWidgets.append(belowWidget!)
                        self.gestureView.currentWidget = belowWidget!
                        self.gestureView.currentMode = .Selection
                        showMenuView()
                        refreshTable()
                    }
                    else {
                        view = WidgetUtil.sharedInstance.generateLabel()
                        rightSegmentControl.selectedSegmentIndex = 2
                    }
                }
                else {
                    view = WidgetUtil.sharedInstance.generateLabel()
                    rightSegmentControl.selectedSegmentIndex = 2
                }
            }
            NSLog("line")
        case "rect":
            view = WidgetUtil.sharedInstance.generateView()
            rightSegmentControl.selectedSegmentIndex = 2
        case "image":
            view = WidgetUtil.sharedInstance.generateImage()
            rightSegmentControl.selectedSegmentIndex = 0
        case "cross":
            view = WidgetUtil.sharedInstance.generateSegment(2)
            rightSegmentControl.selectedSegmentIndex = 2
        case "slider":
            view = WidgetUtil.sharedInstance.generateSlider()
            rightSegmentControl.selectedSegmentIndex = 0
        case "textview":
            view = WidgetUtil.sharedInstance.generateTextView()
            rightSegmentControl.selectedSegmentIndex = 0
        case "tableview":
            view = WidgetUtil.sharedInstance.generateTableView(self, Frame: self.containerView.getFullScreenRect())
            rightSegmentControl.selectedSegmentIndex = 0
        case "mapview":
            view = WidgetUtil.sharedInstance.generateMapView(self.containerView.getFullScreenRect())
            rightSegmentControl.selectedSegmentIndex = 0
        case "webview":
            view = WidgetUtil.sharedInstance.generateWebView(self.containerView.getFullScreenRect())
            rightSegmentControl.selectedSegmentIndex = 0
        case "picker":
            view = WidgetUtil.sharedInstance.generatePickerView(self)
            rightSegmentControl.selectedSegmentIndex = 0
        default:
            NSLog("?")
        }
        
        if view.widgetName != nil {
            appDelegate.insertLog(command: result.name, object: (view.CDObject as! NATIVE_WIDGET).name, project: currentProject, page: currentPage)
            self.containerView.addSubview(view)
            self.updateScreen(view, z_index: self.containerView.subviews.count - 1)
            if view.widgetName == "Tab" || view.widgetName == "Navigation" {
                self.viewDidLayoutSubviews()
            }
        }
        switch rightSegmentControl.selectedSegmentIndex {
        case 0:
            showPropertyTableView()
        case 1:
            showActionTableView()
        case 2:
            showTypeTableView()
        default:
            print("", terminator: "")
        }
    }
    
    func loadWidgetsInPage() {
        self.title = "\(currentProject.name) - \(currentPage.name)"
        
        for view in self.containerView.subviews {
            view.removeFromSuperview()
        }
        var widgets = self.currentPage.widgets.allObjects as! [NATIVE_WIDGET]
        widgets = widgets.filter({!($0 is XKTAB_ITEM) && !($0 is XKNAV_ITEM)})
        widgets.sortInPlace {$0.zIndex.integerValue < $1.zIndex.integerValue}
        
        for object in widgets {
            print("zindex: \(object.zIndex.integerValue)")
            switch object.name {
            case "Label":
                let object = object as! XKLABEL
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadLabelFromDB(object))
            case "Button":
                let object = object as! XKBUTTON
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadButtonFromDB(object))
            case "TextField":
                let object = object as! XKTEXTFIELD
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadTextFieldFromDB(object))
            case "Horizontal-Line":
                let object = object as! XKHR
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadHRFromDB(object))
            case "Slider":
                let object = object as! XKSLIDER
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadSliderFromDB(object))
            case "Switch":
                let object = object as! XKSWITCH
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadSwitchFromDB(object))
            case "Segment Control":
                let object = object as! XKSEGMENT
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadSegmentControlFromDB(object))
            case "Navigation":
                let object = object as! XKNAV
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadNavBarFromDB(object))
                viewDidLayoutSubviews()
            case "Tab":
                let object = object as! XKTAB
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadTabBarFromDB(object))
                viewDidLayoutSubviews()
            case "Image":
                let object = object as! XKIMAGEVIEW
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadImageFromDB(object, controller: propertyVC!))
                viewDidLayoutSubviews()
            case "TextView":
                let object = object as! XKTEXTVIEW
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadTextViewFromDB(object))
            case "Canvas", "View":
                let object = object as! XKCANVAS
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                let view = WidgetUtil.sharedInstance.loadCanvasFromDB(object)
                self.containerView.addSubview(view)
//                self.containerView.insertSubview(view, atIndex: object.zIndex.integerValue)
//                let canvas = view.contentView.subviews[0] as! CanvasView
//                canvas.datasource = self
            case "tableview":
                let object = object as! XKTABLEVIEW
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadTableViewFromDB(object, controller: self))
            case "picker":
                let object = object as! XKPICKERVIEW
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadPickerViewFromDB(object, controller: self))
            case "mapview":
                let object = object as! XKMAPVIEW
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadMapViewFromDB(object))
            case "webview":
                let object = object as! XKWEBVIEW
                WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
                self.containerView.addSubview(WidgetUtil.sharedInstance.loadWebViewFromDB(object))
            default:
                print("GG")
            }
            (self.containerView.subviews.last! as UIView).layer.zPosition = CGFloat(object.zIndex.doubleValue)
        }
        refreshTable()
    }
    
    func updateScreen(view: XKView, z_index:Int) {
        resortWidgetPosition()
        self.gestureView.clearSelectedWidgets()
        self.gestureView.selectedWidgets.append(view)
        self.gestureView.currentWidget = view
        self.gestureView.currentMode = .Selection
        appDelegate.insertLog(command: "Select", object: view.CDObject.objectWithID(), project: currentProject, page: currentPage)
        view.showSelection()
        showMenuView()
        
        view.layer.zPosition = CGFloat(z_index)
        refreshTable()
    }
    
    func refreshTable() {
        updateCurrentWidget()
        if self.gestureView.selectedWidgets.count > 0 {
            self.widgetTableView.lastIndex = Int(self.gestureView.selectedWidgets[0].layer.zPosition)
        }
        else {
            self.widgetTableView.lastIndex = -1
        }
        self.widgetTableView.reloadData()
        self.typeTableView.reloadData()
        self.actionVC?.tableView.reloadData()
        self.viewDidLayoutSubviews()
    }
    
    func updateCurrentWidget() {
        if self.gestureView.currentWidget == nil {
            self.propertyVC?.navigationController?.popViewControllerAnimated(true)
            self.propertyVC?.navBar.title = "Property"
            self.propertyVC?.currentWidget = nil
        }
        else {
            self.propertyVC?.navBar.title = self.gestureView.currentWidget?.widgetName
            self.propertyVC?.currentWidget = self.gestureView.currentWidget
        }
        NSNotificationCenter.defaultCenter().postNotificationName("reloadAction", object: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("reloadProperty", object: nil)
    }
    
    func updatePickerDataSource(notif: NSNotification) {
        let entity = notif.userInfo!["entity"] as! XKPICKERVIEW
        pickerDataSource = entity.dataSource.allObjects as! [Dictionary]
        pickerDataSource.sortInPlace {($0["index"] as! NSNumber).integerValue < ($1["index"] as! NSNumber).integerValue}
        if let picker = self.gestureView.currentWidget?.contentView.subviews[0] as? UIPickerView {
            picker.reloadAllComponents()
        }
    }
    
    func updateTableViewDataSource(notif: NSNotification) {
        if let entity = notif.userInfo!["entity"] as? XKTABLEVIEW {
            tableCellStyle = entity.cellStyle.integerValue
            let table = self.gestureView.currentWidget?.contentView.subviews[0] as! UITableView
            table.reloadData()
        }
    }
    
    func changePage(page page: PAGE, type: SegueType) {
        switch type {
        case .Push:
            self.containerView.viewPushFromRight(animationTime: 0.5)
        case .Pop:
            self.containerView.viewPushFromLeft(animationTime: 0.5)
        case .Cover:
            self.containerView.viewMoveInFromBottom(animationTime: 0.5)
        case .Flip:
            self.containerView.viewFlipFromLeft(animationTime: 0.5)
        case .Cross:
            self.containerView.viewFadeIn(animationTime: 0.5)
        case .Partial:
            self.containerView.viewCurlUp(animationTime: 0.5)
        }
        currentPage = page
        WidgetUtil.sharedInstance.currentPage = currentPage
        print("currentPage:\(WidgetUtil.sharedInstance.currentPage.name)")
        self.gestureView.currentWidget = nil
        self.propertyVC?.currentWidget = nil
        self.gestureView.clearSelectedWidgets()
        self.gestureView.currentMode = .Sketch
        self.loadWidgetsInPage()
    }
    
    func resortWidgetPosition() {
        for (index, view) in (self.containerView.subviews as! [XKView]).enumerate() {
            let entity = view.CDObject as! NATIVE_WIDGET
            entity.zIndex = index
            view.layer.zPosition = CGFloat(index)
        }
        CoreDataUtil.sharedInstance.save()
    }
    
    func updateUndoState() {
        undoButton.enabled = CoreDataUtil.sharedInstance.managedObjectContext!.undoManager!.canUndo
        redoButton.enabled = CoreDataUtil.sharedInstance.managedObjectContext!.undoManager!.canRedo
    }
    
    // MARK: - TableView
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if tableView.isEqual(self.widgetTableView) {
            cell = tableView.dequeueReusableCellWithIdentifier("widgetCell") as UITableViewCell!
            let index = self.containerView.subviews.count - indexPath.row - 1
            cell.textLabel!.text = (self.containerView.subviews[index] as! XKView).widgetName + "\(index)"
            if tableView.lastIndex != -1 && tableView.lastIndex == indexPath.row {
                tableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: false, scrollPosition: .None)
            }
        }
        else if tableView.isEqual(self.typeTableView) {
            cell = tableView.dequeueReusableCellWithIdentifier("TypeCell") as! TypeCell
            let typeName: String = self.gestureView.currentWidget!.alterableTypes[indexPath.row]
            (cell as! TypeCell).widgetName.text = typeName
            switch typeName {
            case "Label":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "label_icon")
            case "Button":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "button_icon")
            case "TextField":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "textfield_icon")
            case "Horizontal-Line":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "HR_icon")
            case "Image":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "imageview_icon")
            case "Canvas":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "canvas_icon")
            case "View":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "view_icon")
            case "TextView":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "textview_icon")
            case "Slider":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "slider_icon")
            case "Switch":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "switch_icon")
            case "Segment Control":
                (cell as! TypeCell).widgetImage.image = UIImage(named: "segment_icon")
            default:
                print("GG")
            }
        }
        else {  // Fake Data
            switch tableCellStyle {
            case 0:
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                cell.textLabel!.text = "Title"
            case 1:
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
                cell.imageView?.image = UIImage(named: "image_default")
                cell.textLabel!.text = "Title"
            case 2:
                cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
                cell.textLabel!.text = "Title"
                cell.detailTextLabel?.text = "SubTitle"
            case 3:
                cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cell")
                cell.imageView?.image = UIImage(named: "image_default")
                cell.textLabel!.text = "Title"
                cell.detailTextLabel?.text = "SubTitle"
            case 4:
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "cell")
                cell.textLabel!.text = "Title"
                cell.detailTextLabel?.text = "Detail"
            case 5:
                cell = UITableViewCell(style: UITableViewCellStyle.Value2, reuseIdentifier: "cell")
                cell.textLabel!.text = "Title"
                cell.detailTextLabel?.text = "Detail"
            default:
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            }
        }
        return cell;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.isEqual(self.widgetTableView) {
            return self.containerView.subviews.count
        }
        else if tableView.isEqual(self.typeTableView) {
            if self.gestureView.currentWidget != nil && self.gestureView.currentWidget?.alterableTypes.count != 0 {
                return self.gestureView.currentWidget!.alterableTypes.count
            }
            else {
                return 0
            }
        }
        else {  // Fake Data
            return 15
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView.isEqual(self.widgetTableView) {
            if modeControl.selectedSegmentIndex == 0 {
                let index = self.containerView.subviews.count - indexPath.row - 1
                let widget = self.containerView.subviews[index] as! XKView
                self.gestureView.clearSelectedWidgets()
                self.gestureView.selectedWidgets.append(widget)
                self.gestureView.currentWidget = widget
                
                widget.showSelection()
                widget.isResize == true ? widget.showResize() : widget.hideResize()
                showMenuView()
                updateCurrentWidget()
                tableView.lastIndex = indexPath.row
                refreshTable()
            }
        }
        else if tableView.isEqual(self.typeTableView) {
            let widget:XKView = self.gestureView.currentWidget!
            let index:CGFloat = widget.layer.zPosition
            
//            WidgetUtil.sharedInstance.result = DollarResult.initByFrame(widget.originalFrame)
            let newframe:CGRect = CGRectMake(widget.frame.origin.x + resizeIconSize/2, widget.frame.origin.y + resizeIconSize/2 , widget.frame.size.width - resizeIconSize, widget.frame.size.height - resizeIconSize)
            WidgetUtil.sharedInstance.result = DollarResult.initByFrame(newframe)
            CoreDataUtil.sharedInstance.deleteCDObject(widget)
            widget.removeFromSuperview()
            var view:XKView = XKView()
            
            switch (widget.widgetName) {
            case "Label", "Button", "TextField", "Horizontal-Line":
                switch (indexPath.row) {
                case 0:
                    view = WidgetUtil.sharedInstance.generateLabel()
                case 1:
                    view = WidgetUtil.sharedInstance.generateButton()
                case 2:
                    view = WidgetUtil.sharedInstance.generateTextField()
                case 3:
                    view = WidgetUtil.sharedInstance.generateHR()
                default:
                    view = WidgetUtil.sharedInstance.generateView()
                }
            case "Slider", "Switch", "Segment Control":
                switch (indexPath.row) {
                case 0:
                    view = WidgetUtil.sharedInstance.generateSegment(2)
                case 1:
                    view = WidgetUtil.sharedInstance.generateSwitch()
                case 2:
                    view = WidgetUtil.sharedInstance.generateSlider()
                default:
                    view = WidgetUtil.sharedInstance.generateView()
                }
            case "View", "Image", "TextView":
                switch (indexPath.row) {
//                case 0:
//                    view = WidgetUtil.sharedInstance.generateCanvas()
//                    let canvas = view.contentView.subviews[0] as! CanvasView
//                    canvas.datasource = self
                case 0:
                    view = WidgetUtil.sharedInstance.generateView()
                case 1:
                    view = WidgetUtil.sharedInstance.generateImage()
                case 2:
                    view = WidgetUtil.sharedInstance.generateTextView()
                default:
                    view = WidgetUtil.sharedInstance.generateView()
                }
            default:
                break
            }
            
            appDelegate.insertLog(command: "Replace", object: (view.CDObject as! NATIVE_WIDGET).name, project: currentProject, page: currentPage)
            self.containerView.addSubview(view)
            self.updateScreen(view, z_index: Int(index))
        }
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEqual(self.widgetTableView) {
            return UITableViewCellEditingStyle.None
        }
        return UITableViewCellEditingStyle.None
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if tableView.isEqual(self.widgetTableView) {
            return true
        }
        return false
    }
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) {
        if tableView.isEqual(self.widgetTableView) {
            let sourceIndex = self.containerView.subviews.count - sourceIndexPath.row - 1
            let destIndex = self.containerView.subviews.count - destinationIndexPath.row - 1
            let sourceView = self.containerView.subviews[sourceIndex] as! XKView
//            let destView = self.containerView.subviews[destIndex] as! XKView
//            let sourceEntity = sourceView.CDObject as! NATIVE_WIDGET
//            let destEntity = destView.CDObject as! NATIVE_WIDGET

            sourceView.removeFromSuperview()
            self.containerView.insertSubview(sourceView, atIndex: destIndex)
            resortWidgetPosition()
        }
    }
    
    // MARK: - WSCoachMarkView Delegate
    
    func coachMarksView(coachMarksView: WSCoachMarksView!, didNavigateToIndex index: Int) {
        strokeImage.hidden = true
        self.strokeImage.alpha = 0
        hideMenuView()
        
        switch NSUserDefaults.standardUserDefaults().integerForKey("TaskCount") {
        case 0:
            switch index {
            case 0:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "line")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 4:
                showTypeTableView()
                rightSegmentControl.selectedSegmentIndex = 2
            default:
                break
            }
        case 1:
            switch index {
            case 0:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "rect")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 2:
                showPropertyTableView()
                rightSegmentControl.selectedSegmentIndex = 0
            default:
                strokeImage.hidden = true
            }
        case 2:
            switch index {
            case 0:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "line_dot")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            default:
                break
            }
        case 3:
            switch index {
            case 2:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "rect_x")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 3:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "line_circle")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 5:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "T")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 6:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "W")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 7:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "M")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 8:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "p")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })

            default:
                break
            }
        case 4:
            switch index {
            case 0:
                showMenuView()
            case 1:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "finger_touch")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            default:
                break
            }
        case 5:
            switch index {
            case 1:
                showActionTableView()
                rightSegmentControl.selectedSegmentIndex = 1
            default:
                break
            }
        case 6:
            switch index {
            case 0:
                strokeImage.hidden = false
                strokeImage.image = UIImage(named: "pinch_touch")
                UIView.animateWithDuration(0.3, animations: { () -> Void in
                    self.strokeImage.alpha = 1
                })
            case 1:
                coachMarksView.removeFromSuperview()
                self.performSegueWithIdentifier("ToStoryboard", sender: nil)
            default:
                break
            }
        default:
            break
        }
    }
    
    // MARK: - PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource[component]["data"]!.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (pickerDataSource[component]["data"] as! Array)[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    // MARK: - IBAction
    
    @IBAction func handlePinchAction(sender: UIPinchGestureRecognizer) {
        animatorManager?.handlePinchAction(sender)
        switch sender.state {
        case .Began:
            appDelegate.insertLog(command: "Pinch To", object: "Storyboard", project: currentProject, page: currentPage)
            self.performSegueWithIdentifier("ToStoryboard", sender: sender)
        default:
            break
        }
    }
    
    @IBAction func ModeSwitchAction(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            self.gestureView.userInteractionEnabled = false
            self.gestureView.hidden = true
            self.gestureView.clearSelectedWidgets()
            appDelegate.insertLog(command: "Switch to", object: "Test Mode", project: currentProject, page: currentPage)
            hideEditorTableView()
            toolsView.hidden = true
        }
        else {
            self.gestureView.userInteractionEnabled = true
            self.gestureView.hidden = false
            if self.gestureView.currentWidget != nil {
                self.gestureView.currentWidget!.isResize == true ? self.gestureView.currentWidget!.showResize() : self.gestureView.currentWidget!.hideResize()
                self.gestureView.currentWidget!.showSelection()
                self.gestureView.selectedWidgets.append(self.gestureView.currentWidget!)
            }
            appDelegate.insertLog(command: "Switch to", object: "Edit Mode", project: currentProject, page: currentPage)
            rightSegmentControl.hidden = false
            widgetTableView.hidden = false
            switch rightSegmentControl.selectedSegmentIndex {
            case 0:
                showPropertyTableView()
            case 1:
                showActionTableView()
            case 2:
                showTypeTableView()
            default:
                print("GG", terminator: "")
            }
            toolsView.hidden = false
        }
    }
    
    @IBAction func RightTableSwitchAction(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            showPropertyTableView()
        case 1:
            showActionTableView()
        case 2:
            showTypeTableView()
        default:
            print("GG", terminator: "")
        }
    }
    
    @IBAction func BorderSwitchAction(sender: UIButton) {
        if self.borderView.alpha == 1.0 {
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.borderView.alpha = 0
            })
        }
        else {
            UIView.animateWithDuration(0.3, animations: { () -> Void in
                self.borderView.alpha = 1
            })
        }
    }

    @IBAction func homeAction(sender: UIButton) {
        updatePreviewImage(currentPage)
        self.navigationController!.popViewControllerAnimated(true)
        NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "TaskCount")
    }
    
    @IBAction func UndoAction(sender: UIButton) {
        CoreDataUtil.sharedInstance.undoAction()
        loadWidgetsInPage()
        
        for view in containerView.subviews as! [XKView] {
            let entity = view.CDObject as! NATIVE_WIDGET
            if entity.zIndex.integerValue == widgetTableView.lastIndex {
                let view: XKView! = containerView.subviews[widgetTableView.lastIndex] as? XKView
                appDelegate.insertLog(command: "Undo", object: (view.CDObject as! NATIVE_WIDGET).name, project: currentProject, page: currentPage)
                self.gestureView.clearSelectedWidgets()
                self.gestureView.selectedWidgets.append(view)
                self.gestureView.currentWidget = view
                self.gestureView.currentMode = .Selection
                view.isResize == true ? view.showResize() : view.hideResize()
                view.showSelection()
                showMenuView()
            }
        }
        refreshTable()
    }
    
    @IBAction func RedoAction(sender: UIButton) {
        CoreDataUtil.sharedInstance.redoAction()
        loadWidgetsInPage()
        for view in containerView.subviews as! [XKView] {
            let entity = view.CDObject as! NATIVE_WIDGET
            if entity.zIndex.integerValue == widgetTableView.lastIndex {
                let view: XKView! = containerView.subviews[widgetTableView.lastIndex] as? XKView
                appDelegate.insertLog(command: "Redo", object: (view.CDObject as! NATIVE_WIDGET).name, project: currentProject, page: currentPage)
                self.gestureView.clearSelectedWidgets()
                self.gestureView.selectedWidgets.append(view)
                self.gestureView.currentWidget = view
                self.gestureView.currentMode = .Selection
                view.isResize == true ? view.showResize() : view.hideResize()
                view.showSelection()
                showMenuView()
            }
        }
        refreshTable()
    }
    
    func showPropertyTableView() {
        propertyTableView.hidden = false
        actionTableView.hidden = true
        typeTableView.hidden = true
    }
    
    func showActionTableView() {
        propertyTableView.hidden = true
        actionTableView.hidden = false
        typeTableView.hidden = true
    }
    
    func showTypeTableView() {
        propertyTableView.hidden = true
        actionTableView.hidden = true
        typeTableView.hidden = false
    }
    
    func hideEditorTableView() {
        propertyTableView.hidden = true
        actionTableView.hidden = true
        typeTableView.hidden = true
        widgetTableView.hidden = true
        rightSegmentControl.hidden = true
    }

    @IBAction func copyAction(sender: UIButton) {
        stagingWidgets.removeAll(keepCapacity: false)
        for widget in self.gestureView.selectedWidgets {
            if let copy = WidgetUtil.sharedInstance.copyWidget(widget.CDObject as! NATIVE_WIDGET) {
                WidgetUtil.sharedInstance.isDuplicate = false
                appDelegate.insertLog(command: "Copy", object: widget.CDObject.objectWithID(), project: currentProject, page: currentPage)
                stagingWidgets.append(copy) // add to cache
                pasteButton.enabled = true
            }
        }
       
    }
    
    @IBAction func pasteAction(sender: UIButton) {
        var pasteWidget: XKView?
        for widgetDict in stagingWidgets {
            if widgetDict["name"] as! String == "Image" {
                pasteWidget = WidgetUtil.sharedInstance.pasteWidget(widgetDict, page: currentPage, controller: propertyVC)
            }
            else if widgetDict["name"] as! String == "tableview" || widgetDict["name"] as! String == "picker" {
                pasteWidget = WidgetUtil.sharedInstance.pasteWidget(widgetDict, page: currentPage, controller: self)
            }
            else {
                pasteWidget = WidgetUtil.sharedInstance.pasteWidget(widgetDict, page: currentPage, controller: nil)
            }
        }
        if let pasteWidget = pasteWidget {
            appDelegate.insertLog(command: "Paste", object: (pasteWidget.CDObject as! NATIVE_WIDGET).name, project: currentProject, page: currentPage)
            self.containerView.addSubview(pasteWidget)
            updateScreen(pasteWidget, z_index: self.containerView.subviews.count - 1)
            pasteWidget.isResize == true ? pasteWidget.showResize() : pasteWidget.hideResize()
            pasteWidget.showSelection()
            refreshTable()
        }
    }
    
    @IBAction func duplicateAction(sender: UIButton) {
        copyAction(UIButton())
        WidgetUtil.sharedInstance.isDuplicate = true
        pasteAction(UIButton())
        WidgetUtil.sharedInstance.isDuplicate = false
        stagingWidgets.removeAll(keepCapacity: false)
    }
    
    @IBAction func cutAction(sender: UIButton) {
        stagingWidgets.removeAll(keepCapacity: false)
        for widget in self.gestureView.selectedWidgets {
            if let copy = WidgetUtil.sharedInstance.copyWidget(widget.CDObject as! NATIVE_WIDGET) {
                appDelegate.insertLog(command: "Cut", object: widget.CDObject.objectWithID(), project: currentProject, page: currentPage)
                stagingWidgets.append(copy) // add to cache
                CoreDataUtil.sharedInstance.deleteCDObject(widget)
                widget.removeFromSuperview()
                pasteButton.enabled = true
            }
        }
        self.gestureView.currentWidget = nil
        self.gestureView.selectedWidgets.removeAll(keepCapacity: false)
        updateCurrentWidget()
        refreshTable()
    }

    @IBAction func deleteAction(sender: UIButton) {
        for widget in self.gestureView.selectedWidgets {
            appDelegate.insertLog(command: "Delete", object: widget.CDObject.objectWithID(), project: currentProject, page: currentPage)
            CoreDataUtil.sharedInstance.deleteCDObject(widget)
            widget.removeFromSuperview()
        }
        self.gestureView.currentWidget = nil
        self.gestureView.selectedWidgets.removeAll(keepCapacity: false)
        resortWidgetPosition()
        updateCurrentWidget()
        refreshTable()
        stagingWidgets.count == 0 ? hideMenuView() : showMenuView()
    }
    
    @IBAction func PrevTaskAction(sender: UIButton) {
        var taskcount = NSUserDefaults.standardUserDefaults().integerForKey("TaskCount")
        NSUserDefaults.standardUserDefaults().setInteger(--taskcount, forKey: "TaskCount")
        refreshTutorial()
    }
    
    @IBAction func NextTaskAction(sender: UIButton) {
        var taskcount = NSUserDefaults.standardUserDefaults().integerForKey("TaskCount")
        NSUserDefaults.standardUserDefaults().setInteger(++taskcount, forKey: "TaskCount")
        refreshTutorial()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToPagePopover" {
            self.updatePreviewImage(currentPage)
            let nav = segue.destinationViewController as! UINavigationController
            let VC = nav.viewControllers[0] as! PageTableViewController
            VC.datasource = self
            VC.delegate = self
        }
        else if segue.identifier == "ToStoryboard" {
            self.updatePreviewImage(currentPage)
            let VC = segue.destinationViewController as! StoryboardViewController
            VC.datasource = self
            VC.delegate = self
        }
    }

    
}
