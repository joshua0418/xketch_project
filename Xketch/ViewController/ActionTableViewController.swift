//
//  ActionTableViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/20.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class ActionTableViewController: UITableViewController {
    var datasource: XKViewProtocol?
    var delegate: XKViewProtocol?
    var targetPage: PAGE!

    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadAction", name: "reloadAction", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadAction() {
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        if let widget = datasource?.currentWidget() {
            let entity = widget.CDObject as! NATIVE_WIDGET
            switch widget.widgetName {
            case "Button":
                return SegueEvent.count
            case "Navigation":
                return (entity as! XKNAV).items.count > 0 ? SegueEvent.count * (entity as! XKNAV).items.count : 0
            case "Tab":
                return (entity as! XKTAB).items.count > 0 ? SegueEvent.count * (entity as! XKTAB).items.count : 0
            case "tableview":
                return SegueEvent.count
            case "Image":
                if (entity as! XKIMAGEVIEW).style.integerValue == 2 {
                    return SegueEvent.count
                }
            default:
                return 0
            }
        }
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if let widget = datasource?.currentWidget() {
            let entity = widget.CDObject as! NATIVE_WIDGET
            let action = entity.action 
            switch entity {
            case is XKBUTTON:
                return action == nil ? 1 : 2
            case is XKNAV:
                var items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
                return items[section].action == nil ? 1 : 2
            case is XKTAB:
                var items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
                return items[section].action == nil ? 1 : 2
            case is XKTABLEVIEW:
                return action == nil ? 1 : 2
            case is XKIMAGEVIEW:
                if (entity as! XKIMAGEVIEW).style.integerValue == 2 {
                return action == nil ? 1 : 2
                }
            default:
                return 0
            }
        }
        return 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCellWithIdentifier("TargetCell", forIndexPath: indexPath)
            cell!.textLabel?.text = "Target Page"
            cell!.detailTextLabel?.text = "?"
            
            if let currentWidget = datasource?.currentWidget() {
                let entity = currentWidget.CDObject as! NATIVE_WIDGET
                switch entity {
                case is XKBUTTON:
                    if let action = entity.action {
                        cell!.detailTextLabel?.text = action.targetPage.name
                        self.targetPage = action.targetPage
                    }
                case is XKNAV:
                    var items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
                    items.sortInPlace {$0.side < $1.side}
                    if let action = items[indexPath.section / SegueEvent.count].action {
                        cell!.detailTextLabel?.text = action.targetPage.name
                        self.targetPage = action.targetPage
                    }
                case is XKTAB:
                    var items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
                    items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
                    if let action = items[indexPath.section / SegueEvent.count].action {
                        cell!.detailTextLabel?.text = action.targetPage.name
                        self.targetPage = action.targetPage
                    }
                case is XKTABLEVIEW:
                    if let action = entity.action {
                        cell!.detailTextLabel?.text = action.targetPage.name
                        self.targetPage = action.targetPage
                    }
                case is XKIMAGEVIEW:
                    if (entity as! XKIMAGEVIEW).style.integerValue == 2 {
                        if let action = entity.action {
                            cell!.detailTextLabel?.text = action.targetPage.name
                            self.targetPage = action.targetPage
                        }
                    }
                default:
                    print("")
                }
                
            }
        }
        else {
            cell = tableView.dequeueReusableCellWithIdentifier("StyleSelectCell", forIndexPath: indexPath) as? StyleSelectCell
            (cell as! StyleSelectCell).attributeName.text = "Animation"
            (cell as! StyleSelectCell).attributeValue.setImage(nil, forState: .Normal)
            
            if let currentWidget = datasource?.currentWidget() {
                let entity = currentWidget.CDObject as! NATIVE_WIDGET
                switch entity {
                case is XKBUTTON:
                    if let action = entity.action {
                        (cell as! StyleSelectCell).attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
                    }
                case is XKNAV:
                    var items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
                    items.sortInPlace {$0.side < $1.side}
                    if let action = items[indexPath.section / SegueEvent.count].action {
                        (cell as! StyleSelectCell).attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
                    }
                case is XKTAB:
                    var items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
                    items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
                    if let action = items[indexPath.section / SegueEvent.count].action {
                        (cell as! StyleSelectCell).attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
                    }
                case is XKTABLEVIEW:
                    if let action = entity.action {
                        (cell as! StyleSelectCell).attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
                    }
                case is XKIMAGEVIEW:
                    if (entity as! XKIMAGEVIEW).style.integerValue == 2 {
                        if let action = entity.action {
                        (cell as! StyleSelectCell).attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
                        }
                    }
                default:
                    print("")
                }
                
            }
        }
        
        cell!.tag = indexPath.section / SegueEvent.count
        return cell!
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let widget = datasource?.currentWidget() {
            let entity = widget.CDObject as! NATIVE_WIDGET
            switch entity {
            case is XKBUTTON:
                return SegueEvent(rawValue: section)!.toString()
            case is XKNAV:
                var items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
                let itemNum = section / SegueEvent.count
                let eventIndex = section > 0 ? section % SegueEvent.count : 0
                return "\(items[itemNum].side.uppercaseString) ITEM - \(SegueEvent(rawValue: eventIndex)!.toString())"
            case is XKTAB:
                var items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
                items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
                let itemNum = section / SegueEvent.count
                let eventIndex = section > 0 ? section % SegueEvent.count : 0
                return "ITEM \(items[itemNum].text) - \(SegueEvent(rawValue: eventIndex)!.toString())"
            case is XKTABLEVIEW:
                return "Cell - \(SegueEvent(rawValue: section)!.toString())"
            case is XKIMAGEVIEW:
                if (entity as! XKIMAGEVIEW).style.integerValue == 2 {
                    return "Cell - \(SegueEvent(rawValue: section)!.toString())"
                }
            default:
                return nil
            }
        }
        return nil
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToPageSelect" {
            let VC = segue.destinationViewController as! PageSelectTableViewController
            VC.PAGEs = datasource!.allPAGEs()
            VC.currentWidget = datasource!.currentWidget()
            VC.itemNum = (sender as! UITableViewCell).tag
        }
        else if segue.identifier == "ToStyleSelect" {
            let VC = segue.destinationViewController as! StyleSelectCollectionViewController
            VC.currentWidget = datasource!.currentWidget()
            let cell = sender as! StyleSelectCell
            VC.navBar.title = cell.attributeName.text
            VC.targetPage = targetPage
            VC.column = cell.tag
            print("ToStyleSelect")
        }

    }
    

}
