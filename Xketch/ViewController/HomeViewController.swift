//
//  HomeViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/7.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {
    
    var PROJECTs:Array<PROJECT>! = []
    
    @IBOutlet var projectCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // reset debug
//        CoreDataUtil.sharedInstance.clearEntity("PROJECT")
//        NSUserDefaults.standardUserDefaults().removeObjectForKey("ProjectCount")
//        if NSUserDefaults.standardUserDefaults().integerForKey("ProjectCount") == 0 {
//            CoreDataUtil.sharedInstance.insertPROJECT("Tutorial")
//        }
//        NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "TaskCount")
        
        (projectCollectionView.collectionViewLayout as! EBCardCollectionViewLayout).offset = UIOffset(horizontal: (projectCollectionView.frame.size.width - 570) / 2, vertical: (projectCollectionView.frame.size.height - 481) / 2)
        PROJECTs = CoreDataUtil.sharedInstance.fetchEntity("PROJECT") as! [PROJECT]
        PROJECTs.sortInPlace {$0.index.integerValue > $1.index.integerValue}
        projectCollectionView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)        
        updateCellStatus()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: ProjectCell = collectionView.dequeueReusableCellWithReuseIdentifier("ProjectCell", forIndexPath: indexPath) as! ProjectCell
        cell.tag = indexPath.row
        cell.projectName.text = self.PROJECTs[indexPath.row].name
        var pages = PROJECTs[indexPath.row].pages.allObjects as! [PAGE]
        pages.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        if let page = pages[0] as PAGE? {
            if let preview = UIImage(data: page.preview) {
                cell.projectPreview.image = preview
            }
            else {
                cell.projectPreview.image = UIImage(named: "snapshot_default")
            }
        }
        cell.projectIcon.layer.shadowRadius = 2
        cell.projectIcon.layer.shadowOffset = CGSize.zero
        cell.projectIcon.layer.shadowOpacity = 0.95
        cell.projectIcon.layer.masksToBounds = false
        cell.projectIcon.layer.shadowColor = UIColor.blackColor().CGColor
        cell.projectIcon.layer.cornerRadius = 2
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.PROJECTs.count
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        updateCellStatus()
    }
    
    func updateCellStatus() {
        let index = (projectCollectionView.collectionViewLayout as! EBCardCollectionViewLayout).currentPage
        for var i=0; i<PROJECTs.count; i++ {
            let indexPath = NSIndexPath(forItem: i, inSection: 0)
            if let cell = projectCollectionView.cellForItemAtIndexPath(indexPath) as? ProjectCell {
                if i == index {
                    cell.highlightView.alpha = 0.6
                }
                else {
                    cell.highlightView.alpha = 0.3
                }
            }
            
        }
    }
    
    @IBAction func CreateProjectAction(sender: UIButton) {
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: nil, message: "Please enter the new project name.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
                textField.placeholder = "New ..."
            }
            
            let okAction = UIAlertAction(title: "Ok", style: .Destructive) { (_) in
                let sourceTextField = alertController.textFields![0]
                CoreDataUtil.sharedInstance.insertPROJECT(sourceTextField.text!)
                self.PROJECTs = CoreDataUtil.sharedInstance.fetchEntity("PROJECT") as! [PROJECT]
                self.PROJECTs.sortInPlace {$0.index.integerValue > $1.index.integerValue}
                self.projectCollectionView.insertItemsAtIndexPaths([NSIndexPath(forItem: 0, inSection: 0)])
                self.projectCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
                self.updateCellStatus()
                NSUserDefaults.standardUserDefaults().setObject([String](), forKey: "PROJECT_\(sourceTextField.text)")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            self.presentViewController(alertController, animated: true) { () -> Void in
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func DeleteProjectAction(sender: UIButton) {
        let index = (projectCollectionView.collectionViewLayout as! EBCardCollectionViewLayout).currentPage
        CoreDataUtil.sharedInstance.deletePROJECT(PROJECTs[index])
        self.PROJECTs = CoreDataUtil.sharedInstance.fetchEntity("PROJECT") as! [PROJECT]
        self.PROJECTs.sortInPlace {$0.index.integerValue > $1.index.integerValue}
        self.projectCollectionView.deleteItemsAtIndexPaths([NSIndexPath(forItem: index, inSection: 0)])
        if self.PROJECTs.count > 0 {
            self.projectCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: true)
        }
        else {
            self.projectCollectionView.reloadData()
        }
        updateCellStatus()
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToEditor" {
            let VC = segue.destinationViewController as! EditorViewController
            VC.currentProject = self.PROJECTs[sender!.tag]
        }
    }


}
