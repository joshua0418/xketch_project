//
//  PageTableViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/15.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class PageTableViewController: UITableViewController {
    
    var datasource: XKViewProtocol?
    var delegate: XKViewProtocol?
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if let pages = datasource?.allPAGEs() {
            return pages.count
        }
        return 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PageCell", forIndexPath: indexPath) 
        if let pages = datasource?.allPAGEs() {
            let page = pages[indexPath.row] 
            if let previewImage = UIImage(data: page.preview) {
                cell.imageView?.image = previewImage
            }
            else {
                cell.imageView?.image = UIImage(named: "snapshot_default")
            }
            cell.textLabel?.text = page.name
            cell.imageView?.layer.borderColor = UIColor.grayColor().CGColor
            cell.imageView?.layer.borderWidth = 1
        }

        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {        
        self.dismissViewControllerAnimated(true, completion: nil)
        if let currentPage = datasource?._currentPage() {
            delegate?.updatePreviewImage(currentPage)
        }
        delegate?.changePage(number: indexPath.row)
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return datasource?.allPAGEs().count > 1 ? true : false
    }


    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            if let pages = datasource?.allPAGEs() {
                let page = pages[indexPath.row] 
                if page == datasource?._currentPage() {
                    delegate?.changePage(number: 0)
                }
                CoreDataUtil.sharedInstance.deletePAGE(page)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            }
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    @IBAction func AddPageAction(sender: UIBarButtonItem) {
        let project: PROJECT! = self.datasource?._currentPage()?.project
        if #available(iOS 8.0, *) {
            let alertController = UIAlertController(title: nil, message: "Please enter the new page name.", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
                textField.placeholder = "New ..."
                textField.text = "Page\(project.incrementalCounter)"
            }
            let currentProject = datasource?.allPAGEs()[0].project
            
            let okAction = UIAlertAction(title: "Ok", style: .Destructive) { (_) in
                let sourceTextField = alertController.textFields![0]
                CoreDataUtil.sharedInstance.insertPAGE(sourceTextField.text!, project: currentProject!)
                project.incrementalCounter = project.incrementalCounter + 1
                self.tableView.reloadData()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            
            self.presentViewController(alertController, animated: true) { () -> Void in
            }

        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func ToStoryboardAction(sender: UIBarButtonItem) {
        appDelegate.insertLog(command: "Click To", object: "Storyboard", project: delegate!._currentPage()!.project, page: delegate!._currentPage()!)
        self.dismissViewControllerAnimated(true, completion: nil)
        self.delegate?.toStoryboardMode()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
