//
//  AnimationSelectTableViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/18.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class AnimationSelectTableViewController: UITableViewController {
    
    var actionWidgets: Array<NATIVE_WIDGET> = []
    var targetPage: PAGE!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        (self.navigationController?.parentViewController as! StoryboardViewController).reDrawConnectLine()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return actionWidgets.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 1
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: StyleSelectCell = tableView.dequeueReusableCellWithIdentifier("StyleSelectCell") as! StyleSelectCell
        cell.attributeName.text = "Animation"
        cell.attributeValue.setTitle(nil, forState: .Normal)
        cell.attributeValue.setImage(nil, forState: .Normal)
        cell.tag = indexPath.section
        
        let widget = actionWidgets[indexPath.section]
        switch widget {
        case is XKBUTTON:
            if let action = widget.action {
                cell.attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
            }
        case is XKNAV_ITEM:
            if let action = widget.action {
                cell.attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
            }
        case is XKTAB_ITEM:
            if let action = widget.action {
                cell.attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
            }
        case is XKTABLEVIEW:
            if let action = widget.action {
                cell.attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
            }
        case is XKIMAGEVIEW:
            if (widget as! XKIMAGEVIEW).style.integerValue == 2 {
                if let action = widget.action {
                    cell.attributeValue.setTitle("\(SegueType(rawValue: action.type.integerValue)!.toString())", forState: .Normal)
                }
            }
        default:
            break
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let widget = actionWidgets[section]
        switch widget {
        case is XKBUTTON:
            return widget.name
        case is XKIMAGEVIEW:
            if (widget as! XKIMAGEVIEW).style.integerValue == 2 {
                return widget.name
            }
        case is XKTABLEVIEW:
            return widget.name
        case is XKNAV_ITEM:
            return "BAR ITEM - \((widget as! XKNAV_ITEM).side.uppercaseString)"
        case is XKTAB_ITEM:
            return "TAB ITEM - \((widget as! XKTAB_ITEM).text)"
        default:
            break
        }
        return nil
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToStyleSelect" {
            let VC = segue.destinationViewController as! StyleSelectCollectionViewController
            let cell = sender as! StyleSelectCell
            var widget: XKView! = XKView()
            var itemNum: Int = 0
            let object = actionWidgets[cell.tag]
            WidgetUtil.sharedInstance.result = DollarResult.initByFrame(object.frame.CGRectValue())
            switch object.name {
            case "Button":
                let object = object as! XKBUTTON
                widget = WidgetUtil.sharedInstance.loadButtonFromDB(object)
            case "NavItem":
                itemNum = (object as! XKNAV_ITEM).side == "right" ? 1 : 0
                let object = (object as! XKNAV_ITEM).nav
                widget = WidgetUtil.sharedInstance.loadNavBarFromDB(object)
            case "TabItem":
                itemNum = (object as! XKTAB_ITEM).index.integerValue - 1
                let object = (object as! XKTAB_ITEM).tab
                widget = WidgetUtil.sharedInstance.loadTabBarFromDB(object)
            case "Image":
                let object = object as! XKIMAGEVIEW
                widget = WidgetUtil.sharedInstance.loadImageFromDB(object, controller: PropertyTableViewController())
            case "tableview":
                let object = object as! XKTABLEVIEW
                widget = WidgetUtil.sharedInstance.loadTableViewFromDB(object, controller: EditorViewController())
            default:
                print("GG")
            }
            VC.currentWidget = widget
            VC.targetPage = self.targetPage
            VC.navBar.title = cell.attributeName.text
            VC.column = itemNum
            print("ToStyleSelect")
        }
    }


}
