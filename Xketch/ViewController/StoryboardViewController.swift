//
//  StoryboardViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/12.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class StoryboardViewController: UIViewController, UIScrollViewDelegate, ZoomAnimatorProtocol, ConnectViewProtocol, WSCoachMarksViewDelegate {
    @IBOutlet var scrollView: StoryboardScrollView!
    @IBOutlet var connectView: ConnectView!
    @IBOutlet var animationSelectView: UIView!
    @IBOutlet var containerView: UIView!
    
    var datasource: XKViewProtocol?
    var delegate: XKViewProtocol?
    var animatorManager: ZoomAnimator?
    
    var sourcePage: PAGE!
    var targetPage: PAGE!
    var actionWidgets: Array<NATIVE_WIDGET> = []
    var animationSelectVC: AnimationSelectTableViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = self.navigationController {
            animatorManager = ZoomAnimator(navigationController: navigationController)
        }
        self.navigationController?.delegate = animatorManager
    
        var project: PROJECT?
        connectView.linksPoint.removeAll(keepCapacity: false)
        if let pages = datasource?.allPAGEs() {
//            containerView.frame = CGRect(x: 0, y: 0, width: ceil(CGFloat(pages.count) / 4.0) * self.view.frame.size.width, height: ceil(CGFloat(pages.count) / 4.0) * self.view.frame.size.height)
            scrollView.delegate = self
            scrollView.minimumZoomScale = 0.2
            scrollView.maximumZoomScale = 1
            scrollView.zoomScale = 1
            scrollView.contentSize = containerView.frame.size
            
            var startOrigin: CGPoint?
//            if let currentPage = datasource?._currentPage() {
//                let frame = currentPage.location.CGRectValue()
//                if frame.size.width != 0 {
//                    startOrigin = frame.origin
//                }
//                else {
////                    startOrigin = CGPoint(x: scrollView.contentSize.width / 2, y: scrollView.contentSize.height / 2)
//                    startOrigin = CGPoint(x: containerView.frame.size.width / 2, y: containerView.frame.size.height / 2)
//                }
//            }
            
            for (index, page) in pages.enumerate() {
                var frame = page.location.CGRectValue()
                if frame.size.width != 0 {
                    startOrigin = frame.origin
                }
                else {
                    //                    startOrigin = CGPoint(x: scrollView.contentSize.width / 2, y: scrollView.contentSize.height / 2)
                    startOrigin = CGPoint(x: containerView.frame.size.width / 2, y: containerView.frame.size.height / 2)
                }

                
                project = page.project
                let pageView = UIView.loadFromNibNamed("StoryboardPageView", bundle: nil) as! StoryboardPageView
                pageView.pageName.text = page.name
                if let imagedata = page.preview as NSData? {
                    pageView.previewImage.image = UIImage(data: imagedata)
                }
                else {
                    pageView.previewImage.image = UIImage(named: "snapshot_default")
                }
                pageView.tag = index
                
                frame = page.location.CGRectValue()
                if frame.size.width != 0 {
                    pageView.frame = frame
                }
                else {
                    pageView.frame.origin.x = (startOrigin!.x - pageView.frame.size.width / 2) + CGFloat(index) * 50
                    pageView.frame.origin.y = (startOrigin!.y - pageView.frame.size.height / 2)
                    page.location = NSValue(CGRect: pageView.frame)
                    CoreDataUtil.sharedInstance.save()
                }
                containerView.addSubview(pageView)
            }
        }
        connectView.scrollView = scrollView
        connectView.detectView = containerView
        connectView.delegate = self
        
        animationSelectVC = self.childViewControllers[0].childViewControllers[0] as? AnimationSelectTableViewController
        
        if let project = project {
            if project.name == "Tutorial" {
                let coachMarks: Array<Dictionary<String, AnyObject>> = [
                    ["rect": NSValue(CGRect: CGRect(x: 112, y: 116, width: 800, height: 600)), "caption": "You can see all pages in this mode."],
                    ["rect": NSValue(CGRect: CGRect(x: 112, y: 116, width: 800, height: 600)), "caption": "Long press to move the page."],
                    ["rect": NSValue(CGRect: CGRect(x: 112, y: 116, width: 800, height: 600)), "caption": "When you start to drag from a page to other page, you can create connect between two pages."],
                    ["rect": NSValue(CGRect: CGRect(x: 112, y: 116, width: 800, height: 600)), "caption": "Congratulation!\n You have completed all of tutorial."]
                ]
                let coachMarksView = WSCoachMarksView(frame: self.navigationController!.view.bounds, coachMarks: coachMarks)
                self.navigationController!.view.addSubview(coachMarksView)
                coachMarksView.enableContinueLabel = true
                coachMarksView.maxLblWidth = 400
                coachMarksView.delegate = self
                coachMarksView.start()
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        reDrawConnectLine()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func centerScrollViewContents() {
//        let boundsSize = scrollView.bounds.size
//        var contentsFrame = containerView.frame
//        
//        if contentsFrame.size.width < boundsSize.width {
//            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
//        } else {
//            contentsFrame.origin.x = 0.0
//        }
//        
//        if contentsFrame.size.height < boundsSize.height {
//            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
//        } else {
//            contentsFrame.origin.y = 0.0
//        }
//        
//        containerView.frame = contentsFrame
//    }
//    
//    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
//        return containerView
//    }
//    
//    func scrollViewDidZoom(scrollView: UIScrollView) {
//        centerScrollViewContents()
//    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
    }
    
    func viewForTransition() -> UIView {
        let pageViews = containerView.subviews.filter {$0 is StoryboardPageView} as! [StoryboardPageView]
        if let index = datasource?._currentPage()?.index.integerValue {
            return pageViews[index].previewImage
        }
        else {
            return pageViews[0].previewImage
        }
    }
    
    func setConnectInfo(Source Source: Int, Target: Int) {
        if let PAGEs = datasource?.allPAGEs() {
            sourcePage = PAGEs[Source]
            targetPage = PAGEs[Target]
            print("sourcePAGE:\(sourcePage.name), targetPAGE:\(targetPage.name)")

            actionWidgets.removeAll(keepCapacity: false)
            let widgets = sourcePage.widgets.allObjects as! [NATIVE_WIDGET]
            for widget in widgets {
                switch widget {
                case is XKBUTTON:
                    actionWidgets.append(widget)
                case is XKIMAGEVIEW:
                    if (widget as! XKIMAGEVIEW).style.integerValue == 2 {
                        actionWidgets.append(widget)
                    }
                case is XKTABLEVIEW:
                    actionWidgets.append(widget)
                case is XKNAV:
                    if let items = (widget as! XKNAV).items.allObjects as? [XKNAV_ITEM] {
                        for item in items {
                            actionWidgets.append(item)
                        }
                    }
                case is XKTAB:
                    if let items = (widget as! XKTAB).items.allObjects as? [XKTAB_ITEM] {
                        var sortitems = items
                        sortitems.sortInPlace {$0.index.integerValue < $1.index.integerValue}
                        for item in sortitems {
                            actionWidgets.append(item)
                        }
                    }
                default:
                    break
                }
                print("wid:\(widget.name)")
            }
            animationSelectVC?.actionWidgets = self.actionWidgets
            animationSelectVC?.targetPage = targetPage
            animationSelectVC?.tableView.reloadData()
            animationSelectView.hidden = false
            animationSelectView.viewMoveInFromLeft(animationTime: 0.5)
        }
    }
    
    func hideRightView() {
        if animationSelectView.hidden == false {
            animationSelectView.viewMoveInFromRight(animationTime: 0.5)
            animationSelectView.hidden = true
        }
    }
    
    func reDrawConnectLine() {
        connectView.linksPoint.removeAll(keepCapacity: false)
        connectView.setNeedsDisplay()
        if let pages = datasource?.allPAGEs() {
            for (index, page) in pages.enumerate() {
                let pageView = containerView.subviews[index] as! StoryboardPageView
                for action in page.actions.allObjects as! [ACTION] {
                    let pageFrame = action.triggerWidget.page.location.CGRectValue()
                    let widgetFrame = action.triggerWidget.frame.CGRectValue()
                    let ratioX = pageView.previewImage.frame.size.width / 259.0
                    let ratioY = pageView.previewImage.frame.size.height / 443.0
                    let start_x = pageFrame.origin.x + pageView.previewImage.frame.origin.x + ((widgetFrame.origin.x + widgetFrame.size.width / 2 - resizeIconSize / 4) * ratioX)
                    let start_y = pageFrame.origin.y + pageView.previewImage.frame.origin.y + ((widgetFrame.origin.y + widgetFrame.size.height / 2) * ratioY) - statusBarHeight
                    let startPoint = CGPoint(x: start_x , y: start_y) //targetwidget
                    var endPoint = pageView.frame.origin    // page
                    if (pageView.frame.origin.x + pageView.frame.size.width) < startPoint.x {
                        endPoint.x = pageView.frame.origin.x + pageView.frame.size.width
                    }
                    endPoint.y += pageView.frame.size.height / 2 + statusBarHeight
                    
                    let m: CGFloat = (endPoint.y - startPoint.y) / (endPoint.x - startPoint.x)
                    let b: CGFloat = startPoint.y - m * startPoint.x
                    
                    connectView.linksPoint.append(["action": action, "startPoint": NSValue(CGPoint: startPoint), "endPoint": NSValue(CGPoint: endPoint), "m": m, "b": b])
                }
            }
        }
        connectView.setNeedsDisplay()
    }
    
    func updatePageViewLocation(pageNum pageNum: Int, frame: CGRect) {
        if let PAGEs = datasource?.allPAGEs() {
            let page = PAGEs[pageNum]
            page.location = NSValue(CGRect: frame)
            CoreDataUtil.sharedInstance.save()
        }
    }
    
    func enterPage(pageNum pageNum: Int) {
        self.navigationController?.popViewControllerAnimated(true)
        delegate?.changePage(number: pageNum)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }

}
