//
//  PropertyTableViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/20.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

class PropertyTableViewController: UITableViewController, UITextFieldDelegate, SourceTableViewProtocol, UICollectionViewDelegate, UICollectionViewDataSource, SwitchImageTypeProtocol {
    
    var currentWidget:XKView?
    var attributeDict:Array<Dictionary<String,String>> = [[:]]
    var valueDict:Array<Dictionary<String,AnyObject>> = [[:]]
    var attributes:Array<Array<String>> = [[],[]]
    var sectionNum = 0
    var itemNum = 0
    var itemImageData: NSData?
    var collectionDataSource: Array<NSData> = []
    var entity:NATIVE_WIDGET!
    var savecontent:Array<Array<String>> = [[]]
    var viewcontroller:ViewController = ViewController()
    @IBOutlet var navBar: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getImageFromLibrary:", name: "getImageFromLibrary", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadProperty", name: "reloadProperty", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        reloadProperty()
    }
    
    func reloadProperty() {
        if currentWidget != nil {
            entity = currentWidget!.CDObject as! NATIVE_WIDGET
            navBar.title = currentWidget!.widgetName
            refreshEntityAttribute(entity)
        }
        else {
            attributeDict = [[:]]
            valueDict = [[:]]
            attributes = [[],[]]
            itemNum = 0
        }
        self.tableView.reloadData()
//        self.tableView.contentOffset = CGPoint(x: 0, y: -44)
    }
    
    func getViewController() -> PropertyTableViewController {
        return self
    }
    
    func reloadCollectionData() {
        if entity is XKIMAGEVIEW && (entity as! XKIMAGEVIEW).style.integerValue == 2 {
            collectionDataSource = Array((entity as! XKIMAGEVIEW).image) as! [NSData]
        }
    }
    
    func getImageFromLibrary(notif: NSNotification) {
        let images = notif.userInfo!["images"] as! [NSData]
        switch entity {
        case is XKIMAGEVIEW:
            (entity as! XKIMAGEVIEW).image = NSSet(array: images)
            for widget in (currentWidget?.contentView.subviews as [UIView]?)! {
                switch (entity as! XKIMAGEVIEW).style {
                case 0, 3, 4:
                    (widget as! UIImageView).image = UIImage(data: images[0])
                case 1:
                    (widget as! UIImageView).animationImages = images.map({ (let data) -> UIImage in
                        let image:UIImage = UIImage(data: data)!
                        return image
                    })
                    (widget as! UIImageView).animationDuration = 0.5
                    (widget as! UIImageView).animationRepeatCount = 0
                    (widget as! UIImageView).startAnimating()
                case 2:
                    collectionDataSource = images
                    if widget is UICollectionView {
                        (widget as! UICollectionView).reloadData()
                    }
                default:
                    print("QQ")
                }
            }
        case is XKBUTTON:
            (entity as! XKBUTTON).image = images[0]
            for widget in (currentWidget?.contentView.subviews as [UIView]?)! {
                (widget as! UIButton).setImage(UIImage(data: images[0]), forState: .Normal)
            }
        case is XKNAV, is XKTAB, is XKSEGMENT:
            itemImageData = images[0]
            let sourceVC = self.navigationController?.visibleViewController as! SourceTableViewController
            sourceVC.setItemImage()
        default:
            print("QQ")
        }
        
        CoreDataUtil.sharedInstance.save()
        reloadProperty()
    }
    
    func itemImageFromLibrary() -> NSData? {
        return self.itemImageData
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return valueDict.count
    }
    
    func refreshEntityAttribute(entity: NATIVE_WIDGET) {
        //print("entity is \(entity)")
        switch currentWidget!.widgetName {
        case "Label":
            entity.setTextAttribute()
            (entity as! XKLABEL).updateAttributes()
        case "Button":
            entity.setActionAttribute()
            (entity as! XKBUTTON).updateAttributes()
            if (entity as! XKBUTTON).style != 0 {
                entity.attributes.removeLast()
                entity.attributes.removeLast()
            }
        case "TextField":
            entity.setActionAttribute()
            (entity as! XKTEXTFIELD).updateAttributes()
        case "Horizontal-Line":
            entity.setGraphicAttribute()
            (entity as! XKHR).updateAttributes()
        case "Slider":
            entity.setActionAttribute()
            (entity as! XKSLIDER).updateAttributes()
        case "Switch":
            entity.setActionAttribute()
            (entity as! XKSWITCH).updateAttributes()
        case "Segment Control":
            entity.setActionAttribute()
            (entity as! XKSEGMENT).updateAttributes()
        case "Image":
            entity.setGraphicAttribute()
            (entity as! XKIMAGEVIEW).updateAttributes()
        case "Canvas":
            entity.setGraphicAttribute()
            (entity as! XKCANVAS).updateAttributes()
        case "View":
            entity.setGraphicAttribute()
            (entity as! XKCANVAS).updateAttributes()
        case "TextView":
            entity.setTextAttribute()
            (entity as! XKTEXTVIEW).updateAttributes()
            print(entity.attributes)
        case "tableview":
            entity.setGraphicAttribute()
            (entity as! XKTABLEVIEW).updateAttributes()
        case "picker":
            entity.setGraphicAttribute()
            (entity as! XKPICKERVIEW).updateAttributes()
        case "webview":
            entity.setGraphicAttribute()
            (entity as! XKWEBVIEW).updateAttributes()
        case "mapview":
            entity.setGraphicAttribute()
            (entity as! XKMAPVIEW).updateAttributes()
        case "Navigation":
            entity.setTextAttribute()
            (entity as! XKNAV).updateAttributes()
        case "Tab":
            entity.setTextAttribute()
            (entity as! XKTAB).updateAttributes()
        default:
            print("", terminator: "")
        }
        setEntityToValueDict(entity)
//        setEntityToAttributeDict(entity)
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return valueDict[section].count
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        if currentWidget != nil {
            let attr = attributes[indexPath.section][indexPath.row]
            let identifer:String = attributeDict[indexPath.section][attr]!
            let attributeName = attributes[indexPath.section][indexPath.row]
            switch identifer {
            case "TextCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! TextCell
                (cell as! TextCell).currentWidget = currentWidget
                (cell as! TextCell).attributeName.text = attributeName
                (cell as! TextCell).setAttributeValue((cell as! TextCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!, item_index:itemNum)
            case "TextSelectCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! TextSelectCell
                (cell as! TextSelectCell).currentWidget = currentWidget
                (cell as! TextSelectCell).attributeName.text = attributeName
                (cell as! TextSelectCell).setAttributeValue((cell as! TextSelectCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!)
            case "FrameCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! FrameCell
                (cell as! FrameCell).currentWidget = currentWidget
                (cell as! FrameCell).attributeName.text = attributeName
                (cell as! FrameCell).setAttributeValue((cell as! FrameCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!)
            case "ImageCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! ImageCell
                (cell as! ImageCell).currentWidget = currentWidget
                (cell as! ImageCell).attributeName.text = attributeName
                (cell as! ImageCell).setAttributeValue((cell as! ImageCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!)
            case "BooleanCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! BooleanCell
                (cell as! BooleanCell).currentWidget = currentWidget
                (cell as! BooleanCell).attributeName.text = attributeName
                (cell as! BooleanCell).setAttributeValue((cell as! BooleanCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!)
            case "ColorSelectCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! ColorSelectCell
                (cell as! ColorSelectCell).currentWidget = currentWidget
                (cell as! ColorSelectCell).attributeName.text = attributeName
                (cell as! ColorSelectCell).setAttributeValue((cell as! ColorSelectCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!)
            case "StyleSelectCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! StyleSelectCell
                (cell as! StyleSelectCell).currentWidget = currentWidget
                (cell as! StyleSelectCell).attributeName.text = attributeName
                (cell as! StyleSelectCell).setAttributeValue((cell as! StyleSelectCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!, item_index:itemNum)
            case "SourceCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! SourceCell
                (cell as! SourceCell).currentWidget = currentWidget
                (cell as! SourceCell).attributeName.text = attributeName
            case "StepCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifer) as! StepCell
                (cell as! StepCell).currentWidget = currentWidget
                (cell as! StepCell).attributeName.text = attributeName
                (cell as! StepCell).setAttributeValue((cell as! StepCell), attr:attributeName, value: valueDict[indexPath.section][attributeName]!)
            default:
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.reuseIdentifier == "ImageCell" {
            NSNotificationCenter.defaultCenter().postNotificationName("popOverImagePicker", object: nil)
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 40
        }
        return 0
    }
    
    
    // MARK: - CollectionView
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! PhotoCell
        if let image = UIImage(data: collectionDataSource[indexPath.item]) {
            cell.photo?.image = image
            if let entity = entity as? XKIMAGEVIEW {
                cell.photo?.contentMode = UIViewContentMode(rawValue: entity.contentMode.integerValue)!
                if entity.style.integerValue == 2{
                    if #available(iOS 8.0, *) {
                        if let effectView = cell.photo?.subviews[0] as? UIVisualEffectView {
                            effectView.hidden = !Bool(entity.blur)
                        }
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
        }
        cell.backgroundColor = UIColor.whiteColor()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionDataSource.count
    }
    
    
    func setEntityToValueDict(entity: NATIVE_WIDGET) {
        attributeDict = [[:]]
        valueDict = [[:]]
        attributes = [[],[]]
        itemNum = 0
        
        
        for attr in entity.attributes {
            if entity.valueForKey(attr) != nil {
                valueDict[sectionNum].updateValue(entity.valueForKey(attr)!, forKey: attr)
                setEntityToAttributeDict(attr)
                /* when property table need more section used.
                if attr != "items" {
                    sectionNum = 0
                    valueDict[sectionNum].updateValue(entity.valueForKey(attr)!, forKey: attr)
                    setEntityToAttributeDict(attr)
                }
                else {
                    sectionNum = 1
                    switch entity {
                    case is XKSEGMENT:
                        var itemsDict:Dictionary<String, Array<AnyObject>> = [:]
                        itemsDict = ["text": [], "image": []]
                        var items:Array<XKSEGMENT_ITEM> = (entity as! XKSEGMENT).items.allObjects as! [XKSEGMENT_ITEM]
                        items.sort {$0.index.integerValue < $1.index.integerValue}
                        for item in items {
                            item.updateAttributes()
                            itemsDict["text"]?.append(item.text)
                            itemsDict["image"]?.append(item.image)
                        }
                        (entity as! XKSEGMENT).insertSubAttributes()
                        valueDict.append(itemsDict)
                        attributeDict.append([:])
                        setEntityToAttributeDict("text")
                        setEntityToAttributeDict("image")
                    default:
                        println("GG")
                    }
                }
                */
                
            }
            else {
                valueDict[sectionNum].updateValue("", forKey: attr)
                setEntityToAttributeDict(attr)
            }
        }
        saveattribute(attributes[0], value: valueDict[0], entity:entity.name)
    }
    
    func setEntityToAttributeDict(attr: String) {
            //print("entity attr is \(attr)")
            switch attr {
            case "text", "title", "cellHeight", "sectionHeight", "url":
                attributeDict[sectionNum].updateValue("TextCell", forKey: attr)
            case "alpha", "currentValue", "zoomLevel":
                attributeDict[sectionNum].updateValue("TextSelectCell", forKey: attr)
            case "frame", "coordinate":
                attributeDict[sectionNum].updateValue("FrameCell", forKey: attr)
            case "image", "markerImage":
                attributeDict[sectionNum].updateValue("ImageCell", forKey: attr)
            case "enabled", "state", "editable", "blur":
                attributeDict[sectionNum].updateValue("BooleanCell", forKey: attr)
            case "backgroundColor", "fontColor", "maxTrackColor", "minTrackColor", "thumbColor", "tintColor", "strokeColor":
                attributeDict[sectionNum].updateValue("ColorSelectCell", forKey: attr)
            case "fontStyle", "alignment", "borderStyle", "keyboardStyle", "style", "cellStyle", "separator", "tableStyle", "contentMode", "stylus":
                attributeDict[sectionNum].updateValue("StyleSelectCell", forKey: attr)
            case "dataSource", "items", "cell", "section":
                attributeDict[sectionNum].updateValue("SourceCell", forKey: attr)
            case "columnCount", "itemCount", "strokeSize", "pageCount", "selected":
                attributeDict[sectionNum].updateValue("StepCell", forKey: attr)
            default:
                attributeDict[sectionNum].updateValue("TextSelectCell", forKey: attr)
            }
        attributes[sectionNum].append(attr)
    }
    
    
    func saveattribute(attributes:Array<String>, value:Dictionary<String, AnyObject>, entity: String){
        
        var array_n : Array<String> = []
        savecontent.popLast()
        print("savecontent is \(savecontent)")
        
        var arrayname:Array<String> = []
        arrayname.append(entity)
        arrayname.append("\n")
        
        savecontent.append(arrayname)
        
        for(var i=0; i<attributes.count; i++){
            var newarray:Array<String>= []
            newarray.append(attributes[i])
            
            let newstring:String = String(value[attributes[i]])
            var test:Array<String>=[]
            test=newstring.componentsSeparatedByString("(")
            test=test[1].componentsSeparatedByString(")")            
            newarray.append(test[0])
            
            savecontent.append(newarray)
        }
        let control = ViewController()
        control.updatefile(savecontent)
        savecontent = [[]]
    }
    
    
    
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "ToTextSelect" {
            let VC = segue.destinationViewController as! TextSelectTableViewController
            print("vc: \(VC.currentWidget)")
            VC.currentWidget = currentWidget
            let cell = sender as! TextSelectCell
            if cell.attributeName.text == "alpha" {
                VC.dataArray = ["0", "10", "20", "30", "40", "50", "60", "70", "80", "90", "100"]
            }
            else if cell.attributeName.text == "currentValue" {
                VC.dataArray = ["0.0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1.0"]
            }
            VC.navBar.title = cell.attributeName.text
            print("ToTextSelect")
        }
        else if segue.identifier == "ToFrame" {
            let VC = segue.destinationViewController as! FrameTableViewController
            VC.currentWidget = currentWidget
            let cell = sender as! FrameCell
            VC.navBar.title = cell.attributeName.text
            if cell.attributeName.text == "frame" {
                var rect: Array<String>! = cell.attributeValue.text?.componentsSeparatedByString(" , ")
                VC.origin = rect[0].substringWithRange(Range<String.Index>(start: rect[0].startIndex.advancedBy(1), end: rect[0].endIndex.advancedBy(-1)))
                VC.size = rect[1]
            }
            else {
                VC.origin = cell.attributeValue.text
                VC.size = ""
            }
            print("ToFrame")
        }
        else if segue.identifier == "ToColorSelect" {
            let VC = segue.destinationViewController as! ColorSelectCollectionViewController
            VC.currentWidget = currentWidget
            let cell = sender as! ColorSelectCell
            VC.navBar.title = cell.attributeName.text
            print("ToColorSelect")
        }
        else if segue.identifier == "ToStyleSelect" {
            let VC = segue.destinationViewController as! StyleSelectCollectionViewController
            VC.delegate = self
            VC.dataSource = self
            VC.currentWidget = currentWidget
            let cell = sender as! StyleSelectCell
            VC.navBar.title = cell.attributeName.text
            print("ToStyleSelect")
        }
        else if segue.identifier == "ToSource" {
            let VC = segue.destinationViewController as! SourceTableViewController
            VC.delegate = self
            VC.datasource = self
            VC.currentWidget = currentWidget
            let cell = sender as! SourceCell
            VC.navBar.title = cell.attributeName.text
            print("ToSource")
        }
        
        
    }

}
