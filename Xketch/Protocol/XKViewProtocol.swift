//
//  XKViewProtocol.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/15.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import UIKit

@objc protocol XKViewProtocol {
    
    // datasource
    func currentWidget() -> XKView?
    func allPAGEs() -> Array<PAGE>
    func _currentPage() -> PAGE?
    
    // delegate
    func changePage(number number: Int)
    func updatePreviewImage(page: PAGE)
    func toStoryboardMode()
    func showMenuView()
    func hideMenuView()
}
