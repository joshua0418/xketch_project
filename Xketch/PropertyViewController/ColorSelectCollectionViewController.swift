//
//  ColorSelectCollectionViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/29.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

extension UIColor {
    class func colorWithRGBHex(hex: Int, alpha: Float = 1.0) -> UIColor {
        let r = Float((hex >> 16) & 0xFF)
        let g = Float((hex >> 8) & 0xFF)
        let b = Float((hex) & 0xFF)
        
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue:CGFloat(b / 255.0), alpha: CGFloat(alpha))
    }
}

class ColorSelectCollectionViewController: UICollectionViewController {
    var currentWidget:XKView?
    @IBOutlet var navBar: UINavigationItem!
//    var colors:Array<Int> = [0xD24D57, 0xDB0A5B, 0xF64747, 0xF1A9A0, 0xD2527F, 0xF62459, 0xDCC6E0, 0x663399, 0x674172, 0xAEA8D3, 0x446CB3, 0xE4F1FE, 0x4183D7, 0x81CFE0, 0x4ECDC4, 0xA2DED0, 0x87D37C, 0x90C695, 0x03C9A9, 0x68C3A3, 0xF5D76E, 0xFDE3A7, 0xF89406, 0xECECEC, 0x6C7A89, 0x000000, 0x262626, 0x464646, 0x555555, 0x707070, 0xACACAC, 0xC2C2C2, 0xD7D7D7, 0xEBEBEB, 0xFFFFFF]
    var colors: Array<Int> = [0xff0000, 0xc30210, 0xd6083b, 0xec667f, 0xff5113, 0xff7000, 0xfe4819, 0xe24912, 0xfff100, 0xfff799, 0xecac00, 0xe9ef50, 0xb3d465, 0x77b800, 0x32b16c, 0x025927, 0xa0dae8, 0x00a8e1, 0x0065a2, 0x005072, 0x1e0576, 0x1d0d69, 0x003e74, 0x0707b6, 0x440062, 0x512698, 0xbc9dd6, 0xa40035, 0xdcdcdc, 0x929d9e, 0x4b5457, 0x343735, 0x747679, 0x000000, 0xffffff]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return colors.count + 1
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ColorCell", forIndexPath: indexPath) 

        for subview in cell.subviews {
            subview.removeFromSuperview()
        }
        
        if indexPath.item < colors.count {
            cell.backgroundColor = UIColor.colorWithRGBHex(colors[indexPath.item])
        }
        else {
            cell.backgroundColor = UIColor.clearColor()
            let transprantImage = UIImageView(image: UIImage(named: "transprant"))
            cell.addSubview(transprantImage)
        }
        cell.layer.borderColor = UIColor.blackColor().CGColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        cell.layer.shadowColor = UIColor.darkGrayColor().CGColor
        cell.layer.shadowOffset = cell.frame.size
        cell.layer.shadowRadius = 10
        // Configure the cell
    
        return cell
    }

    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath)
        let selectedColor = cell?.backgroundColor
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        for obj in currentWidget?.contentView.subviews as [UIView]! {
            if navBar.title == "backgroundColor" {
                obj.backgroundColor = selectedColor
            }
            else if navBar.title == "fontColor" {
                switch obj {
                case is UILabel:
                    (obj as! UILabel).textColor = selectedColor
                case is UIButton:
                    (obj as! UIButton).setTitleColor(selectedColor, forState: UIControlState.Normal)
                case is UITextField:
                    (obj as! UITextField).textColor = selectedColor
                case is UISegmentedControl:
                    (obj as! UISegmentedControl).tintColor = selectedColor
                case is UITextView:
                    (obj as! UITextView).textColor = selectedColor
                case is UINavigationBar:
                    (obj as! UINavigationBar).titleTextAttributes = [NSForegroundColorAttributeName: selectedColor!]
                default:
                    print("GG")
                }

            }
            else if navBar.title == "maxTrackColor" {
                (obj as! UISlider).maximumTrackTintColor = selectedColor
            }
            else if navBar.title == "minTrackColor" {
                (obj as! UISlider).minimumTrackTintColor = selectedColor
            }
            else if navBar.title == "tintColor" {
                (obj as! UISwitch).tintColor = selectedColor
            }
            else if navBar.title == "strokeColor" {
                (obj as! CanvasView).strokeColor = selectedColor
            }
//            else if navBar.title == "thumbColor" {
//                (obj as! UISlider).thumbTintColor = selectedColor
//            }
        }
        entity.setValue(selectedColor, forKey: navBar.title!)
        CoreDataUtil.sharedInstance.save()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func drawLayer(layer: CALayer, inContext ctx: CGContext) {
        print("XD")
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
