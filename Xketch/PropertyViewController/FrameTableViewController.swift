//
//  FrameTableViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/28.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import MapKit

class FrameTableViewController: UITableViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    var currentWidget:XKView?
    var origin: String!
    var size: String!
    var prevPoint: CGPoint?
    @IBOutlet var navBar: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "popVC", name: "refreshTable", object: nil)
    }
    
    func popVC() {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TextField Delegate
    
    func textFieldDidEndEditing(textField: UITextField) {
        let cell = textField.superview?.superview as? TextCell
        let attr = cell?.attributeName.text
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        
        if navBar.title == "frame" {
            let value = entity.valueForKey("frame") as! NSValue
            
            var rect:CGRect = value.CGRectValue()
            if attr == "X" {
                rect.origin.x = CGFloat((textField.text! as NSString).doubleValue) - resizeIconSize/2
            }
            else if attr == "Y" {
                rect.origin.y = CGFloat((textField.text! as NSString).doubleValue) - resizeIconSize/2
            }
            else if attr == "Width" {
                rect.size.width = CGFloat((textField.text! as NSString).doubleValue) + resizeIconSize
            }
            else if attr == "Height" {
                rect.size.height = CGFloat((textField.text! as NSString).doubleValue) + resizeIconSize
            }
            entity.setValue(NSValue(CGRect: rect), forKey: "frame")
            currentWidget?.frame = rect
            print("CGPOINT: \(CGPoint(x: resizeIconSize/2, y: resizeIconSize/2))")
            let content_rect = CGRect(origin: CGPoint(x: resizeIconSize/2, y: resizeIconSize/2), size: CGSize(width: rect.size.width - resizeIconSize, height: rect.size.height - resizeIconSize))
            currentWidget?.contentView.frame = content_rect
            let object:UIView = currentWidget?.contentView.subviews[0] as UIView!
            object.frame = CGRectMake(0, 0, content_rect.size.width, content_rect.size.height)
            currentWidget?.reDefineFrame()
            currentWidget?.reDefineResizeIcon()
        }
        else {
            let value = entity.valueForKey("coordinate") as! NSData
            let location = NSKeyedUnarchiver.unarchiveObjectWithData(value) as! CLLocation
            var lat = location.coordinate.latitude
            var lng = location.coordinate.longitude
            if attr == "Lat" {
                lat = (textField.text! as NSString).doubleValue
            }
            else if attr == "Lng" {
                lng = (textField.text! as NSString).doubleValue
            }
            let new_location = CLLocation(latitude: lat, longitude: lng)
            entity.setValue(NSKeyedArchiver.archivedDataWithRootObject(new_location), forKey: "coordinate")
            let object:MKMapView = currentWidget?.contentView.subviews[0] as! MKMapView
            let center = CLLocationCoordinate2D(latitude: new_location.coordinate.latitude, longitude: new_location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            object.setRegion(region, animated: true)
        }
        
        CoreDataUtil.sharedInstance.save()
        NSNotificationCenter.defaultCenter().postNotificationName("refreshTable", object: nil)
    }


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        if size == "" {
            return 1
        }
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 2
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TextCell", forIndexPath: indexPath) as! TextCell
        if indexPath.section == 0 {
            if size == "" {
                cell.attributeName.text = indexPath.row == 0 ? "Lat" : "Lng"
            }
            else {
                cell.attributeName.text = indexPath.row == 0 ? "X" : "Y"
                let panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: "handlerPan:")
                panGesture.delegate = self
                cell.attributeValue.addGestureRecognizer(panGesture)
            }
            let info: Array<String>! = origin.componentsSeparatedByString(",")
            cell.attributeValue.text = indexPath.row == 0 ? info[0] : info[1]
        }
        else {
            cell.attributeName.text = indexPath.row == 0 ? "Width" : "Height"
            let info: Array<String>! = size.componentsSeparatedByString("x")
            cell.attributeValue.text = indexPath.row == 0 ? info[0] : info[1]
            let panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: "handlerPan:")
            panGesture.delegate = self
            cell.attributeValue.addGestureRecognizer(panGesture)
        }
        cell.attributeValue.keyboardType = UIKeyboardType.NumberPad
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            if size == "" {
                return "Location"
            }
            return "Origin"
        }
        else {
            return "Size"
        }
    }

    func handlerPan(sender: UIPanGestureRecognizer) {
        let attributeValue = sender.view! as! UITextField
        let cell = attributeValue.superview?.superview as? TextCell
        let attr = cell?.attributeName.text
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        let location = sender.locationInView(sender.view!)
        switch sender.state {
        case .Began:
            prevPoint = location
        case .Changed:
            let offsetY = Int(prevPoint!.y - location.y)
            if offsetY != 0 && (attributeValue.text! as NSString).integerValue >= 0 {
                prevPoint = location
                attributeValue.text = "\((attributeValue.text! as NSString).integerValue + offsetY)"
            
                let value = entity.valueForKey("frame") as! NSValue
                var rect:CGRect = value.CGRectValue()
                if attr == "X" {
                    rect.origin.x = CGFloat((attributeValue.text! as NSString).doubleValue) - resizeIconSize/2
                }
                else if attr == "Y" {
                    rect.origin.y = CGFloat((attributeValue.text! as NSString).doubleValue) - resizeIconSize/2
                }
                else if attr == "Width" {
                    rect.size.width = CGFloat((attributeValue.text! as NSString).doubleValue) + resizeIconSize
                }
                else if attr == "Height" {
                    rect.size.height = CGFloat((attributeValue.text! as NSString).doubleValue) + resizeIconSize
                }
                entity.setValue(NSValue(CGRect: rect), forKey: "frame")
                currentWidget?.frame = rect
                let content_rect = CGRect(origin: CGPoint(x: resizeIconSize/2, y: resizeIconSize/2), size: CGSize(width: rect.size.width - resizeIconSize, height: rect.size.height - resizeIconSize))
                currentWidget?.contentView.frame = content_rect
                let object:UIView = currentWidget?.contentView.subviews[0] as UIView!
                object.frame = CGRectMake(0, 0, content_rect.size.width, content_rect.size.height)
                currentWidget?.reDefineResizeIcon()
            }
        default:
            print("?")
        }
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
