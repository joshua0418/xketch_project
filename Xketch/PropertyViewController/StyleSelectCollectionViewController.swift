//
//  StyleSelectCollectionViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/29.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

protocol SwitchImageTypeProtocol {
    func getViewController() -> PropertyTableViewController
    func reloadCollectionData()
}

class StyleSelectCollectionViewController: UICollectionViewController {
    var delegate: SwitchImageTypeProtocol?
    var dataSource: SwitchImageTypeProtocol?
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var column = 0
    var currentWidget:XKView?
    var targetPage: PAGE!
    
    @IBOutlet var navBar: UINavigationItem!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        if navBar.title == "alignment" {
            if currentWidget?.widgetName == "Label" {
                return 3
            }
            else {
                return 4
            }
        }
        else if navBar.title == "borderStyle" {
            return 6
        }
        else if navBar.title == "fontStyle" {
            return 6
        }
        else if navBar.title == "keyboardStyle" {
            return 4
        }
        else if navBar.title == "contentMode" {
            return 3
        }
        else if navBar.title == "style" {
            if currentWidget?.widgetName == "TextField" {
                return 4
            }
            else if currentWidget?.widgetName == "Button" {
                return 7
            }
            else if currentWidget?.widgetName == "Horizontal-Line" {
                return 6
            }
            else if currentWidget?.widgetName == "Image" {
                return 5
            }
            else if currentWidget?.widgetName == "Navigation" {
                return 22   // not include 6,7
            }
            else if currentWidget?.widgetName == "Tab" {
                return 13
            }
        }
        else if navBar.title == "tableStyle" {
            return 2
        }
        else if navBar.title == "cellStyle" {
            return 6
        }
        else if navBar.title == "Animation" {
            return 6
        }
        else {
            return 7
        }
        return 0
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("StyleCell", forIndexPath: indexPath) as! StyleCell
        cell.tag = indexPath.item
        cell.contentView.layer.borderColor = UIColor.grayColor().CGColor
        cell.contentView.layer.borderWidth = 1
        cell.styleItem.setImage(nil, forState: .Normal)
        cell.styleItem.setBackgroundImage(nil, forState: .Normal)
        
        if navBar.title == "alignment" {
            switch indexPath.item {
            case 0:
                cell.styleItem.setBackgroundImage(UIImage(named: "align_left"), forState: .Normal)
            case 1:
                cell.styleItem.setBackgroundImage(UIImage(named: "align_center"), forState: .Normal)
            case 2:
                cell.styleItem.setBackgroundImage(UIImage(named: "align_right"), forState: .Normal)
            case 3:
                cell.styleItem.setBackgroundImage(UIImage(named: "align_justify"), forState: .Normal)
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
        else if navBar.title == "style" {
            if currentWidget?.widgetName == "Horizontal-Line" {
                switch indexPath.item {
                case 0:
                    cell.styleItem.setBackgroundImage(UIImage(named: "HR0"), forState: .Normal)
                case 1:
                    cell.styleItem.setBackgroundImage(UIImage(named: "HR1"), forState: .Normal)
                case 2:
                    cell.styleItem.setBackgroundImage(UIImage(named: "HR2"), forState: .Normal)
                case 3:
                    cell.styleItem.setBackgroundImage(UIImage(named: "HR3"), forState: .Normal)
                case 4:
                    cell.styleItem.setBackgroundImage(UIImage(named: "HR4"), forState: .Normal)
                case 5:
                    cell.styleItem.setBackgroundImage(UIImage(named: "HR5"), forState: .Normal)
                default:
                    cell.styleItem.setBackgroundImage(UIImage(named: ""), forState: .Normal)
                    cell.styleItem.setTitle(String(indexPath.item), forState: .Normal)
                }
            }
            else if currentWidget?.widgetName == "Button" {
                switch indexPath.item {
                case 0:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Custom"), forState: .Normal)
                case 1:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Button_Style0"), forState: .Normal)
                case 2:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Button_Style1"), forState: .Normal)
                case 3:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Button_Style2"), forState: .Normal)
                case 4:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Button_Style3"), forState: .Normal)
                case 5:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Button_Style4"), forState: .Normal)
                case 6:
                    cell.styleItem.setBackgroundImage(UIImage(named: "Button_Style5"), forState: .Normal)
                default:
                    cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
                }
            }
            else if currentWidget?.widgetName == "TextField" {
                cell.contentView.layer.borderColor = UIColor.clearColor().CGColor
                cell.contentView.layer.borderWidth = 0
                cell.styleItem.imageView?.contentMode = .ScaleAspectFit
                switch indexPath.item {
                case 0:
                    cell.styleItem.setImage(UIImage(named: "TextField_Style0"), forState: .Normal)
                case 1:
                    cell.styleItem.setImage(UIImage(named: "TextField_Style1"), forState: .Normal)
                case 2:
                    cell.styleItem.setImage(UIImage(named: "TextField_Style2"), forState: .Normal)
                case 3:
                    cell.styleItem.setImage(UIImage(named: "TextField_Style3"), forState: .Normal)
                default:
                    cell.styleItem.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
            }
            else if currentWidget?.widgetName == "Image" {
                switch indexPath.item {
                case 0:
                    cell.styleItem.setImage(UIImage(named: "image_style0"), forState: .Normal)
                case 1:
                    cell.styleItem.setImage(UIImage(named: "image_style1"), forState: .Normal)
                case 2:
                    let gif: Array<UIImage> = [UIImage(named: "image_style2_1")!, UIImage(named: "image_style2_2")!, UIImage(named: "image_style2_3")!, UIImage(named: "image_style2_3")!, UIImage(named: "image_style2_1")!]
                    cell.animateImage.animationImages = gif
                    cell.animateImage.animationDuration = 1.5
                    cell.animateImage.animationRepeatCount = 0
                    cell.animateImage.startAnimating()
                case 3:
                    let gif: Array<UIImage> = [UIImage(named: "image_style3_1")!, UIImage(named: "image_style3_2")!, UIImage(named: "image_style3_3")!, UIImage(named: "image_style3_4")!, UIImage(named: "image_style3_4")!, UIImage(named: "image_style3_1")!]
                    cell.animateImage.animationImages = gif
                    cell.animateImage.animationDuration = 1.5
                    cell.animateImage.animationRepeatCount = 0
                    cell.animateImage.startAnimating()
                case 4:
                    cell.styleItem.setImage(UIImage(named: "image_style4"), forState: .Normal)
                default:
                    cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
                }
            }
            else if currentWidget?.widgetName == "Navigation" {
                switch indexPath.item {
                case 0:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "Custom"), forState: .Normal)
                case 1:
                    cell.styleItem.setTitle("Done", forState: .Normal)
                    cell.styleItem.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    cell.styleItem.backgroundColor = UIColor.whiteColor()
                    cell.styleItem.setImage(nil, forState: .Normal)
                case 2:
                    cell.styleItem.setTitle("Cancel", forState: .Normal)
                    cell.styleItem.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    cell.styleItem.backgroundColor = UIColor.whiteColor()
                    cell.styleItem.setImage(nil, forState: .Normal)
                case 3:
                    cell.styleItem.setTitle("Edit", forState: .Normal)
                    cell.styleItem.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    cell.styleItem.backgroundColor = UIColor.whiteColor()
                    cell.styleItem.setImage(nil, forState: .Normal)
                case 4:
                    cell.styleItem.setTitle("Save", forState: .Normal)
                    cell.styleItem.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    cell.styleItem.backgroundColor = UIColor.whiteColor()
                    cell.styleItem.setImage(nil, forState: .Normal)
                case 5:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item5"), forState: .Normal)
                case 6:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item6"), forState: .Normal)
                case 7:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item7"), forState: .Normal)
                case 8:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item8"), forState: .Normal)
                case 9:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item9"), forState: .Normal)
                case 10:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item10"), forState: .Normal)
                case 11:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item11"), forState: .Normal)
                case 12:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item12"), forState: .Normal)
                case 13:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item13"), forState: .Normal)
                case 14:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item14"), forState: .Normal)
                case 15:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item15"), forState: .Normal)
                case 16:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item16"), forState: .Normal)
                case 17:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item17"), forState: .Normal)
                case 18:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item18"), forState: .Normal)
                case 19:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "nav_item19"), forState: .Normal)
                case 20:
                    cell.styleItem.setTitle("Undo", forState: .Normal)
                    cell.styleItem.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    cell.styleItem.backgroundColor = UIColor.whiteColor()
                    cell.styleItem.setImage(nil, forState: .Normal)
                case 21:
                    cell.styleItem.setTitle("Redo", forState: .Normal)
                    cell.styleItem.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    cell.styleItem.backgroundColor = UIColor.whiteColor()
                    cell.styleItem.setImage(nil, forState: .Normal)
                default:
                    cell.styleItem.backgroundColor = UIColor.redColor()
                    cell.styleItem.setTitle(String(format: "%d", indexPath.item), forState: .Normal)
                }
            }
            else if currentWidget?.widgetName == "Tab" {
                switch indexPath.item {
                case 0:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "Custom"), forState: .Normal)
                    cell.descLabel.text = ""
                case 1:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item1"), forState: .Normal)
                    cell.descLabel.text = "More"
                case 2:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item2"), forState: .Normal)
                    cell.descLabel.text = "Favorites"
                case 3:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item2"), forState: .Normal)
                    cell.descLabel.text = "Featured"
                case 4:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item2"), forState: .Normal)
                    cell.descLabel.text = "Top Rated"
                case 5:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item5"), forState: .Normal)
                    cell.descLabel.text = "Recents"
                case 6:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item6"), forState: .Normal)
                    cell.descLabel.text = "Contacts"
                case 7:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item5"), forState: .Normal)
                    cell.descLabel.text = "History"
                case 8:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item8"), forState: .Normal)
                    cell.descLabel.text = "Bookmarks"
                case 9:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item9"), forState: .Normal)
                    cell.descLabel.text = "Search"
                case 10:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item10"), forState: .Normal)
                    cell.descLabel.text = "Downloads"
                case 11:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item5"), forState: .Normal)
                    cell.descLabel.text = "Most Recent"
                case 12:
                    cell.styleItem.setTitle("", forState: .Normal)
                    cell.styleItem.setImage(UIImage(named: "tab_item12"), forState: .Normal)
                    cell.descLabel.text = "Most Viewed"
                default:
                    cell.styleItem.backgroundColor = UIColor.redColor()
                    cell.styleItem.setTitle(String(format: "%d", indexPath.item), forState: .Normal)
                }
            }
        }
        else if navBar.title == "borderStyle" {
            cell.contentView.layer.borderColor = UIColor.clearColor().CGColor
            cell.contentView.layer.borderWidth = 0
            switch indexPath.item {
            case 0:
                cell.styleItem.setBackgroundImage(UIImage(named: "border_stylex"), forState: .Normal)
            case 1:
                cell.styleItem.setBackgroundImage(UIImage(named: "border_style1"), forState: .Normal)
            case 2:
                cell.styleItem.setBackgroundImage(UIImage(named: "border_style2"), forState: .Normal)
            case 3:
                cell.styleItem.setBackgroundImage(UIImage(named: "border_style3"), forState: .Normal)
            case 4:
                cell.styleItem.setBackgroundImage(UIImage(named: "border_style4"), forState: .Normal)
            case 5:
                cell.styleItem.setBackgroundImage(UIImage(named: "border_style5"), forState: .Normal)
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
        else if navBar.title == "keyboardStyle" {
            switch indexPath.item {
            case 0:
                cell.styleItem.setImage(UIImage(named: "keyboard_stylex"), forState: .Normal)
            case 1:
                cell.styleItem.setImage(UIImage(named: "keyboard_style1"), forState: .Normal)
            case 2:
                cell.styleItem.setImage(UIImage(named: "keyboard_style2"), forState: .Normal)
            case 3:
                cell.styleItem.setImage(UIImage(named: "keyboard_style3"), forState: .Normal)
            default:
                cell.styleItem.setImage(UIImage(named: "image_default"), forState: .Normal)
            }

        }
        else if navBar.title == "contentMode" {
            switch indexPath.item {
            case 0:
                cell.styleItem.setBackgroundImage(UIImage(named: "content_mode0"), forState: .Normal)
            case 1:
                cell.styleItem.setBackgroundImage(UIImage(named: "content_mode1"), forState: .Normal)
            case 2:
                cell.styleItem.setBackgroundImage(UIImage(named: "content_mode2"), forState: .Normal)
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
        else if navBar.title == "tableStyle" {
            switch indexPath.item {
            case 0:
                cell.styleItem.setBackgroundImage(UIImage(named: "tableview_stylex"), forState: .Normal)
            case 1:
                cell.styleItem.setBackgroundImage(UIImage(named: "tableview_style1"), forState: .Normal)
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
        else if navBar.title == "cellStyle" {
            switch indexPath.item {
            case 0:
                cell.styleItem.setBackgroundImage(UIImage(named: "cell_style0"), forState: .Normal)
            case 1:
                cell.styleItem.setBackgroundImage(UIImage(named: "cell_style1"), forState: .Normal)
            case 2:
                cell.styleItem.setBackgroundImage(UIImage(named: "cell_style2"), forState: .Normal)
            case 3:
                cell.styleItem.setBackgroundImage(UIImage(named: "cell_style3"), forState: .Normal)
            case 4:
                cell.styleItem.setBackgroundImage(UIImage(named: "cell_style4"), forState: .Normal)
            case 5:
                cell.styleItem.setBackgroundImage(UIImage(named: "cell_style5"), forState: .Normal)
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
        else if navBar.title == "fontStyle" {
            switch indexPath.item {
            case 0:
                cell.styleItem.setBackgroundImage(UIImage(named: "font_style0"), forState: .Normal)
            case 1:
                cell.styleItem.setBackgroundImage(UIImage(named: "font_style1"), forState: .Normal)
            case 2:
                cell.styleItem.setBackgroundImage(UIImage(named: "font_style2"), forState: .Normal)
            case 3:
                cell.styleItem.setBackgroundImage(UIImage(named: "font_style3"), forState: .Normal)
            case 4:
                cell.styleItem.setBackgroundImage(UIImage(named: "font_style4"), forState: .Normal)
            case 5:
                cell.styleItem.setBackgroundImage(UIImage(named: "font_style5"), forState: .Normal)
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
        else if navBar.title == "Animation" {
            switch indexPath.item {
            case 0:
                let gif: Array<UIImage> = [UIImage(named: "push(1)")!, UIImage(named: "push(2)")!, UIImage(named: "push(3)")!, UIImage(named: "push(4)")!, UIImage(named: "push(4)")!, UIImage(named: "push(1)")!]
                cell.animateImage.animationImages = gif
                cell.animateImage.animationDuration = 1.5
                cell.animateImage.animationRepeatCount = 0
                cell.animateImage.startAnimating()
            case 1:
                let gif: Array<UIImage> = [UIImage(named: "pop(1)")!, UIImage(named: "pop(2)")!, UIImage(named: "pop(3)")!, UIImage(named: "pop(4)")!, UIImage(named: "pop(4)")!, UIImage(named: "pop(1)")!]
                cell.animateImage.animationImages = gif
                cell.animateImage.animationDuration = 1.5
                cell.animateImage.animationRepeatCount = 0
                cell.animateImage.startAnimating()
            case 2:
                let gif: Array<UIImage> = [UIImage(named: "cover(0)")!, UIImage(named: "cover(1)")!, UIImage(named: "cover(2)")!, UIImage(named: "cover(3)")!, UIImage(named: "cover(3)")!, UIImage(named: "cover(0)")!]
                cell.animateImage.animationImages = gif
                cell.animateImage.animationDuration = 1.5
                cell.animateImage.animationRepeatCount = 0
                cell.animateImage.startAnimating()
            case 3:
                let gif: Array<UIImage> = [UIImage(named: "flip(1)")!, UIImage(named: "flip(2)")!, UIImage(named: "flip(3)")!, UIImage(named: "flip(4)")!, UIImage(named: "flip(5)")!, UIImage(named: "flip(6)")!, UIImage(named: "flip(7)")!, UIImage(named: "flip(8)")!, UIImage(named: "flip(9)")!, UIImage(named: "flip(10)")!, UIImage(named: "flip(11)")!, UIImage(named: "flip(11)")!, UIImage(named: "flip(1)")! ]
                cell.animateImage.animationImages = gif
                cell.animateImage.animationDuration = 1.5
                cell.animateImage.animationRepeatCount = 0
                cell.animateImage.startAnimating()
            case 4:
                let gif: Array<UIImage> = [UIImage(named: "cross(1)")!, UIImage(named: "cross(2)")!, UIImage(named: "cross(3)")!, UIImage(named: "cross(4)")!, UIImage(named: "cross(5)")!, UIImage(named: "cross(5)")!, UIImage(named: "cross(1)")!]
                cell.animateImage.animationImages = gif
                cell.animateImage.animationDuration = 1.5
                cell.animateImage.animationRepeatCount = 0
                cell.animateImage.startAnimating()
            case 5:
                let gif: Array<UIImage> = [UIImage(named: "partial(0)")!, UIImage(named: "partial(1)")!, UIImage(named: "partial(2)")!, UIImage(named: "partial(3)")!, UIImage(named: "partial(4)")!, UIImage(named: "partial(5)")!, UIImage(named: "partial(6)")!, UIImage(named: "partial(7)")!, UIImage(named: "partial(8)")!, UIImage(named: "partial(8)")!, UIImage(named: "partial(0)")!]
                cell.animateImage.animationImages = gif
                cell.animateImage.animationDuration = 1.5
                cell.animateImage.animationRepeatCount = 0
                cell.animateImage.startAnimating()
            default:
                cell.styleItem.setBackgroundImage(UIImage(named: "image_default"), forState: .Normal)
            }
        }
            /* 要補上Style的判斷與預覽圖，先用編號示意 */
        else {
            cell.styleItem.backgroundColor = UIColor.redColor()
            cell.styleItem.setTitle(String(format: "%d", indexPath.item), forState: .Normal)
        }
        // Configure the cell
    
        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! StyleCell
        cell.contentView.layer.borderColor = UIColor.grayColor().CGColor
        cell.contentView.layer.borderWidth = 1
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        appDelegate.insertLog(command: "Select style(\(indexPath.row))", object: entity.objectWithID(), project: entity.page.project, page: entity.page)
        
        if navBar.title == "alignment" {
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UILabel:
                    (obj as! UILabel).textAlignment = NSTextAlignment(rawValue: indexPath.item)!
                case is UIButton:
                    (obj as! UIButton).titleLabel?.textAlignment = NSTextAlignment(rawValue: indexPath.item)!
                case is UITextField:
                    (obj as! UITextField).textAlignment = NSTextAlignment(rawValue: indexPath.item)!
                case is UITextView:
                    (obj as! UITextView).textAlignment = NSTextAlignment(rawValue: indexPath.item)!
                default:
                    print("GG")
                }
            }
            entity.setValue(indexPath.item, forKey: navBar.title!)
        }
        else if navBar.title == "keyboardStyle" {
            targetHighLight(cell: cell, collectionView: collectionView, indexPath: indexPath)
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UITextField:
                    (obj as! UITextField).textAlignment = NSTextAlignment(rawValue: indexPath.item)!
                    switch indexPath.item {
                    case 0:
                        (obj as! UITextField).keyboardType = UIKeyboardType.Default
                    case 1:
                        (obj as! UITextField).keyboardType = UIKeyboardType.NumberPad
                    case 2:
                        (obj as! UITextField).keyboardType = UIKeyboardType.URL
                    case 3:
                        (obj as! UITextField).keyboardType = UIKeyboardType.EmailAddress
                    default:
                        print("GG")
                    }
                    (obj as! UITextField).resignFirstResponder()
                    (obj as! UITextField).becomeFirstResponder()
                default:
                    print("GG")
                }
            }
            entity.setValue(indexPath.item, forKey: navBar.title!)
        }
        else if navBar.title == "borderStyle" {
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UIButton:
                    switch indexPath.item {
                    case 0:
                        (obj as! UIButton).layer.borderWidth = 0
                        (obj as! UIButton).layer.borderColor = UIColor.clearColor().CGColor
                        (obj as! UIButton).layer.cornerRadius = 0
                        (obj as! UIButton).layer.shadowOpacity = 0.0;
                        (obj as! UIButton).layer.shadowColor = UIColor.clearColor().CGColor
                        (obj as! UIButton).layer.shadowOffset = CGSizeMake(0.0, 0.0);
                        (obj as! UIButton).layer.shadowRadius = 0;
                    case 1:
                        (obj as! UIButton).layer.borderWidth = 1
                        (obj as! UIButton).layer.borderColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.cornerRadius = 0
                        (obj as! UIButton).layer.shadowOpacity = 0.0;
                        (obj as! UIButton).layer.shadowColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.shadowOffset = CGSizeMake(0.0, 0.0);
                        (obj as! UIButton).layer.shadowRadius = 0;
                    case 2:
                        (obj as! UIButton).layer.borderWidth = 1
                        (obj as! UIButton).layer.borderColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.cornerRadius = 10
                        (obj as! UIButton).layer.shadowOpacity = 0.0;
                        (obj as! UIButton).layer.shadowColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.shadowOffset = CGSizeMake(0.0, 0.0);
                        (obj as! UIButton).layer.shadowRadius = 0;
                    case 3:
                        (obj as! UIButton).layer.borderWidth = 0
                        (obj as! UIButton).layer.borderColor = UIColor.clearColor().CGColor
                        (obj as! UIButton).layer.cornerRadius = 0
                        (obj as! UIButton).layer.shadowOpacity = 0.7;
                        (obj as! UIButton).layer.shadowColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.shadowOffset = CGSizeMake(3.0, 3.0);
                        (obj as! UIButton).layer.shadowRadius = 3;
                    case 4:
                        (obj as! UIButton).layer.borderWidth = 1
                        (obj as! UIButton).layer.borderColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.cornerRadius = 0
                        (obj as! UIButton).layer.shadowOpacity = 0.7;
                        (obj as! UIButton).layer.shadowColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.shadowOffset = CGSizeMake(3.0, 3.0);
                        (obj as! UIButton).layer.shadowRadius = 3;
                    case 5:
                        (obj as! UIButton).layer.borderWidth = 1
                        (obj as! UIButton).layer.borderColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.cornerRadius = 10
                        (obj as! UIButton).layer.shadowOpacity = 0.7;
                        (obj as! UIButton).layer.shadowColor = UIColor.blackColor().CGColor
                        (obj as! UIButton).layer.shadowOffset = CGSizeMake(3.0, 3.0);
                        (obj as! UIButton).layer.shadowRadius = 3;
                    default:
                        print("border style GG")
                    }
                default:
                    print("GG")
                }
            }
            entity.setValue(indexPath.item, forKey: navBar.title!)
        }
        else if navBar.title == "style" {
            if currentWidget?.widgetName == "Horizontal-Line" {
                var img: UIImage?
                switch indexPath.item {
                case 0:
                    img = UIImage(named: "HR_line0")!
                case 1:
                    img = UIImage(named: "HR_line1")!
                case 2:
                    img = UIImage(named: "HR_line2")!
                case 3:
                    img = UIImage(named: "HR_line3")!
                case 4:
                    img = UIImage(named: "HR_line4")!
                case 5:
                    img = UIImage(named: "HR_line5")!
                default:
                    print("GG")
                }
                for obj in currentWidget?.contentView.subviews as [UIView]! {
                    switch obj {
                    case is UIImageView:
                        (obj as! UIImageView).image = img
                        currentWidget?.frame.size.height = img!.size.height + resizeIconSize
                        currentWidget?.reDefineFrame()
                        currentWidget?.reDefineResizeIcon()
                    default:
                        print("GG")
                    }
                }
                entity.setValue(indexPath.item, forKey: navBar.title!)
            }
            else if currentWidget?.widgetName == "Navigation" {
                let widget = currentWidget?.contentView.subviews[0] as! UINavigationBar
                let topItem = widget.items![0] as UINavigationItem
                var items: Array<XKNAV_ITEM> = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
                items.sortInPlace {$0.side < $1.side}
                items[column].style = indexPath.item 
                if indexPath.item > 0 {
                    let type = indexPath.item < 6 ? indexPath.item - 1 : indexPath.item + 1
                    if let action = entity.action {
                        ActionUtil.sharedInstance.assignWidgetAction(currentWidget!, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: 0)
                    }
                    else {
                        if items[column].side == "left" {
                            topItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem(rawValue: type)!, target: nil, action: nil)
                            topItem.leftBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 5.0), forBarMetrics: .Default)
                        }
                        else {
                            topItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem(rawValue: type)!, target: nil, action: nil)
                            topItem.rightBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 5.0), forBarMetrics: .Default)
                        }
                    }
                }
                else {
                    if let action = entity.action {
                        ActionUtil.sharedInstance.assignWidgetAction(currentWidget!, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: 0)
                    }
                    else {
                        let button = UIButton(frame: CGRect(x: 0, y: 0, width: barButtonSize, height: barButtonSize))
                        if let image = UIImage(data: items[column].image) {
                            button.setImage(image, forState: .Normal)
                            button.imageEdgeInsets = UIEdgeInsetsMake(5.0, 0, -5.0, 0)
                            if items[column].side == "left" {
                                topItem.setLeftBarButtonItem(UIBarButtonItem(customView: button), animated: true)
                            }
                            else {
                                topItem.setRightBarButtonItem(UIBarButtonItem(customView: button), animated: true)
                            }
                        }
                        else {
                            let barItem: UIBarButtonItem = UIBarButtonItem()
                            barItem.title = items[column].text
                            if items[column].side == "left" {
                                topItem.setLeftBarButtonItem(barItem, animated: true)
                            }
                            else {
                                topItem.setRightBarButtonItem(barItem, animated: true)
                            }
                        }
                    }
                }
            }
            else if currentWidget?.widgetName == "Tab" {
                let widget = currentWidget?.contentView.subviews[0] as! UITabBar
                var items = Array((entity as! XKTAB).items) as! [XKTAB_ITEM]
                items.sortInPlace {($0 ).index.integerValue < ($1 ).index.integerValue}
                items[column].style = indexPath.item
                let selectIndex = widget.selectedItem?.tag
                var tabItems = widget.items! as [UITabBarItem]
                if indexPath.item > 0
                {
                    tabItems[column] = UITabBarItem(tabBarSystemItem: UITabBarSystemItem(rawValue: indexPath.item - 1)!, tag: column + 1)
                }
                else
                {
                    let itemImage:UIImage? = UIImage.imageResize(image: UIImage(data: items[indexPath.item].image), sizeChange: CGSize(width: barButtonSize, height: barButtonSize))
                    tabItems[column] = UITabBarItem(title: items[indexPath.item].text, image: itemImage, tag: column + 1)
                }
                widget.setItems(tabItems, animated: true)
                widget.selectedItem = (column + 1) == selectIndex ? tabItems[column] : widget.selectedItem
            }
            else {
                for obj in currentWidget?.contentView.subviews as [UIView]! {
                    switch obj {
                    case is UITextField:
                        switch indexPath.item {
                        case 0:
                            (obj as! UITextField).borderStyle = UITextBorderStyle.RoundedRect
                            (obj as! UITextField).borderStyle = UITextBorderStyle.None
                        case 1:
                            (obj as! UITextField).borderStyle = UITextBorderStyle.Line
                        case 2:
                            (obj as! UITextField).borderStyle = UITextBorderStyle.Bezel
                        case 3:
                            (obj as! UITextField).borderStyle = UITextBorderStyle.RoundedRect
                        default:
                            (obj as! UITextField).borderStyle = UITextBorderStyle.Bezel
                        }
                    case is UIButton:
                        switch indexPath.item {
                        case 0:
                            (obj as! UIButton).setTitle("Button", forState: .Normal)
                            (obj as! UIButton).setImage(nil, forState: .Normal)
                        case 1:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style0"), forState: .Normal)
                        case 2:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style1"), forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                        case 3:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style2"), forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                        case 4:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style3"), forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                        case 5:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style4"), forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                        case 6:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style5"), forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                        case 7:
                            (obj as! UIButton).setTitle("", forState: .Normal)
                            (obj as! UIButton).setImage(UIImage(named: "Button_Style6"), forState: .Normal)
                            (obj as! UIButton).backgroundColor = UIColor.clearColor()
                        default:
                            (obj as! UIButton).setTitle("Button", forState: .Normal)
                            (obj as! UIButton).setImage(nil, forState: .Normal)
                        }
                    case is UIImageView:
                        var images: Array<NSData> = (entity as! XKIMAGEVIEW).image.allObjects as! [NSData]
                        switch indexPath.item {
                        case 0:
                            (obj as! UIImageView).stopAnimating()
                            (obj as! UIImageView).image = UIImage(data: images[0])
                            (obj as! UIImageView).clipsToBounds = false
                            (obj as! UIImageView).layer.cornerRadius = 0
                            currentWidget?.resetLayout(resizeable: true)
                        case 1:
                            (obj as! UIImageView).animationImages = images.map({ ( data) -> UIImage in
                                let image:UIImage = UIImage(data: data)!
                                return image
                            })
                            (obj as! UIImageView).animationDuration = 0.5
                            (obj as! UIImageView).animationRepeatCount = 0
                            (obj as! UIImageView).startAnimating()
                            (obj as! UIImageView).clipsToBounds = false
                            (obj as! UIImageView).layer.cornerRadius = 0
                            currentWidget?.resetLayout(resizeable: true)
                        case 2:
                            let layout = EBCardCollectionViewLayout()
                            layout.offset = UIOffset(horizontal: (screenWidth - obj.frame.size.width)/2, vertical: 0)
                            currentWidget?.frame.origin.x = -resizeIconSize/2
                            currentWidget?.frame.size.width = screenWidth + resizeIconSize
                            entity.frame = NSValue(CGRect: currentWidget!.frame)
                            (entity as! XKIMAGEVIEW).imageSize = NSValue(CGSize: obj.frame.size)
                            let cardView = UICollectionView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: obj.frame.size.height), collectionViewLayout: layout)
                            obj.removeFromSuperview()
                            cardView.backgroundColor = entity.backgroundColor as? UIColor
                            cardView.delegate = ActionUtil.sharedInstance
                            cardView.dataSource = dataSource!.getViewController()
                            cardView.registerClass(PhotoCell.self, forCellWithReuseIdentifier: "Cell")
                            currentWidget?.contentView.addSubview(cardView)
                            currentWidget?.resetLayout(resizeable: false)
                            dataSource!.reloadCollectionData()
                            cardView.reloadData()
                        case 3:
                            (obj as! UIImageView).stopAnimating()
                            let fullScreenFrame = (currentWidget?.superview as! ScreenView).getFullScreenRect()
                            (obj as! UIImageView).frame = CGRect(x: 0, y: 0, width: fullScreenFrame.size.width, height: fullScreenFrame.size.height)
                            (obj as! UIImageView).image = UIImage(data: images[0])
                            currentWidget?.frame = CGRect(x: -resizeIconSize/2, y: -resizeIconSize/2 + fullScreenFrame.origin.y, width: fullScreenFrame.size.width + resizeIconSize, height: fullScreenFrame.size.height + resizeIconSize)
                            entity.frame = NSValue(CGRect: currentWidget!.frame)
                            (obj as! UIImageView).clipsToBounds = false
                            (obj as! UIImageView).layer.cornerRadius = 0
                            currentWidget?.resetLayout(resizeable: false)
                        case 4:
                            (obj as! UIImageView).stopAnimating()
                            (obj as! UIImageView).image = UIImage(data: images[0])
                            (obj as! UIImageView).layer.cornerRadius = (obj as! UIImageView).frame.size.width < (obj as! UIImageView).frame.size.height ? (obj as! UIImageView).frame.size.width / 2 : (obj as! UIImageView).frame.size.height / 2
                            (obj as! UIImageView).clipsToBounds = true
                            currentWidget?.resetLayout(resizeable: true)
                        default:
                            print("GG")
                        }
                    case is UICollectionView:
                        let images: Array<NSData> = (entity as! XKIMAGEVIEW).image.allObjects as! [NSData]
                        let cell = (obj as! UICollectionView).cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as! PhotoCell
                        let imageView = UIImageView(frame: cell.contentView.frame)
                        if #available(iOS 8.0, *) {
                            let blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
                            let effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
                            effectView.frame = imageView.frame
                            effectView.hidden = true
                            imageView.addSubview(effectView)
                        } else {
                            // Fallback on earlier versions
                        }
                        switch indexPath.item {
                        case 0:
                            imageView.image = UIImage(data: images[0])
                            imageView.layer.cornerRadius = 0
                            imageView.clipsToBounds = false
                            obj.removeFromSuperview()
                            currentWidget?.frame.size.height = imageView.frame.size.height + resizeIconSize
                            currentWidget?.frame.size.width = imageView.frame.size.width + resizeIconSize
                            currentWidget?.contentView.addSubview(imageView)
                            entity.frame = NSValue(CGRect: currentWidget!.frame)
                            currentWidget?.resetLayout(resizeable: true)
                        case 1:
                            imageView.animationImages = images.map({ ( data) -> UIImage in
                                let image:UIImage = UIImage(data: data)!
                                return image
                            })
                            imageView.animationDuration = 0.5
                            imageView.animationRepeatCount = 0
                            imageView.startAnimating()
                            imageView.layer.cornerRadius = 0
                            imageView.clipsToBounds = false
                            obj.removeFromSuperview()
                            currentWidget?.frame.size.height = imageView.frame.size.height + resizeIconSize
                            currentWidget?.frame.size.width = imageView.frame.size.width + resizeIconSize
                            currentWidget?.contentView.addSubview(imageView)
                            entity.frame = NSValue(CGRect: currentWidget!.frame)
                            currentWidget?.resetLayout(resizeable: true)
                        case 3:
                            (obj as! UIImageView).frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
                            imageView.image = UIImage(data: images[0])
                            imageView.layer.cornerRadius = 0
                            imageView.clipsToBounds = false
                            obj.removeFromSuperview()
                            currentWidget?.frame.size.height = imageView.frame.size.height + resizeIconSize
                            currentWidget?.frame.size.width = imageView.frame.size.width + resizeIconSize
                            currentWidget?.contentView.addSubview(imageView)
                            entity.frame = NSValue(CGRect: currentWidget!.frame)
                            currentWidget?.resetLayout(resizeable: false)
                        case 4:
                            imageView.image = UIImage(data: images[0])
                            imageView.layer.cornerRadius = imageView.frame.size.width < imageView.frame.size.height ? imageView.frame.size.width / 2 : imageView.frame.size.height / 2
                            imageView.clipsToBounds = true
                            obj.removeFromSuperview()
                            currentWidget?.frame.size.height = imageView.frame.size.height + resizeIconSize
                            currentWidget?.frame.size.width = imageView.frame.size.width + resizeIconSize
                            currentWidget?.contentView.addSubview(imageView)
                            entity.frame = NSValue(CGRect: currentWidget!.frame)
                            currentWidget?.resetLayout(resizeable: true)
                        default:
                            print("G")
                        }
                    default:
                        print("button style GG")
                    }
                }
                entity.setValue(indexPath.item, forKey: navBar.title!)
                NSNotificationCenter.defaultCenter().postNotificationName("refreshTable", object: nil)
            }
        }
        else if navBar.title == "contentMode" {
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UIImageView:
                    (obj as! UIImageView).contentMode = UIViewContentMode(rawValue: indexPath.item)!
                case is UICollectionView:
                    (obj as! UICollectionView).reloadData()
                default:
                    print("GG")
                }
            }
            entity.setValue(indexPath.item, forKey: navBar.title!)
        }
        else if navBar.title == "tableStyle" {
            var new_obj = UITableView()
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UITableView:
                    if indexPath.item == 0 {
                        new_obj = UITableView(frame: obj.frame, style: UITableViewStyle.Plain)
                    }
                    else {
                        new_obj = UITableView(frame: obj.frame, style: UITableViewStyle.Grouped)
                    }
                    new_obj.delegate = (obj as! UITableView).delegate
                    new_obj.dataSource = (obj as! UITableView).dataSource
                    obj.removeFromSuperview()
                default:
                    print("GG")
                }
            }
            currentWidget?.contentView.addSubview(new_obj)
            new_obj.reloadData()
            entity.setValue(indexPath.item, forKey: navBar.title!)
        }
        else if navBar.title == "cellStyle" {
            targetHighLight(cell: cell, collectionView: collectionView, indexPath: indexPath)
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UITableView:
                    entity.setValue(indexPath.item, forKey: navBar.title!)
                default:
                    print("GG")
                }
            }
        }
        else if navBar.title == "fontStyle" {
            for obj in currentWidget?.contentView.subviews as [UIView]! {
                switch obj {
                case is UILabel:
                    entity.setValue(indexPath.item, forKey: navBar.title!)
                    switch indexPath.item {
                    case 0:
                        (obj as! UILabel).font = UIFont(name: "HelveticaNeue", size: 17)
                    case 1:
                        (obj as! UILabel).font = UIFont(name: "HelveticaNeue-Bold", size: 17)
                    case 2:
                        (obj as! UILabel).font = UIFont(name: "HelveticaNeue-Light", size: 11)
                    case 3:
                        (obj as! UILabel).font = UIFont(name: "HelveticaNeue", size: 11)
                    case 4:
                        (obj as! UILabel).font = UIFont(name: "HelveticaNeue-Italic", size: 21)
                    case 5:
                        (obj as! UILabel).font = UIFont(name: "HelveticaNeue-Bold", size: 24)
                    default:
                        print("GG")
                    }
                default:
                    print("GG")
                }
            }
        }
        else if navBar.title == "Animation" {
            var action: ACTION?
            if entity.name == "Tab" {
                var items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
                items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
                if items[column].action != nil {
                    action = items[column].action
                    action!.setValue(indexPath.item, forKey: "type")
                }
            }
            else if entity.name == "Navigation" {
                var items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
                items.sortInPlace {$0.side < $1.side}
                let item: XKNAV_ITEM = column == 1 ? items.filter {$0.side == "right"}[0] : items.filter {$0.side == "left"}[0]
                if item.action != nil {
                    action = item.action
                    action!.setValue(indexPath.item, forKey: "type")
                }
            }
            else {
                if entity.action != nil {
                    action = entity.action
                    action!.setValue(indexPath.item, forKey: "type")
                }
            }
            
            if let action = action {
                if action.targetPage != targetPage {
                    ActionUtil.sharedInstance.assignWidgetAction(currentWidget!, page: targetPage, event: SegueEvent.TouchUpInside, type: SegueType(rawValue: indexPath.item), index: column)
                }
                else {
                    NSNotificationCenter.defaultCenter().postNotificationName("reloadAction", object: nil)
                }
            }
            else {
                ActionUtil.sharedInstance.assignWidgetAction(currentWidget!, page: targetPage, event: SegueEvent.TouchUpInside, type: SegueType(rawValue: indexPath.item), index: column)
                
                print("no target page")
            }
        }
        
        CoreDataUtil.sharedInstance.save()
        
        if navBar.title == "cellStyle" {
            NSNotificationCenter.defaultCenter().postNotificationName("updateTableViewDataSource", object: nil, userInfo: ["entity": entity])
        }
        else if navBar.title != "keyboardStyle" {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

    func targetHighLight(cell cell: StyleCell, collectionView: UICollectionView, indexPath: NSIndexPath) {
        for var index=0; index<collectionView.numberOfItemsInSection(indexPath.section); index++ {
            let otherCell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: index, inSection: indexPath.section)) as! StyleCell
            otherCell.contentView.layer.borderColor = UIColor.grayColor().CGColor
        }
        cell.contentView.layer.borderColor = UIColor.redColor().CGColor
    }
    
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
