//
//  SourceTableViewController.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/12.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit
protocol SourceTableViewProtocol {
    func itemImageFromLibrary () -> NSData?
}

class SourceTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PropertySectionViewProtocol {
    var delegate: SourceTableViewProtocol?
    var datasource: SourceTableViewProtocol?
    var currentWidget:XKView?
    var sources:Array<Dictionary<String, AnyObject>> = [[:]]
    var items:Array<NATIVE_WIDGET> = []
    var column = 0
    var entity: NATIVE_WIDGET!
    @IBOutlet var navBar: UINavigationItem!
    @IBOutlet var columnControl: UISegmentedControl!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var tableViewFooter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "popVC", name: "refreshTable", object: nil)
        initColumnControl()
       
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func initColumnControl() {
        columnControl.removeAllSegments()
        entity = currentWidget?.CDObject as! NATIVE_WIDGET
        switch entity {
        case is XKPICKERVIEW:
            tableView.editing = true
            sources = (entity as! XKPICKERVIEW).dataSource.allObjects as! [Dictionary<String, AnyObject>]
            sources.sortInPlace {($0["index"] as! NSNumber).integerValue < ($1["index"] as! NSNumber).integerValue}
            let columnCount = (entity as! XKPICKERVIEW).columnCount.integerValue
            while columnControl.numberOfSegments < columnCount {
                columnControl.insertSegmentWithTitle("Column \(columnControl.numberOfSegments + 1)", atIndex: columnControl.numberOfSegments, animated: true)
            }
            columnControl.selectedSegmentIndex = 0
        case is XKIMAGEVIEW:
            print("generate columnControl error.")
        case is XKSEGMENT:
            items = (entity as! XKSEGMENT).items.allObjects as! [XKSEGMENT_ITEM]
            for item in items as! [XKSEGMENT_ITEM] {
                item.updateAttributes()
            }
            items.sortInPlace {($0 as! XKSEGMENT_ITEM).index.integerValue < ($1 as! XKSEGMENT_ITEM).index.integerValue}
        case is XKNAV:
            items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
            for item in items as! [XKNAV_ITEM] {
                item.updateAttributes()
            }
            tableViewFooter.hidden = true
        case is XKTAB:
            items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
            for item in items as! [XKTAB_ITEM] {
                item.updateAttributes()
            }
            items.sortInPlace {($0 as! XKTAB_ITEM).index.integerValue < ($1 as! XKTAB_ITEM).index.integerValue}
        case is XKTABLEVIEW:
            if navBar.title == "cell" {
                columnControl.insertSegmentWithTitle("Titles", atIndex: columnControl.numberOfSegments, animated: true)
                columnControl.insertSegmentWithTitle("Sub-Titles", atIndex: columnControl.numberOfSegments, animated: true)
                columnControl.insertSegmentWithTitle("Images", atIndex: columnControl.numberOfSegments, animated: true)
                columnControl.selectedSegmentIndex = 0
            }
            else {
                
            }
        default:
            print("generate columnControl error.")
        }
    }

    func getCellIdentifier(attr: String) -> String {
        switch attr {
        case "text", "title":
            return "TextCell"
        case "image":
            return "ImageCell"
        case "style":
            return "StyleSelectCell"
        default:
            return "TextSelectCell"
        }
    }
    
    func popVC() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func removeSectionView(section: Int) {
        switch entity {
        case is XKSEGMENT:
            let widget = currentWidget?.contentView.subviews[0] as! UISegmentedControl
            WidgetUtil.sharedInstance.deleteSegmentItemBySection(widget, element: entity as! XKSEGMENT, section: section)
            items = (entity as! XKSEGMENT).items.allObjects as! [XKSEGMENT_ITEM]
            items.sortInPlace {($0 as! XKSEGMENT_ITEM).index.integerValue < ($1 as! XKSEGMENT_ITEM).index.integerValue}
        case is XKNAV:
            let widget = currentWidget?.contentView.subviews[0] as! UINavigationBar
            guard let widgetItems = widget.items else {
                return
            }
            let topItem = widgetItems[0] as UINavigationItem
            let item = items[section] as! XKNAV_ITEM
            WidgetUtil.sharedInstance.deleteNavigationBarItem(topItem, element: item)
            items.removeAtIndex(section)
        case is XKTAB:
            let widget = currentWidget?.contentView.subviews[0] as! UITabBar
            guard let widgetItems = widget.items else {
                return
            }
            let selectIndex = widget.selectedItem?.tag
            let item = items[section] as! XKTAB_ITEM
            if items.count > 1 { // can't less then 1
                WidgetUtil.sharedInstance.deleteTabBarItem(widget, element: item)
                items.removeAtIndex(section)
                for (index, item) in (items as! [XKTAB_ITEM]).enumerate() {
                    item.index = index + 1
                    (widgetItems[index] as UITabBarItem).tag = index + 1
                    print("index:\(item.index)")
                }
                if (section + 1) == selectIndex {
                    if section == items.count {
                        (entity as! XKTAB).selected = (items[items.count - 1] as! XKTAB_ITEM).index
                        widget.selectedItem = widgetItems[items.count - 1] as UITabBarItem
                    }
                    else if section == 0 {
                        (entity as! XKTAB).selected = (items[0] as! XKTAB_ITEM).index
                        widget.selectedItem = widgetItems[0] as UITabBarItem
                    }
                }
                else if (section + 1) < selectIndex {
                    (entity as! XKTAB).selected = (items[section] as! XKTAB_ITEM).index
                    widget.selectedItem = widgetItems[section] as UITabBarItem
                }
                (entity as! XKTAB).itemCount = items.count
                CoreDataUtil.sharedInstance.save()
            }
        default:
            print("")
        }
        tableView.reloadData()
    }

    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        switch entity {
        case is XKSEGMENT, is XKNAV, is XKTAB:
            return items.count
        default:
            return 1
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        switch entity {
        case is XKPICKERVIEW:
            if let data = sources[column]["data"] as? Array<String> {
                return data.count
            }
            else {
                return 0
            }
        case is XKSEGMENT:
            return items[section].attributes.count
        case is XKNAV:
            return (items[section] as! XKNAV_ITEM).style == 0 ? items[section].attributes.count : 1
        case is XKTAB:
            return (items[section] as! XKTAB_ITEM).style == 0 ? items[section].attributes.count : 1
        default:
            return 0
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell!
        switch entity {
        case is XKPICKERVIEW:
            cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) 
            cell.textLabel?.text = (sources[column]["data"] as! Array)[indexPath.row] 
        case is XKNAV, is XKSEGMENT, is XKTAB:
            let item = items[indexPath.section]
            let attributeName = item.attributes[indexPath.row]
            let identifier = getCellIdentifier(attributeName)
            switch identifier {
            case "TextCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! TextCell
                (cell as! TextCell).currentWidget = currentWidget
                (cell as! TextCell).attributeName.text = attributeName
                switch item {
                case is XKNAV_ITEM:
                    if (item as! XKNAV_ITEM).side == "left" {
                        (cell as! TextCell).setAttributeValue((cell as! TextCell), attr:attributeName, value: item.valueForKey(attributeName)!, item_index:0)
                    }
                    else {
                        (cell as! TextCell).setAttributeValue((cell as! TextCell), attr:attributeName, value: item.valueForKey(attributeName)!, item_index:1)
                    }
                default:
                    (cell as! TextCell).setAttributeValue((cell as! TextCell), attr:attributeName, value: item.valueForKey(attributeName)!, item_index:indexPath.section)
                }

            case "ImageCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! ImageCell
                (cell as! ImageCell).currentWidget = currentWidget
                (cell as! ImageCell).attributeName.text = attributeName
                (cell as! ImageCell).setAttributeValue((cell as! ImageCell), attr:attributeName, value: item.valueForKey(attributeName))
            case "StyleSelectCell":
                cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! StyleSelectCell
                (cell as! StyleSelectCell).currentWidget = currentWidget
                (cell as! StyleSelectCell).attributeName.text = attributeName
                (cell as! StyleSelectCell).setAttributeValue((cell as! StyleSelectCell), attr:attributeName, value: item.valueForKey(attributeName)!, item_index:indexPath.section)
            default:
                cell = tableView.dequeueReusableCellWithIdentifier(identifier) as! TextSelectCell
                (cell as! TextSelectCell).currentWidget = currentWidget
                (cell as! TextSelectCell).attributeName.text = attributeName
                (cell as! TextSelectCell).setAttributeValue((cell as! TextSelectCell), attr:attributeName, value: item.valueForKey(attributeName)!)
            }
        default:
            print("generate tableview cell error")
        }

        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.reuseIdentifier == "ImageCell" {
            NSNotificationCenter.defaultCenter().postNotificationName("popOverImagePicker", object: nil)
            column = indexPath.section
        }
    }
    
    func setItemImage() {
        switch entity {
        case is XKNAV:
            let item = items[column] as! XKNAV_ITEM
            if let data = datasource!.itemImageFromLibrary() {
                item.image = data
                item.text = ""
                let nav = currentWidget?.contentView.subviews[0] as! UINavigationBar
                guard let navItems = nav.items else {
                    return
                }
                let topItem = navItems[0]
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: barButtonSize, height: barButtonSize))
                button.setImage(UIImage(data: data), forState: .Normal)
                button.imageEdgeInsets = UIEdgeInsetsMake(5.0, 0, -5.0, 0)
                if item.side == "left" {
                    topItem.setLeftBarButtonItem(UIBarButtonItem(customView: button), animated: true)
                }
                else {
                    topItem.setRightBarButtonItem(UIBarButtonItem(customView: button), animated: true)
                }
            }
        case is XKTAB:
            let item = items[column] as! XKTAB_ITEM
            if let data = datasource!.itemImageFromLibrary() {
                item.image = data
                let tab = currentWidget?.contentView.subviews[0] as! UITabBar
                var tabItems = tab.items as [UITabBarItem]!
                tabItems[column] = UITabBarItem(title: item.text, image: imageResize(image: UIImage(data: data)!, sizeChange: CGSize(width: barButtonSize, height: barButtonSize)) , tag: column)
                tab.setItems(tabItems, animated: true)
            }

        case is XKSEGMENT:
            let item = items[column] as! XKSEGMENT_ITEM
            if let data = datasource!.itemImageFromLibrary() {
                item.image = data
                let segment = currentWidget?.contentView.subviews[0] as! UISegmentedControl
                segment.setImage(UIImage(data: data), forSegmentAtIndex: column)
            }
        default:
            print("setItemImage error.")
        }
        
        tableView.reloadData()
    }
    
    func imageResize (image image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch entity {
        case is XKSEGMENT, is XKTAB:
            return "ITEM \(section + 1)"
        case is XKNAV:
            let item = items[section] as! XKNAV_ITEM
            return "\(item.side.uppercaseString) ITEM"
        default:
            return ""
        }
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            let entity = currentWidget?.CDObject as! NATIVE_WIDGET

            var data = sources[column]["data"] as! Array<String>
            data.removeAtIndex(indexPath.row)
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            entity.setValue(sources, forKey: navBar.title!)
            CoreDataUtil.sharedInstance.save()
            NSNotificationCenter.defaultCenter().postNotificationName("updatePickerDataSource", object: nil, userInfo: ["entity": entity])
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        // remove the dragged row's model
        var data = sources[column]["data"] as! Array<String>
        let val = data.removeAtIndex(fromIndexPath.row)
        
        // insert it into the new position
        data.insert(val, atIndex: toIndexPath.row)
        
        entity.setValue(sources, forKey: navBar.title!)
        CoreDataUtil.sharedInstance.save()
        
        switch entity {
        case is XKPICKERVIEW:
            NSNotificationCenter.defaultCenter().postNotificationName("updatePickerDataSource", object: nil, userInfo: ["entity": entity])
        default:
            print("NSNotificationCenter error.")
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch entity {
        case is XKSEGMENT, is XKTAB:
            let sectionView = UIView.loadFromNibNamed("PropertySectionView")! as! PropertySectionView
            sectionView.delegate = self
            sectionView.sectionName.text = "Item \(section + 1)"
            sectionView.tag = section
            if items.count == 1 {
                sectionView.removeButton.hidden = true
            }
            return sectionView
        case is XKNAV:
            let item = items[section] as! XKNAV_ITEM
            let sectionView = UIView.loadFromNibNamed("PropertySectionView")! as! PropertySectionView
            sectionView.delegate = self
            sectionView.sectionName.text = item.side == "left" ? "LEFT ITEM" : "RIGHT ITEM"
            sectionView.tag = section
            return sectionView
        default:
            return nil
        }
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    @IBAction func SwitchColumnAction(sender: UISegmentedControl) {
        switch entity {
        case is XKPICKERVIEW:
            column = sender.selectedSegmentIndex
        default:
            print("change columnControl error.")
        }
        tableView.reloadData()
    }
    
    @IBAction func NewSourceAction(sender: UIButton) {
        switch entity {
        case is XKPICKERVIEW:
            if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: nil, message: "Please enter the source content.", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
                    textField.placeholder = "New ..."
                }
                let okAction = UIAlertAction(title: "Ok", style: .Destructive) { (_) in
                    let sourceTextField = alertController.textFields![0]
                    var data = (self.sources[self.column]["data"] as! Array<String>)
                    data.append(sourceTextField.text!)
                    self.sources[self.column]["data"] = data
                    self.tableView.reloadData()
                    
                    self.entity.setValue(NSSet(array: self.sources), forKey: self.navBar.title!)
                    CoreDataUtil.sharedInstance.save()
                    
                    guard let subviews = self.currentWidget?.contentView.subviews else {
                        return
                    }
                    for obj in subviews as [UIView] {
                        if obj is UIPickerView {
                            NSNotificationCenter.defaultCenter().postNotificationName("updatePickerDataSource", object: nil, userInfo: ["entity": self.entity])
                        }
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
                alertController.addAction(cancelAction)
                alertController.addAction(okAction)
                
                self.presentViewController(alertController, animated: true) { () -> Void in
                }

            } else {
                // Fallback on earlier versions
            }
        case is XKSEGMENT:
            let widget = currentWidget?.contentView.subviews[0] as! UISegmentedControl
            WidgetUtil.sharedInstance.generateSegmentItem(widget, element: entity as! XKSEGMENT)
            items = (entity as! XKSEGMENT).items.allObjects as! [XKSEGMENT_ITEM]
            items.sortInPlace {($0 as! XKSEGMENT_ITEM).index.integerValue < ($1 as! XKSEGMENT_ITEM).index.integerValue}
            (entity as! XKSEGMENT).itemCount = items.count
            tableView.reloadData()
        case is XKTAB:
            let widget = currentWidget?.contentView.subviews[0] as! UITabBar
            var tabItems = widget.items as [UITabBarItem]!
            let itemWidth = screenWidth / CGFloat(items.count)
            tabItems.append(WidgetUtil.sharedInstance.generateTabBarItem(widget, element: entity as! XKTAB, index: widget.items!.count, item_width: itemWidth))
            widget.setItems(tabItems, animated: true)
            items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
            items.sortInPlace {($0 as! XKTAB_ITEM).index.integerValue < ($1 as! XKTAB_ITEM).index.integerValue}
            (entity as! XKTAB).itemCount = items.count
            tableView.reloadData()
        default:
            print("")
        }
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ToStyleSelect" {
            let VC = segue.destinationViewController as! StyleSelectCollectionViewController
            VC.currentWidget = currentWidget
            let cell = sender as! StyleSelectCell
            VC.column = cell.tag
            VC.navBar.title = cell.attributeName.text
            print("ToStyleSelect")
        }
    }

}
