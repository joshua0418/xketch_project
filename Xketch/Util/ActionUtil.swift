//
//  ActionUtil.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/2.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

private let _SharedInstance = ActionUtil()

class ActionUtil: NSObject, UITabBarDelegate, UITableViewDelegate, UICollectionViewDelegate {
    var controller: EditorViewController?
    
    override init() {
        super.init()
    }
    
    class var sharedInstance: ActionUtil {
        return _SharedInstance
    }
    
    func assignWidgetAction(widget: XKView, page: PAGE, event: SegueEvent, type: SegueType?, index: Int) {
        let entity = widget.CDObject as! NATIVE_WIDGET
        var action: ACTION?
        
        switch entity {
        case is XKBUTTON, is XKTABLEVIEW, is XKIMAGEVIEW:
            if let existAction = entity.action {
                action = existAction
                action!.event = event.rawValue
                action!.type = type != nil ? type!.rawValue : action!.type
                action!.targetPage = page
            }
            else {
                action = NSEntityDescription.insertNewObjectForEntityForName("ACTION", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as? ACTION
                action!.event = event.rawValue
                action!.type = type != nil ? type!.rawValue : SegueType.Cross.rawValue
                action!.targetPage = page
            }
        default:
            break
        }
        
        CoreDataUtil.sharedInstance.save()
        
        switch entity {
        case is XKBUTTON:
            let object: UIButton = widget.contentView.subviews[0] as! UIButton
            switch event {
            case .TouchUpInside:
                object.addTarget(self, action: "TouchUpInside:", forControlEvents: UIControlEvents.TouchUpInside)
            }
            entity.action = action!
            object.actionInfo = action!
        case is XKNAV:
            let bar: UINavigationBar = widget.contentView.subviews[0] as! UINavigationBar
            let topItem = bar.topItem
            var items = (entity as! XKNAV).items.allObjects as! [XKNAV_ITEM]
            items.sortInPlace {$0.side < $1.side}
            
            let item = items.count == 2 ? items[index] : items[0]
            if let existAction = item.action {
                action = existAction
                action!.event = event.rawValue
                action!.type = type != nil ? type!.rawValue : action!.type
                action!.targetPage = page
            }
            else {
                action = NSEntityDescription.insertNewObjectForEntityForName("ACTION", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as? ACTION
                action!.event = event.rawValue
                action!.type = type != nil ? type!.rawValue : SegueType.Cross.rawValue
                action!.targetPage = page
            }
            
            if item.style.integerValue == 0 {
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: barButtonSize, height: barButtonSize))
                if item.side == "left" {
                    if let data = item.image as NSData? {
//                        topItem?.leftBarButtonItem = UIBarButtonItem(image: UIImage(data: data), style: .Plain, target: self, action: "TouchUpInside:")
                        button.setImage(UIImage(data: data), forState: .Normal)
                        button.imageEdgeInsets = UIEdgeInsetsMake(5.0, 0, -5.0, 0)

                    }
                    else {
//                        topItem?.leftBarButtonItem = UIBarButtonItem(title: item.text, style: .Plain, target: self, action: "TouchUpInside:")
                        button.frame.size.width = 60
                        button.setTitle(item.text, forState: .Normal)
                        button.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    }
                    button.addTarget(self, action: "TouchUpInside:", forControlEvents: UIControlEvents.TouchUpInside)
                    topItem!.setLeftBarButtonItem(UIBarButtonItem(customView: button), animated: false)
//                    topItem?.leftBarButtonItem?.actionInfo = action!
                    button.actionInfo = action!
                }
                else {
                    if let data = item.image as NSData? {
//                        topItem?.rightBarButtonItem = UIBarButtonItem(image: UIImage(data: data), style: .Plain, target: self, action: "TouchUpInside:")
                        button.setImage(UIImage(data: data), forState: .Normal)
                        button.imageEdgeInsets = UIEdgeInsetsMake(5.0, 0, -5.0, 0)
                    }
                    else {
//                        topItem?.rightBarButtonItem = UIBarButtonItem(title: item.text, style: .Plain, target: self, action: "TouchUpInside:")
                        button.frame.size.width = 60
                        button.setTitle(item.text, forState: .Normal)
                        button.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    }
                    button.addTarget(self, action: "TouchUpInside:", forControlEvents: UIControlEvents.TouchUpInside)
                    topItem!.setRightBarButtonItem(UIBarButtonItem(customView: button), animated: false)
//                    topItem?.rightBarButtonItem?.actionInfo = action!
                    button.actionInfo = action!
                }
            }
            else {
                let type = item.style.integerValue < 6 ? item.style.integerValue - 1 : item.style.integerValue + 1
                if item.side == "left" {
                    topItem?.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem(rawValue: type)!, target: self, action: "TouchUpInside:")
                    topItem?.leftBarButtonItem?.actionInfo = action!
                }
                else {
                    topItem?.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem(rawValue: type)!, target: self, action: "TouchUpInside:")
                    topItem?.rightBarButtonItem?.actionInfo = action!
                }
            }
            item.action = action!
        case is XKTAB:
            let tab: UITabBar = widget.contentView.subviews[0] as! UITabBar
            var barItems:Array<UITabBarItem>! = tab.items as [UITabBarItem]?
            var items = (entity as! XKTAB).items.allObjects as! [XKTAB_ITEM]
            items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
            
            if let existAction = items[index].action {
                action = existAction
                action!.event = event.rawValue
                action!.type = type != nil ? type!.rawValue : action!.type
                action!.targetPage = page
            }
            else {
                action = NSEntityDescription.insertNewObjectForEntityForName("ACTION", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as? ACTION
                action!.event = event.rawValue
                action!.type = type != nil ? type!.rawValue : SegueType.Cross.rawValue
                action!.targetPage = page
            }
            
            items[index].action = action!
            barItems[index].actionInfo = action!
        case is XKTABLEVIEW:
            let object: UITableView = widget.contentView.subviews[0] as! UITableView
            object.actionInfo = action!
            entity.action = action!
        case is XKIMAGEVIEW:
            if (entity as! XKIMAGEVIEW).style.integerValue == 2 {
                let object: UICollectionView = widget.contentView.subviews[0] as! UICollectionView
                object.actionInfo = action!
                entity.action = action!
            }
        default:
            print("no object")
        }
        
        CoreDataUtil.sharedInstance.save()
    }
    
    func TouchUpInside(sender: AnyObject) {
        var action: ACTION?
        switch sender {
        case is UIButton:
            if let existAction = (sender as! UIButton).actionInfo {
                action = existAction
            }
        case is UIBarButtonItem:
            if let existAction = (sender as! UIBarButtonItem).actionInfo {
                action = existAction
            }
        default:
            break
        }
        
        if let action = action {
            controller?.changePage(page: action.targetPage, type: SegueType(rawValue: action.type.integerValue)!)
        }
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        if let action = item.actionInfo {
            controller?.changePage(page: action.targetPage, type: SegueType(rawValue: action.type.integerValue)!)
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let action = tableView.actionInfo {
            controller?.changePage(page: action.targetPage, type: SegueType(rawValue: action.type.integerValue)!)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let action = collectionView.actionInfo {
            controller?.changePage(page: action.targetPage, type: SegueType(rawValue: action.type.integerValue)!)
        }
    }
}
