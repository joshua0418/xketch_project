//
//  ExtensionUtil.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/13.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//


import Foundation
import CoreData

extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
    
    func getUnderViewByPoint(view: UIView, point:CGPoint, subviews: [UIView]) -> UIView? {
        for view in Array(subviews.reverse()) {
            let frame = self.convertRect(view.frame, fromView: view.superview)
            
            if CGRectContainsPoint(frame, point) {
                return view
            }
        }
        return nil
    }
    
    func onFloating(color: UIColor) {
        self.layer.shadowOpacity = 0.8
        self.layer.shadowColor = color.CGColor
        self.layer.shadowRadius = 4
        self.layer.shadowOffset = CGSize(width: 4, height: 4)
    }
    
    func onSinking() {
        self.layer.shadowOpacity = 0.0
        self.layer.shadowColor = UIColor.clearColor().CGColor
        self.layer.shadowRadius = 0
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    func viewCurlUp(animationTime animationTime:Float)
    {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
        UIView.setAnimationDuration(NSTimeInterval(animationTime))
        UIView.setAnimationTransition(UIViewAnimationTransition.CurlUp, forView: self, cache: false)
        
        UIView.commitAnimations()
    }
    
    func viewCurlDown(animationTime animationTime:Float)
    {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
        UIView.setAnimationDuration(NSTimeInterval(animationTime))
        UIView.setAnimationTransition(UIViewAnimationTransition.CurlDown, forView: self, cache: false)
        
        UIView.commitAnimations()
    }
    
    func viewFlipFromLeft(animationTime animationTime:Float)
    {
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseInOut)
        UIView.setAnimationDuration(NSTimeInterval(animationTime))
        UIView.setAnimationTransition(UIViewAnimationTransition.FlipFromLeft, forView: self, cache: false)
        UIView.commitAnimations()
    }
    
    func viewMoveInFromTop(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "moveIn"
        animation.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        animation.subtype = "fromBottom"
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewMoveInFromBottom(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "moveIn"
        animation.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        animation.subtype = "fromTop"
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewMoveInFromRight(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "moveIn"
        animation.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        animation.subtype = "fromLeft"
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewMoveInFromLeft(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "moveIn"
        animation.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        animation.subtype = "fromRight"
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewPushFromLeft(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "push"
        animation.timingFunction = CAMediaTimingFunction(name: "easeOut")
        animation.subtype = "fromLeft"
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewPushFromRight(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "push"
        animation.timingFunction = CAMediaTimingFunction(name: "easeOut")
        animation.subtype = "fromRight"
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewFadeIn(animationTime animationTime:Float)
    {
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "fade"
        animation.timingFunction = CAMediaTimingFunction(name: "easeIn")
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }
    
    func viewFadeOut(animationTime animationTime:Float){
        let animation:CATransition = CATransition()
        animation.duration = CFTimeInterval(animationTime)
        animation.type = "fade"
        animation.timingFunction = CAMediaTimingFunction(name: "easeOut")
        animation.fillMode = "forwards"
        self.layer.addAnimation(animation, forKey: nil)
    }

}

extension UIColor {
    class func iOSblueColor() -> UIColor {
        return UIColor(red: 0, green: 122.0 / 255.0, blue: 1, alpha: 1)
    }
    
    class func iOSredColor() -> UIColor {
        return UIColor(red: 0, green: 0.22, blue: 122.0 / 255.0, alpha: 1)
    }
}

extension UIImage {
    class func imageResize (image image:UIImage?, sizeChange:CGSize)-> UIImage?{
        if let image = image {
            let hasAlpha = true
            let scale: CGFloat = 0.0 // Use scale factor of main screen
            
            UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
            image.drawInRect(CGRect(origin: CGPointZero, size: sizeChange))
            
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            return scaledImage
        }
        else {
            return nil
        }
    }
    
    class func imageWithView(view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0)
        view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

private var actionAssociationKey: UInt8 = 0
private var indexAssociationKey: UInt8 = 1

extension UIButton {
    var actionInfo: ACTION! {
        get {
            return objc_getAssociatedObject(self, &actionAssociationKey) as? ACTION
        }
        set(newValue) {
            objc_setAssociatedObject(self, &actionAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UIBarButtonItem {
    var actionInfo: ACTION! {
        get {
            return objc_getAssociatedObject(self, &actionAssociationKey) as? ACTION
        }
        set(newValue) {
            objc_setAssociatedObject(self, &actionAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UITabBarItem {
    var actionInfo: ACTION! {
        get {
            return objc_getAssociatedObject(self, &actionAssociationKey) as? ACTION
        }
        set(newValue) {
            objc_setAssociatedObject(self, &actionAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UITableView {
    var actionInfo: ACTION! {
        get {
            return objc_getAssociatedObject(self, &actionAssociationKey) as? ACTION
        }
        set(newValue) {
            objc_setAssociatedObject(self, &actionAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    var lastIndex: Int! {
        get {
            return objc_getAssociatedObject(self, &indexAssociationKey) as? Int
        }
        set(newValue) {
            objc_setAssociatedObject(self, &indexAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UICollectionView {
    var actionInfo: ACTION! {
        get {
            return objc_getAssociatedObject(self, &actionAssociationKey) as? ACTION
        }
        set(newValue) {
            objc_setAssociatedObject(self, &actionAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension CGPoint {
    
    static func distanceBetweenPoint(point: CGPoint, toPoint: CGPoint) -> Double {
        let dx = Double(toPoint.x - point.x)
        let dy = Double(toPoint.y - point.y)
        return sqrt(dx * dx + dy * dy)
    }
}

extension PROJECT {
    var incrementalCounter: Int! {
        get {
            if let value = objc_getAssociatedObject(self, &indexAssociationKey) as? Int {
            }
            else {
                objc_setAssociatedObject(self, &indexAssociationKey, 1, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return objc_getAssociatedObject(self, &indexAssociationKey) as? Int
        }
        set(newValue) {
            objc_setAssociatedObject(self, &indexAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension NSManagedObject {
    func objectWithID() -> String {
        let objDesc = self.objectID.description
        var str: Array<String> = objDesc.componentsSeparatedByString("/")
        let objName = str[str.count - 2] + "/" + str[str.count - 1].substringWithRange(Range<String.Index>(start: str[str.count - 1].startIndex, end: str[str.count - 1].endIndex.advancedBy(-1)))
        return objName
    }
}