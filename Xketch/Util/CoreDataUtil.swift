//
//  CoreDataUtil.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/7.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

private let _SharedInstance = CoreDataUtil()

class CoreDataUtil: NSObject {
    
    var stack: Array<Dictionary<String, AnyObject>>?
    
    // MARK: - General Function
    
    class var sharedInstance: CoreDataUtil {
        return _SharedInstance
    }
    
    lazy var managedObjectContext : NSManagedObjectContext? = {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if let managedObjectContext = appDelegate.managedObjectContext {
            return managedObjectContext
        }
        else {
            return nil
        }
        }()
    
    func fetchEntity(name: String) -> [NSManagedObject] {
        let fetchRequest = NSFetchRequest(entityName: name)
        
        do {
            let fetchResults = try managedObjectContext!.executeFetchRequest(fetchRequest)
            return fetchResults as! [NSManagedObject]
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return []
    }

    func save() {
        do {
           try managedObjectContext!.save()
            NSNotificationCenter.defaultCenter().postNotificationName("updateUndoState", object: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("abort")
            abort()
        }        
    }
    
    func clearEntity(name: String) {
        let fetchRequest = NSFetchRequest(entityName: name)
        
        if let fetchResults = try? managedObjectContext!.executeFetchRequest(fetchRequest) {
            for item in fetchResults as! [NSManagedObject] {
                managedObjectContext!.deleteObject(item)
            }
        }
        save()
    }
    
    func deleteCDObject(view: XKView) {
        if let action = (view.CDObject as! NATIVE_WIDGET).action {
            managedObjectContext!.deleteObject(action)
        }
        managedObjectContext!.deleteObject(view.CDObject)
        save()
    }
    
    func deleteWidget(widget widget: NATIVE_WIDGET) {
        if let action = widget.action {
            managedObjectContext!.deleteObject(action)
        }
        managedObjectContext!.deleteObject(widget)
        save()
    }
    
    func deleteAction(action: ACTION) {
        managedObjectContext!.deleteObject(action)
        save()
    }
    
    func getWidgetWithID(objectID: NSManagedObjectID) -> NATIVE_WIDGET {
        return managedObjectContext!.objectWithID(objectID) as! NATIVE_WIDGET
    }
    
    // MARK: - New NSManagedObject Function
    
    func insertPROJECT(projectName: String) {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("PROJECT", inManagedObjectContext: self.managedObjectContext!) as! PROJECT
        newItem.name = projectName
        save()
        print("project's index:\(newItem.index)")
        
        insertPAGE("Home Page", project: newItem)
    }
    
    func insertPAGE(pageName: String, project:PROJECT) {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("PAGE", inManagedObjectContext: self.managedObjectContext!) as! PAGE
        newItem.name = pageName
        newItem.index = project.pages.count
        newItem.project = project
        save()
    }
    
    func deletePROJECT(project: PROJECT) {
        managedObjectContext!.deleteObject(project)
        save()
    }
    
    func deletePAGE(page: PAGE) {
        managedObjectContext!.deleteObject(page)
        save()
    }
    
    func undoAction() {
        managedObjectContext!.undoManager?.undo()
        save()
    }
    
    func redoAction() {
        managedObjectContext!.undoManager?.redo()
        save()
    }
}
