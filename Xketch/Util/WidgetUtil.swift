//
//  WidgetUtil.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/11/20.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import MapKit
import CoreData

// iPad=264ppi, iPhone=326ppi

let screenWidth:CGFloat = 259   // 320px
let screenHeight:CGFloat = 460  // 568px
let statusBarHeight:CGFloat = 17    // 20px
let navigationBarHeight:CGFloat = 35    // 44px
let tabBarOriginY:CGFloat = 421 // 519px
let tabBarHeight:CGFloat = 39   // 49px
let labelHeight:CGFloat = 17    // 21px
let buttonHeight:CGFloat = 24   // 30px
let sliderWidth:CGFloat = 100   // 123px
let sliderHeight:CGFloat = 25   // 31px
let switchWidth:CGFloat = 42    // 51px
let switchHeight:CGFloat = 25   // 31px
let segmentHeight:CGFloat = 24  // 29px
let segmentWidth:CGFloat = 96   // 118px
let pickerHeight:CGFloat = 178  // 219px
let barButtonSize:CGFloat = 18  // 22px

enum SegueType: Int {
    case Push = 0, Pop, Cover, Flip, Cross, Partial
    
    static var count: Int {return SegueType.Partial.hashValue + 1}
    func toString() -> String {
        switch self {
        case .Push:
            return "Push"
        case .Pop:
            return "Pop"
        case .Cover:
            return "Cover"
        case .Flip:
            return "Flip"
        case .Cross:
            return "Cross"
        case .Partial:
            return "Partial"
        }
    }
}

enum SegueEvent: Int {
    case TouchUpInside = 0
    
    static var count: Int {return SegueEvent.TouchUpInside.hashValue + 1}
    func toString() -> String {
        switch self {
        case .TouchUpInside:
            return "TouchUpInside"
        }
    }
}

private let _SharedInstance = WidgetUtil()

class WidgetUtil: NSObject, CLLocationManagerDelegate {
   
    // MARK:
    // MARK: PARAMETER DEFINE
    
    var result: DollarResult = DollarResult()
    var baseView: XKView!
    var locationManager: CLLocationManager!
    var location:CLLocation?
    var currentPage:PAGE!
    var isDuplicate: Bool = false
    
    override init() {
        super.init()
        initLocationManager()
    }
    
    // MARK:
    // MARK: SINGLETON
    
    class var sharedInstance: WidgetUtil {
        return _SharedInstance
    }
    
    // MARK:
    // MARK: METHOD
    
    func initLocationManager() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getCurrentLocation:", name: "getCurrentLocation", object: nil)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if #available(iOS 8.0, *) {
            locationManager.requestAlwaysAuthorization()
        } else {
            // Fallback on earlier versions
        }
        locationManager.startUpdatingLocation()
    }
    
    
    // alternative type provided right side
    func getAlterableTypes(stroke:String) ->Array<String> {
        switch stroke {
        case "line":
            return ["Label", "Button", "TextField", "Horizontal-Line", "View"]
        case "rect":
//            return ["Canvas", "Image", "TextView"]
            return ["View", "Image", "TextView"]
        case "cross":
            return ["Segment Control", "Switch", "Slider", "View"]
        default:
            return []
        }
    }
    
    func initTextAttribute(element: NATIVE_WIDGET, obj: AnyObject) {
        element.page = currentPage
        element.name = baseView.widgetName
        element.frame = NSValue(CGRect: baseView.frame)
        element.alpha = 1
        switch obj {
        case is UILabel:
            element.fontColor = (obj as! UILabel).textColor
            element.backgroundColor = (obj as! UILabel).backgroundColor!
        case is UITextView:
            element.fontColor = UIColor.blackColor()
            element.backgroundColor = (obj as! UITextView).backgroundColor!
        default:
            element.fontColor = UIColor.blackColor()
            element.backgroundColor = UIColor.whiteColor()
        }
    }
    
    func initGraphicAttribute(element: NATIVE_WIDGET, obj: UIView) {
        element.page = currentPage
        element.name = baseView.widgetName
        element.frame = NSValue(CGRect: baseView.frame)
        if let bgColor = obj.backgroundColor as UIColor? {
            element.backgroundColor = bgColor
        }
        else {
            element.backgroundColor = UIColor.clearColor()
            (obj as UIView).backgroundColor = UIColor.clearColor()
        }

        element.alpha = 1
        
    }
    
    func initActionAttribute(element: NATIVE_WIDGET, obj: AnyObject) {
        element.page = currentPage
        element.name = baseView.widgetName
        element.frame = NSValue(CGRect: baseView.frame)
        switch obj {
        case is UIButton:
            element.fontColor = (obj as! UIButton).titleColorForState(UIControlState.Normal)!
            element.backgroundColor = UIColor.whiteColor()
        case is UITextField:
            element.fontColor = (obj as! UITextField).textColor!
            element.backgroundColor = UIColor.clearColor()
        case is UISegmentedControl:
            element.fontColor = (obj as! UISegmentedControl).tintColor
            element.backgroundColor = UIColor.whiteColor()
        default:
            element.fontColor = UIColor.blackColor()
            element.backgroundColor = UIColor.clearColor()
        }
        element.enabled = 1
    }
    
//    func saveelement(element: NATIVE_WIDGET, obj:AnyObject) {
//        print("element is \(element)")
//        print("obj is \(obj)")
//        
//        switch obj {
////        case is UILabel:
////            (obj as! UILabel).alpha = CGFloat(element.alpha)
////            (obj as! UILabel).backgroundColor = element.backgroundColor as? UIColor
////            (obj as! UILabel).textColor = element.fontColor as! UIColor
////            // fontStyle design not yet
////        case is UIButton:
////            (obj as! UIButton).alpha = CGFloat(element.alpha)
////            (obj as! UIButton).backgroundColor = element.backgroundColor as? UIColor
////            (obj as! UIButton).setTitleColor(element.fontColor as? UIColor, forState: .Normal)
////            (obj as! UIButton).enabled = element.enabled.boolValue
////        case is UITextField:
////            (obj as! UITextField).alpha = CGFloat(element.alpha)
////            (obj as! UITextField).backgroundColor = element.backgroundColor as? UIColor
////            (obj as! UITextField).textColor = element.fontColor as? UIColor
////        case is UISwitch:
////            (obj as! UISwitch).alpha = CGFloat(element.alpha)
////            (obj as! UISwitch).backgroundColor = element.backgroundColor as? UIColor
////            (obj as! UISwitch).enabled = element.enabled.boolValue
////        case is UISlider:
////            (obj as! UISlider).alpha = CGFloat(element.alpha)
////            (obj as! UISlider).backgroundColor = element.backgroundColor as? UIColor
////            (obj as! UISlider).enabled = element.enabled.boolValue
////        case is UISegmentedControl:
////            (obj as! UISegmentedControl).alpha = CGFloat(element.alpha)
////            (obj as! UISegmentedControl).backgroundColor = element.backgroundColor as? UIColor
////            (obj as! UISegmentedControl).enabled = element.enabled.boolValue
////        case is UITableView:
////            (obj as! UITableView).alpha = CGFloat(element.alpha)
////            (obj as! UITableView).backgroundColor = element.backgroundColor as? UIColor
////        case is UIWebView:
////            (obj as! UIWebView).alpha = CGFloat(element.alpha)
////            (obj as! UIWebView).backgroundColor = element.backgroundColor as? UIColor
////        case is MKMapView:
////            (obj as! MKMapView).alpha = CGFloat(element.alpha)
////            (obj as! MKMapView).backgroundColor = element.backgroundColor as? UIColor
////        case is UITextView:
////            (obj as! UITextView).alpha = CGFloat(element.alpha)
////            (obj as! UITextView).backgroundColor = element.backgroundColor as? UIColor
////        case is UIImageView:
////            (obj as! UIImageView).alpha = CGFloat(element.alpha)
////            (obj as! UIImageView).backgroundColor = element.backgroundColor as? UIColor
////        case is UINavigationBar:
////            (obj as! UINavigationBar).alpha = CGFloat(element.alpha)
////            (obj as! UINavigationBar).backgroundColor = element.backgroundColor as? UIColor
////        case is UITabBar:
////            (obj as! UITabBar).alpha = CGFloat(element.alpha)
////            (obj as! UITabBar).backgroundColor = element.backgroundColor as? UIColor
//        case is UIImageView:
//            print("ImageView")
//            
////        case is UICollectionView:
////            (obj as! UICollectionView).alpha = CGFloat(element.alpha)
////            (obj as! UICollectionView).backgroundColor = element.backgroundColor as? UIColor
//        case is UITextView:
//            print("TextView")
//        case is CanvasView:
//            print("CanvasView")
////        case is UIPickerView:
////            (obj as! UIPickerView).alpha = CGFloat(element.alpha)
////            (obj as! UIPickerView).backgroundColor = element.backgroundColor as? UIColor
//        case is UIView:
//            print("View")
//            print(element.attributes.count)
//        default:
//            print("????")
//        }
//    }
    
    func setBasicAttribute(element: NATIVE_WIDGET, obj: AnyObject) {
        switch obj {
        case is UILabel:
            (obj as! UILabel).alpha = CGFloat(element.alpha)
            (obj as! UILabel).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UILabel).textColor = element.fontColor as! UIColor
            // fontStyle design not yet
        case is UIButton:
            (obj as! UIButton).alpha = CGFloat(element.alpha)
            (obj as! UIButton).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UIButton).setTitleColor(element.fontColor as? UIColor, forState: .Normal)
            (obj as! UIButton).enabled = element.enabled.boolValue
        case is UITextField:
            (obj as! UITextField).alpha = CGFloat(element.alpha)
            (obj as! UITextField).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UITextField).textColor = element.fontColor as? UIColor
        case is UISwitch:
            (obj as! UISwitch).alpha = CGFloat(element.alpha)
            (obj as! UISwitch).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UISwitch).enabled = element.enabled.boolValue
        case is UISlider:
            (obj as! UISlider).alpha = CGFloat(element.alpha)
            (obj as! UISlider).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UISlider).enabled = element.enabled.boolValue
        case is UISegmentedControl:
            (obj as! UISegmentedControl).alpha = CGFloat(element.alpha)
            (obj as! UISegmentedControl).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UISegmentedControl).enabled = element.enabled.boolValue
        case is UITableView:
            (obj as! UITableView).alpha = CGFloat(element.alpha)
            (obj as! UITableView).backgroundColor = element.backgroundColor as? UIColor
        case is UIWebView:
            (obj as! UIWebView).alpha = CGFloat(element.alpha)
            (obj as! UIWebView).backgroundColor = element.backgroundColor as? UIColor
        case is MKMapView:
            (obj as! MKMapView).alpha = CGFloat(element.alpha)
            (obj as! MKMapView).backgroundColor = element.backgroundColor as? UIColor
        case is UITextView:
            (obj as! UITextView).alpha = CGFloat(element.alpha)
            (obj as! UITextView).backgroundColor = element.backgroundColor as? UIColor
        case is UIImageView:
            (obj as! UIImageView).alpha = CGFloat(element.alpha)
            (obj as! UIImageView).backgroundColor = element.backgroundColor as? UIColor
        case is UINavigationBar:
            (obj as! UINavigationBar).alpha = CGFloat(element.alpha)
            (obj as! UINavigationBar).backgroundColor = element.backgroundColor as? UIColor
        case is UITabBar:
            (obj as! UITabBar).alpha = CGFloat(element.alpha)
            (obj as! UITabBar).backgroundColor = element.backgroundColor as? UIColor
        case is UIImageView:
            (obj as! UIImageView).alpha = CGFloat(element.alpha)
            (obj as! UIImageView).backgroundColor = element.backgroundColor as? UIColor
        case is UICollectionView:
            (obj as! UICollectionView).alpha = CGFloat(element.alpha)
            (obj as! UICollectionView).backgroundColor = element.backgroundColor as? UIColor
        case is UITextView:
            (obj as! UITextView).alpha = CGFloat(element.alpha)
            (obj as! UITextView).backgroundColor = element.backgroundColor as? UIColor
            (obj as! UITextView).textColor = element.fontColor as? UIColor
        case is CanvasView:
            (obj as! CanvasView).alpha = CGFloat(element.alpha)
            (obj as! CanvasView).backgroundColor = element.backgroundColor as? UIColor
        case is UIPickerView:
            (obj as! UIPickerView).alpha = CGFloat(element.alpha)
            (obj as! UIPickerView).backgroundColor = element.backgroundColor as? UIColor
        case is UIView:
            (obj as! UIView).alpha = CGFloat(element.alpha)
            (obj as! UIView).backgroundColor = element.backgroundColor as? UIColor
        default:
            print("????")
        }
        
    }
    
    // MARK:
    // MARK: LINE
    
    func generateNavigationBar() -> XKView {
        baseView = XKView(frame: CGRectMake(0, statusBarHeight, screenWidth, navigationBarHeight))
        let object:UINavigationBar = UINavigationBar(frame: CGRectMake(0, 0, screenWidth, navigationBarHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Navigation"
        baseView.isResize = false
        baseView.isMove = false
        baseView.resizeHorizontal.hidden = true
        baseView.resizeVertical.hidden = true
        let item:UINavigationItem = UINavigationItem(title: "Title")
        object.setItems([item], animated: true)
        object.setTitleVerticalPositionAdjustment(5.0, forBarMetrics: .Default)

        let element = NSEntityDescription.insertNewObjectForEntityForName("XKNAV", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKNAV
        initTextAttribute(element, obj: object)
        element.fontStyle = 0
        element.title = item.title!
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()

        // set original position
//        baseView.frame = CGRect(x: 0, y: statusBarHeight, width: screenWidth, height: navigationBarHeight)
//        baseView.contentView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: navigationBarHeight)
        return baseView
    }
    
    func generateNavigationBarItem(bar:UINavigationItem, element:XKNAV , side:String) {
        let navItem: UIBarButtonItem = UIBarButtonItem()
        navItem.title = "item"
        if side == "right" {
            bar.rightBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 5), forBarMetrics: .Default)
            bar.setRightBarButtonItem(navItem, animated: true)
        }
        else if side == "left" {
            bar.leftBarButtonItem?.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 5), forBarMetrics: .Default)
            bar.setLeftBarButtonItem(navItem, animated: true)
        }
        
        let items:NSMutableSet = NSMutableSet(set: element.items)
        let item = NSEntityDescription.insertNewObjectForEntityForName("XKNAV_ITEM", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKNAV_ITEM
        item.name = "NavItem"
        item.updateAttributes()
        item.nav = element
        item.text = navItem.title!
        item.side = side
        item.style = 0
        item.page = currentPage
        let itemView = navItem.valueForKey("view") as! UIView?
        var frame = itemView!.frame
        frame.origin.y += statusBarHeight * 2
        item.frame = NSValue(CGRect: frame)
        items.addObject(item)
        
        element.items = NSSet(set: items)
        CoreDataUtil.sharedInstance.save()
    }
    
    func deleteNavigationBarItem(topItem:UINavigationItem, element:XKNAV_ITEM) {
        if element.side == "left" {
            topItem.leftBarButtonItem = nil
        }
        else {
            topItem.rightBarButtonItem = nil
        }
        CoreDataUtil.sharedInstance.deleteWidget(widget: element)
    }
    
    func generateLabel() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y , result.size.width, labelHeight))
        let object:UILabel = UILabel(frame: CGRectMake(0, 0, result.size.width, labelHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Label"
        baseView.alterableTypes = self.getAlterableTypes("line")
        object.text = "Label"
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKLABEL", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKLABEL
        initTextAttribute(element, obj: object)
        element.text = object.text!
        element.alignment = object.textAlignment.rawValue
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateButton() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, buttonHeight))
        let object:UIButton = UIButton(frame: CGRectMake(0, 0, result.size.width, buttonHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Button"
        baseView.alterableTypes = self.getAlterableTypes("line")
        object.setTitle("Button", forState: .Normal)
        object.setTitleColor(UIColor.blueColor(), forState: .Normal)
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKBUTTON", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKBUTTON
        initActionAttribute(element, obj: object)
        element.text = object.titleForState(UIControlState.Normal)!
        element.style = 0
        element.borderStyle = 0
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()

        return baseView
    }

    func generateTextField() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, buttonHeight))
        let object:UITextField = UITextField(frame: CGRectMake(0, 0, result.size.width, buttonHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "TextField"
        baseView.resizeVertical.hidden = true
        baseView.alterableTypes = self.getAlterableTypes("line")
        object.text = ""
        object.textAlignment = NSTextAlignment.Left
        object.borderStyle = .Bezel
        object.keyboardType = .Default
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKTEXTFIELD", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTEXTFIELD
        initActionAttribute(element, obj: object)
        element.text = object.text!
        element.alignment = object.textAlignment.rawValue
        element.style = object.borderStyle.rawValue
        element.keyboardStyle = object.keyboardType.rawValue
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateHR() -> XKView {
        let img = UIImage(named: "HR_line0")!
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, img.size.height))
        let object:UIImageView = UIImageView(frame: CGRectMake(0, 0, result.size.width, img.size.height))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Horizontal-Line"
        baseView.alterableTypes = self.getAlterableTypes("line")
        object.image = img
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKHR", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKHR
        initGraphicAttribute(element, obj: object)
        element.style = 0
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateTabBar(number:Int) -> XKView {
        baseView = XKView(frame: CGRectMake(0, tabBarOriginY, screenWidth, tabBarHeight))
        let object:UITabBar = UITabBar(frame: CGRectMake(0, 0, screenWidth, tabBarHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Tab"
        baseView.isResize = false
        baseView.isMove = false
        baseView.resizeHorizontal.hidden = true
        baseView.resizeVertical.hidden = true
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKTAB", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTAB
        initTextAttribute(element, obj: object)
        baseView.CDObject = element

        let itemWidth = screenWidth / CGFloat(number)
        var items: Array<UITabBarItem>! = []
        for var i=0; i<number; i++ {
            items.append(generateTabBarItem(object, element: element, index: i, item_width: itemWidth))
        }
        object.setItems(items, animated: true)
        object.selectedItem = items[0]
        object.delegate = ActionUtil.sharedInstance
        element.itemCount = items.count
        element.selected = 1
        
//        baseView.frame = CGRect(x: 0, y: tabBarOriginY, width: screenWidth, height: tabBarHeight)
//        baseView.contentView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: tabBarHeight)
        return baseView
    }
    
    func generateTabBarItem(bar:UITabBar, element:XKTAB, index:Int, item_width: CGFloat) -> UITabBarItem {
        let items:NSMutableSet = NSMutableSet(set: element.items)
        let tabItem: UITabBarItem = UITabBarItem(title: "item\(index+1)", image:nil , tag: index + 1)
        
        let item = NSEntityDescription.insertNewObjectForEntityForName("XKTAB_ITEM", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTAB_ITEM
        item.name = "TabItem"
        item.updateAttributes()
        item.tab = element
        item.text = tabItem.title!
        item.index = NSNumber(integer: index + 1)
        item.style = 0
        item.page = currentPage
        item.frame = NSValue(CGRect: CGRect(x: item_width * CGFloat(index), y: tabBarOriginY - statusBarHeight, width: item_width, height: tabBarHeight))
        items.addObject(item)
        
        return tabItem
    }
    
    func deleteTabBarItem(bar:UITabBar, element:XKTAB_ITEM) {
        bar.items?.removeAtIndex(element.index.integerValue - 1)
        CoreDataUtil.sharedInstance.deleteWidget(widget: element)
    }
    
    // MARK:
    // MARK: CROSS
    
    func generateSlider() -> XKView {
        
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width < sliderWidth ? sliderWidth : result.size.width, sliderHeight))
        let object:UISlider = UISlider(frame: CGRectMake(0, 0, result.size.width < sliderWidth ? sliderWidth : result.size.width, sliderHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Slider"
        baseView.alterableTypes = self.getAlterableTypes("cross")
        object.value = 0.5
        object.maximumValue = 1.0
        object.minimumValue = 0.0
        object.maximumTrackTintColor = UIColor.lightGrayColor()
        object.minimumTrackTintColor = UIColor.blueColor()
//        object.thumbTintColor = UIColor.whiteColor()
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKSLIDER", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSLIDER
        initActionAttribute(element, obj: object)
        baseView.CDObject = element
        element.currentValue = object.value
        element.maxTrackColor = object.maximumTrackTintColor!
        element.minTrackColor = object.minimumTrackTintColor!
//        element.thumbColor = object.thumbTintColor!
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateSwitch() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, switchWidth, switchHeight))
        let object:UISwitch = UISwitch(frame: CGRectMake(0, 0, switchWidth, switchHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Switch"
        baseView.alterableTypes = self.getAlterableTypes("cross")
        object.on = true
        object.tintColor = UIColor.lightGrayColor()
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKSWITCH", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSWITCH
        initActionAttribute(element, obj: object)
        baseView.CDObject = element
        element.state = NSNumber(bool: object.on)
        element.tintColor = object.tintColor!
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateSegment(number: Int) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width < segmentWidth ? segmentWidth : result.size.width, segmentHeight))
        let object:UISegmentedControl = UISegmentedControl(frame: CGRectMake(0, 0, result.size.width < segmentWidth ? segmentWidth : result.size.width, segmentHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Segment Control"
        baseView.alterableTypes = self.getAlterableTypes("cross")
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKSEGMENT", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSEGMENT
        initActionAttribute(element, obj: object)
        baseView.CDObject = element

        
        for var i=0;i<number;i++ {
            generateSegmentItem(object, element: element)
        }
        object.selectedSegmentIndex = 0
        element.selected = NSNumber(integer: object.selectedSegmentIndex + 1)
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateSegmentItem(object: UISegmentedControl, element: XKSEGMENT) {
        let items:NSMutableSet = NSMutableSet(set: element.items)
        object.insertSegmentWithTitle("Item\(object.numberOfSegments + 1)", atIndex: object.numberOfSegments, animated: true)
        let item = NSEntityDescription.insertNewObjectForEntityForName("XKSEGMENT_ITEM", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSEGMENT_ITEM
        item.updateAttributes()
        item.text = "Item\(object.numberOfSegments)"
        item.segment = element
        item.index = items.count
        items.addObject(item)
        element.itemCount = NSNumber(integer: items.count)
    }
    
    // no used
    func deleteSegmentItem(object: UISegmentedControl, element: XKSEGMENT) {
        let items:NSMutableSet = NSMutableSet(set: element.items)
        object.removeSegmentAtIndex(items.count - 1, animated: true)
        var new_items = items.allObjects as! [XKSEGMENT_ITEM]
        new_items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        new_items.removeLast()
        element.itemCount = NSNumber(integer: new_items.count)
        element.items = NSSet(array: new_items)
        CoreDataUtil.sharedInstance.save()
    }
    
    func deleteSegmentItemBySection(object: UISegmentedControl, element: XKSEGMENT, section: Int) {
        let items:NSMutableSet = NSMutableSet(set: element.items)
        object.removeSegmentAtIndex(section, animated: true)
        var new_items = items.allObjects as! [XKSEGMENT_ITEM]
        new_items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        new_items.removeAtIndex(section)
        var newIndex = 0
        new_items.map {
            (item) in
            item.index = newIndex++
        }
        element.itemCount = NSNumber(integer: new_items.count)
        element.items = NSSet(array: new_items)
        CoreDataUtil.sharedInstance.save()
    }
    
    // MARK:
    // MARK: RECT
    
    func generateImage() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, result.size.height))
        let object:UIImageView = UIImageView(frame: CGRectMake(0, 0, result.size.width, result.size.height))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Image"
        object.image = UIImage(named: "image_default")
        object.clipsToBounds = true
        baseView.alterableTypes = self.getAlterableTypes("rect")
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKIMAGEVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKIMAGEVIEW
        initGraphicAttribute(element, obj: object)
        element.contentMode = object.contentMode.rawValue
        element.image = NSSet(array: Array(arrayLiteral: UIImageJPEGRepresentation(object.image!, 1)!))
        element.blur = NSNumber(bool: false)
        baseView.CDObject = element
        
        if #available(iOS 8.0, *) {
            let blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
            let effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
            effectView.frame = object.frame
            effectView.hidden = true
            object.addSubview(effectView)
        } else {
            // Fallback on earlier versions
        }
        //saveelement(element, obj: object)
        CoreDataUtil.sharedInstance.save()

        return baseView
    }
    
    func generateCanvas() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, result.size.height))
        let object:CanvasView = CanvasView(frame: CGRectMake(0, 0, result.size.width, result.size.height))
        object.backgroundColor = UIColor.whiteColor()
        object.isEditable = true
        object.strokeSize = 1
        object.strokeColor = UIColor.blackColor()
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Canvas"
        baseView.alterableTypes = self.getAlterableTypes("rect")
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKCANVAS", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKCANVAS
        initGraphicAttribute(element, obj: object)
        baseView.CDObject = element
        element.editable = NSNumber(bool: object.isEditable)
        element.strokeSize = object.strokeSize
        element.strokeColor = object.strokeColor
        CoreDataUtil.sharedInstance.save()
        
        //saveelement(element, obj: object)
        return baseView
    }
    
    func generateView() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, result.size.height))
        let object:UIView = UIView(frame: CGRectMake(0, 0, result.size.width, result.size.height))
        object.backgroundColor = UIColor.whiteColor()
        baseView.contentView.addSubview(object)
        baseView.widgetName = "View"
        baseView.alterableTypes = self.getAlterableTypes("rect")
        
        print("view is \(baseView)")
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKCANVAS", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKCANVAS
        initGraphicAttribute(element, obj: object)
        baseView.CDObject = element
        CoreDataUtil.sharedInstance.save()
        
        //print("coredata is \(CoreDataUtil.sharedInstance)")
        //print("baseview is \(baseView)")
        
        //saveelement(element, obj: object)
        return baseView

    }
    
    func generateTextView() -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x, result.origin.y, result.size.width, result.size.height))
        let object:UITextView = UITextView(frame: CGRectMake(0, 0, result.size.width, result.size.height))
        object.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
        object.textAlignment = NSTextAlignment.Left
        baseView.contentView.addSubview(object)
        baseView.widgetName = "TextView"
        baseView.alterableTypes = self.getAlterableTypes("rect")
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKTEXTVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTEXTVIEW
        initTextAttribute(element, obj: object)
        baseView.CDObject = element
        element.text = object.text
        element.alignment = object.textAlignment.rawValue
        
        CoreDataUtil.sharedInstance.save()
        //saveelement(element, obj: object)
        return baseView
    }
    
    // MARK:
    // MARK: ADVANCE
    
    func generatePickerView(controller: EditorViewController) -> XKView {
        baseView = XKView(frame: CGRectMake(0, result.origin.y, screenWidth, pickerHeight))
        let object:UIPickerView = UIPickerView(frame: CGRectMake(0, 0, screenWidth, pickerHeight))
        baseView.contentView.addSubview(object)
        baseView.widgetName = result.name
        object.delegate = controller
        object.dataSource = controller
        controller.pickerDataSource = [["index": NSNumber(integer: 0) , "data": ["C1 Item"]]]
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKPICKERVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKPICKERVIEW
        initGraphicAttribute(element, obj: object)
        baseView.CDObject = element
        element.dataSource = NSSet(array: controller.pickerDataSource)
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateTableView(controller: EditorViewController, Frame:CGRect) -> XKView {
        baseView = XKView(frame: Frame)
        let object:UITableView = UITableView(frame: CGRectMake(0, 0, Frame.size.width, Frame.size.height))
        baseView.contentView.addSubview(object)
        baseView.widgetName = result.name
        object.delegate = ActionUtil.sharedInstance
        object.dataSource = controller
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKTABLEVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTABLEVIEW
        initGraphicAttribute(element, obj: object)
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func generateWebView(Frame:CGRect) -> XKView {
        baseView = XKView(frame: Frame)
        let object:UIWebView = UIWebView(frame: CGRectMake(0, 0, Frame.size.width, Frame.size.height))
        baseView.contentView.addSubview(object)
        baseView.widgetName = result.name
        let request:NSURLRequest = NSURLRequest(URL: NSURL(string: "https://m.flickr.com/#/home")!)
        object.loadRequest(request)
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKWEBVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKWEBVIEW
        initGraphicAttribute(element, obj: object)
        element.url = "https://m.flickr.com/#/home"
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    func getCurrentLocation(notification: NSNotification) {
        let map = notification.userInfo!["map"] as! MKMapView
        if let location = self.location {
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
            map.setRegion(region, animated: true)
        }
    }
    
    func generateMapView(Frame:CGRect) -> XKView {
        baseView = XKView(frame: Frame)
        let object:MKMapView = MKMapView(frame: CGRectMake(0, 0, Frame.size.width, Frame.size.height))
        baseView.contentView.addSubview(object)
        baseView.widgetName = result.name
        object.showsUserLocation = true
        object.scrollEnabled = true
        object.zoomEnabled = true
        
        NSNotificationCenter.defaultCenter().postNotificationName("getCurrentLocation", object: nil, userInfo: ["map": object])
        
        let element = NSEntityDescription.insertNewObjectForEntityForName("XKMAPVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKMAPVIEW
        initGraphicAttribute(element, obj: object)
        if let location = self.location {
            element.coordinate = NSKeyedArchiver.archivedDataWithRootObject(location)
        }
        baseView.CDObject = element
        
        CoreDataUtil.sharedInstance.save()
        
        return baseView
    }
    
    // MARK: - CoreData
    
    func loadLabelFromDB(element: XKLABEL) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UILabel = UILabel(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Label"
        baseView.alterableTypes = self.getAlterableTypes("line")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        object.text = element.text
        object.textAlignment = NSTextAlignment(rawValue: element.alignment.integerValue)!
        switch element.fontStyle {
        case 0:
            object.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        case 1:
            object.font = UIFont(name: "HelveticaNeue-Bold", size: 17)
        case 2:
            object.font = UIFont(name: "HelveticaNeue-Light", size: 11)
        case 3:
            object.font = UIFont(name: "HelveticaNeue-Regular", size: 11)
        case 4:
            object.font = UIFont(name: "HelveticaNeue-Italic", size: 21)
        case 5:
            object.font = UIFont(name: "HelveticaNeue-Bold", size: 24)
        default:
            print("GG")
        }
        return baseView
    }
    
    func loadButtonFromDB(element: XKBUTTON) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UIButton = UIButton(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Button"
        baseView.alterableTypes = self.getAlterableTypes("line")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        
        switch element.borderStyle {
        case 0:
            object.layer.borderWidth = 0
            object.layer.borderColor = UIColor.clearColor().CGColor
            object.layer.cornerRadius = 0
            object.layer.shadowOpacity = 0.0
            object.layer.shadowColor = UIColor.clearColor().CGColor
            object.layer.shadowOffset = CGSizeMake(0.0, 0.0)
            object.layer.shadowRadius = 0
        case 1:
            object.layer.borderWidth = 1
            object.layer.borderColor = UIColor.blackColor().CGColor
            object.layer.cornerRadius = 0
            object.layer.shadowOpacity = 0.0
            object.layer.shadowColor = UIColor.blackColor().CGColor
            object.layer.shadowOffset = CGSizeMake(0.0, 0.0)
            object.layer.shadowRadius = 0
        case 2:
            object.layer.borderWidth = 1
            object.layer.borderColor = UIColor.blackColor().CGColor
            object.layer.cornerRadius = 10
            object.layer.shadowOpacity = 0.0
            object.layer.shadowColor = UIColor.blackColor().CGColor
            object.layer.shadowOffset = CGSizeMake(0.0, 0.0)
            object.layer.shadowRadius = 0
        case 3:
            object.layer.borderWidth = 0
            object.layer.borderColor = UIColor.clearColor().CGColor
            object.layer.cornerRadius = 0
            object.layer.shadowOpacity = 0.7
            object.layer.shadowColor = UIColor.blackColor().CGColor
            object.layer.shadowOffset = CGSizeMake(3.0, 3.0);
            object.layer.shadowRadius = 3
        case 4:
            object.layer.borderWidth = 1
            object.layer.borderColor = UIColor.blackColor().CGColor
            object.layer.cornerRadius = 0
            object.layer.shadowOpacity = 0.7
            object.layer.shadowColor = UIColor.blackColor().CGColor
            object.layer.shadowOffset = CGSizeMake(3.0, 3.0)
            object.layer.shadowRadius = 3
        case 5:
            object.layer.borderWidth = 1
            object.layer.borderColor = UIColor.blackColor().CGColor
            object.layer.cornerRadius = 0
            object.layer.cornerRadius = 10
            object.layer.shadowOpacity = 0.7
            object.layer.shadowColor = UIColor.blackColor().CGColor
            object.layer.shadowOffset = CGSizeMake(3.0, 3.0)
            object.layer.shadowRadius = 3
        default:
            break
        }
        switch element.style {
        case 0:
            if element.image.length > 0 {
                object.setImage(UIImage(data: element.image), forState: .Normal)
            }
            else {
                object.setTitle(element.text, forState: .Normal)
            }
        case 1:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style0"), forState: .Normal)
        case 2:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style1"), forState: .Normal)
        case 3:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style2"), forState: .Normal)
        case 4:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style3"), forState: .Normal)
        case 5:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style4"), forState: .Normal)
        case 6:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style5"), forState: .Normal)
        case 7:
            object.setTitle("", forState: .Normal)
            object.backgroundColor = UIColor.clearColor()
            object.setImage(UIImage(named: "Button_Style6"), forState: .Normal)
        default:
            break
        }
        if let action = element.action as ACTION? {
            ActionUtil.sharedInstance.assignWidgetAction(baseView, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: 0)
        }
        return baseView
    }
    
    func loadTextFieldFromDB(element: XKTEXTFIELD) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UITextField = UITextField(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "TextField"
        baseView.alterableTypes = self.getAlterableTypes("line")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        object.text = element.text
        object.textAlignment = NSTextAlignment(rawValue: element.alignment.integerValue)!
        object.borderStyle = UITextBorderStyle(rawValue: element.style.integerValue)!
        return baseView
    }
    
    func loadHRFromDB(element: XKHR) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UIImageView = UIImageView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Horizontal-Line"
        baseView.alterableTypes = self.getAlterableTypes("line")
        baseView.hideResize()
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        switch element.style.integerValue {
        case 0:
            object.image = UIImage(named: "HR_line0")
        case 1:
            object.image = UIImage(named: "HR_line1")
        case 2:
            object.image = UIImage(named: "HR_line2")
        case 3:
            object.image = UIImage(named: "HR_line3")
        case 4:
            object.image = UIImage(named: "HR_line4")
        case 5:
            object.image = UIImage(named: "HR_line5")
        default:
            object.image = UIImage(named: "hr_default")
        }
        return baseView
    }
    
    func loadSliderFromDB(element: XKSLIDER) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UISlider = UISlider(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Slider"
        baseView.alterableTypes = self.getAlterableTypes("cross")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        object.value = element.currentValue.floatValue
        object.maximumTrackTintColor = element.maxTrackColor as? UIColor
        object.minimumTrackTintColor = element.minTrackColor as? UIColor
//        object.thumbTintColor = element.thumbColor as? UIColor
        
        return baseView
    }
    
    func loadSwitchFromDB(element: XKSWITCH) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UISwitch = UISwitch(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Switch"
        baseView.alterableTypes = self.getAlterableTypes("cross")
        baseView.hideResize()
        baseView.CDObject = element
        baseView.isResize = false
        setBasicAttribute(element, obj: object)
        object.on = element.state.boolValue
        object.tintColor = element.tintColor as? UIColor
        
        return baseView
    }
    
    func loadSegmentControlFromDB(element: XKSEGMENT) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UISegmentedControl = UISegmentedControl(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Segment Control"
        baseView.alterableTypes = self.getAlterableTypes("cross")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        var items = element.items.allObjects as! [XKSEGMENT_ITEM]
        items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        for item in items  {
            object.insertSegmentWithTitle(item.text, atIndex: item.index.integerValue, animated: false)
        }
        object.selectedSegmentIndex = element.selected.integerValue - 1
        
        return baseView
    }
    
    func loadImageFromDB(element: XKIMAGEVIEW, controller: PropertyTableViewController) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.widgetName = "Image"
        baseView.alterableTypes = self.getAlterableTypes("rect")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        let images: Array<NSData> = element.image.allObjects as! [NSData]
        switch element.style {
        case 0, 3, 4:
            let object:UIImageView = UIImageView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
            object.image = UIImage(data: images[0])
            object.contentMode = UIViewContentMode(rawValue: element.contentMode.integerValue)!
            
            if #available(iOS 8.0, *) {
                let blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
                let effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
                effectView.frame = object.frame
                effectView.hidden = true
                object.addSubview(effectView)
                effectView.hidden = !Bool(element.blur)

            } else {
                // Fallback on earlier versions
            }
            
            baseView.contentView.addSubview(object)
            setBasicAttribute(element, obj: object)
            if element.style.integerValue == 3 {
                baseView.isResize = false
            }
            else if element.style.integerValue == 4 {
                object.layer.cornerRadius = object.frame.size.width / 2
                object.clipsToBounds = true
            }
        case 1:
            let object:UIImageView = UIImageView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
            object.animationImages = images.map({ (data) -> UIImage in
                let image:UIImage = UIImage(data: data)!
                return image
            })
            object.animationDuration = 0.5
            object.animationRepeatCount = 0
            object.startAnimating()
            object.contentMode = UIViewContentMode(rawValue: element.contentMode.integerValue)!
            
            if #available(iOS 8.0, *) {
                let blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
                let effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
                effectView.frame = object.frame
                effectView.hidden = true
                object.addSubview(effectView)
                effectView.hidden = !Bool(element.blur)
            } else {
                // Fallback on earlier versions
            }
            
            baseView.contentView.addSubview(object)
            setBasicAttribute(element, obj: object)
        case 2:
            let layout = EBCardCollectionViewLayout()
            let size = element.imageSize.CGSizeValue()
            layout.offset = UIOffset(horizontal: (screenWidth - size.width)/2, vertical: 0)
            let cardView = UICollectionView(frame: CGRect(x: 0, y: 0, width: result.size.width - resizeIconSize, height: result.size.height - resizeIconSize), collectionViewLayout: layout)
            cardView.delegate = ActionUtil.sharedInstance
            cardView.dataSource = controller
            cardView.registerClass(PhotoCell.self, forCellWithReuseIdentifier: "Cell")
            baseView.contentView.addSubview(cardView)
            baseView.isResize = false
            controller.entity = element
            controller.reloadCollectionData()
            cardView.reloadData()
            setBasicAttribute(element, obj: cardView)
            if let action = element.action as ACTION? {
                ActionUtil.sharedInstance.assignWidgetAction(baseView, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: 0)
            }
        default:
            print("not yet")
        }
        return baseView
    }
    
    func loadTextViewFromDB(element: XKTEXTVIEW) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UITextView = UITextView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "TextView"
        baseView.alterableTypes = self.getAlterableTypes("rect")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        object.textAlignment = NSTextAlignment(rawValue: element.alignment.integerValue)!
        object.text = element.text
        
        return baseView
    }
    
    func loadCanvasFromDB(element: XKCANVAS) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
//        var object:CanvasView = CanvasView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UIView = UIView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
//        baseView.widgetName = "Canvas"
        baseView.widgetName = "View"
        baseView.alterableTypes = self.getAlterableTypes("rect")
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
//        object.isEditable = Bool(element.editable)
//        object.strokeColor = element.strokeColor as! UIColor
//        object.strokeSize = CGFloat(element.strokeSize.doubleValue)
//        if let paths = element.paths?.allObjects as? [UIBezierPath] {
//            object.paths = paths
//        }
        
        return baseView
    }
    
    func loadNavBarFromDB(element: XKNAV) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UINavigationBar = UINavigationBar(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Navigation"
        baseView.hideResize()
        baseView.CDObject = element
        baseView.isResize = false
        setBasicAttribute(element, obj: object)
        let topItem:UINavigationItem = UINavigationItem(title: element.title)
        object.setItems([topItem], animated: false)
        object.titleTextAttributes = [NSForegroundColorAttributeName: element.fontColor as! UIColor]
        // fontstyle not yet
        
        var items = element.items.allObjects as! [XKNAV_ITEM]
        items.sortInPlace {$0.side < $1.side}
        for (itemNum, item) in items.enumerate() {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: barButtonSize, height: barButtonSize))
//            var barItem: UIBarButtonItem = UIBarButtonItem()
            if item.style == 0 {
                if let action = item.action as ACTION? {
                    ActionUtil.sharedInstance.assignWidgetAction(baseView, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: itemNum)
                }
                else {
//                    barItem.title = item.text
                    if item.image.length > 0 {
//                        barItem.image = UIImage(data: item.image)
                        button.setTitle(nil, forState: .Normal)
                        button.setImage(UIImage(data: item.image), forState: .Normal)
                        button.imageEdgeInsets = UIEdgeInsetsMake(5.0, 0, -5.0, 0)

                    }
                    else {
                        button.frame.size.width = 60
                        button.setTitle(item.text, forState: .Normal)
                        button.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                    }
                    if item.side == "right" {
//                        topItem.setRightBarButtonItem(barItem, animated: false)
                        topItem.setRightBarButtonItem(UIBarButtonItem(customView: button), animated: false)
                    }
                    else {
//                        topItem.setLeftBarButtonItem(barItem, animated: false)
                        topItem.setLeftBarButtonItem(UIBarButtonItem(customView: button), animated: false)
                        topItem.leftBarButtonItem?.customView?.layoutIfNeeded()
                    }
                }
            }
            else {
                let type = item.style.integerValue < 6 ? item.style.integerValue - 1 : item.style.integerValue + 1
                if let action = item.action as ACTION? {
                    ActionUtil.sharedInstance.assignWidgetAction(baseView, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: itemNum)
                }
                else {
                    if item.side == "right" {
                        topItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem(rawValue: type)!, target: self, action: "")
                    }
                    else {
                        topItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem(rawValue: type)!, target: self, action: "")
                    }
                }
            }
        }
        return baseView
    }
    
    func loadTabBarFromDB(element: XKTAB) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UITabBar = UITabBar(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = "Tab"
        baseView.hideResize()
        baseView.CDObject = element
        baseView.isResize = false
        setBasicAttribute(element, obj: object)

        var barItems:Array<UITabBarItem>! = []
        var items:Array<XKTAB_ITEM> = element.items.allObjects as! [XKTAB_ITEM]
        items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
        for item in items {
            var newItem: UITabBarItem!
            if item.style == 0 {
                newItem = UITabBarItem(title: item.text, image: UIImage.imageResize(image: UIImage(data: item.image), sizeChange: CGSize(width: barButtonSize, height: barButtonSize)) , tag: item.index.integerValue)
                barItems.append(newItem)
            }
            else {
                newItem = UITabBarItem(tabBarSystemItem: UITabBarSystemItem(rawValue: item.style.integerValue - 1)!, tag: item.index.integerValue)
                barItems.append(newItem)
            }
            if let action = item.action as ACTION? {
                newItem.actionInfo = action
            }
        }
        if barItems.count > 0 {
            object.setItems(barItems, animated: false)
            object.selectedItem = barItems[element.selected.integerValue - 1]
        }
        object.delegate = ActionUtil.sharedInstance
        
        return baseView
    }
    
    func loadTableViewFromDB(element: XKTABLEVIEW, controller: EditorViewController) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let tableStyle = element.tableStyle.integerValue == 0 ? UITableViewStyle.Plain : UITableViewStyle.Grouped
        let object:UITableView = UITableView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize), style: tableStyle)
        baseView.contentView.addSubview(object)
        baseView.widgetName = element.name
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        object.delegate = ActionUtil.sharedInstance
        object.dataSource = controller
        controller.tableCellStyle = element.cellStyle.integerValue
        if let action = element.action as ACTION? {
            ActionUtil.sharedInstance.assignWidgetAction(baseView, page: action.targetPage, event: SegueEvent(rawValue: action.event.integerValue)!, type: SegueType(rawValue: action.type.integerValue)!, index: 0)
        }
        return baseView
    }
    
    func loadPickerViewFromDB(element: XKPICKERVIEW, controller: EditorViewController) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UIPickerView = UIPickerView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = element.name
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        object.delegate = controller
        object.dataSource = controller
        NSNotificationCenter.defaultCenter().postNotificationName("updatePickerDataSource", object: nil, userInfo: ["entity": element])
        return baseView
    }
    
    func loadMapViewFromDB(element: XKMAPVIEW) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:MKMapView = MKMapView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = element.name
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        let location = NSKeyedUnarchiver.unarchiveObjectWithData(element.coordinate) as! CLLocation
        let region = MKCoordinateRegion(center: location.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
        object.setRegion(region, animated: true)
//        object.showsUserLocation = true
        object.scrollEnabled = true
        object.zoomEnabled = true

        return baseView
    }
    
    func loadWebViewFromDB(element: XKWEBVIEW) -> XKView {
        baseView = XKView(frame: CGRectMake(result.origin.x + resizeIconSize/2, result.origin.y + resizeIconSize/2 , result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        let object:UIWebView = UIWebView(frame: CGRectMake(0, 0, result.size.width - resizeIconSize, result.size.height - resizeIconSize))
        baseView.contentView.addSubview(object)
        baseView.widgetName = element.name
        baseView.hideResize()
        baseView.isResize = true
        baseView.CDObject = element
        setBasicAttribute(element, obj: object)
        let request:NSURLRequest = NSURLRequest(URL: NSURL(string: element.url)!)
        object.loadRequest(request)
        
        return baseView

    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.last as CLLocation?
    }
    
    // MARK:
    // MARK: Widget Operation
    
    func copyWidget(source: NATIVE_WIDGET) -> Dictionary<String, AnyObject>? {
        var copy: Dictionary<String, AnyObject> = [:]
        
        // copy attribute and ship to dictionary
        let attributes = source.entity.attributesByName 
        for attr in attributes.keys {
            if let value: AnyObject = source.valueForKey(attr) {
                copy.updateValue(value, forKey: attr)
            }
        }
        let relations = source.entity.relationshipsByName 
        for ship in relations.keys {
            if let value: AnyObject = source.valueForKey(ship) {
                
                if ship == "items" {
                    var items: Array<Dictionary<String, AnyObject>> = Array()
                    for item in (value as! NSSet).allObjects {
                        switch source {
                        case is XKTAB, is XKNAV:
                            let itemDict = copyWidget(item as! NATIVE_WIDGET)!
                            items.append(itemDict)
                        default:
                            break
                        }
                    }
                    copy.updateValue(items, forKey: ship)
                }
                else {
                    copy.updateValue(value, forKey: ship)
                }
            }
        }
        return copy
    }
    
    func passAttributeAndShip(source: Dictionary<String, AnyObject>, paste: NATIVE_WIDGET, page: PAGE) {
        let attributes = paste.entity.attributesByName 
        for attr in attributes.keys {
            paste.setValue(source[attr], forKey: attr)
        }
        let relations = paste.entity.relationshipsByName 
        for ship in relations.keys {
            if ship == "items" {
                switch paste {
                case is XKTAB:
                    let items = source[ship] as! Array<Dictionary<String, AnyObject>>
                    for itemDict in items {
                        let newItem = NSEntityDescription.insertNewObjectForEntityForName("XKTAB_ITEM", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTAB_ITEM
                        passAttributeAndShip(itemDict, paste: newItem, page: page)
                        newItem.tab = paste as! XKTAB
                    }
                case is XKNAV:
                    let items = source[ship] as! Array<Dictionary<String, AnyObject>>
                    for itemDict in items {
                        let newItem = NSEntityDescription.insertNewObjectForEntityForName("XKNAV_ITEM", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKNAV_ITEM
                        passAttributeAndShip(itemDict, paste: newItem, page: page)
                        newItem.nav = paste as! XKNAV
                    }
                default:
                    break
                }
            }
            else if ship != "tab" && ship != "nav" {
                paste.setValue(source[ship], forKey: ship)
            }
        }
        paste.page = page
        CoreDataUtil.sharedInstance.save()
    }
    
    func pasteWidget(source: Dictionary<String, AnyObject>, page: PAGE, controller: UIViewController?) -> XKView? {
        var paste: NATIVE_WIDGET?
        self.result = DollarResult.initByFrame((source["frame"] as! NSValue).CGRectValue())
        var frame:CGRect! = (source["frame"] as! NSValue).CGRectValue()
        if isDuplicate {
            self.result.origin.x += resizeIconSize
            self.result.origin.y += resizeIconSize
            frame.origin.x += resizeIconSize
            frame.origin.y += resizeIconSize
        }
        var newSource = source
        newSource.updateValue(NSValue(CGRect: frame), forKey: "frame")
        
        switch newSource["name"] as! String {
        case "Label":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKLABEL", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKLABEL
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadLabelFromDB(paste as! XKLABEL)
        case "Button":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKBUTTON", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKBUTTON
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadButtonFromDB(paste as! XKBUTTON)
        case "TextField":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKTEXTFIELD", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTEXTFIELD
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadTextFieldFromDB(paste as! XKTEXTFIELD)
        case "Horizontal-Line":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKHR", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKHR
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadHRFromDB(paste as! XKHR)
        case "Slider":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKSLIDER", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSLIDER
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadSliderFromDB(paste as! XKSLIDER)
        case "Switch":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKSWITCH", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSWITCH
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadSwitchFromDB(paste as! XKSWITCH)
        case "Segment Control":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKSEGMENT", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKSEGMENT
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadSegmentControlFromDB(paste as! XKSEGMENT)
        case "Image":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKIMAGEVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKIMAGEVIEW
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadImageFromDB(paste as! XKIMAGEVIEW, controller: (controller as! PropertyTableViewController))
        case "TextView":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKTEXTVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTEXTVIEW
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadTextViewFromDB(paste as! XKTEXTVIEW)
        case "Canvas", "View":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKCANVAS", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKCANVAS
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadCanvasFromDB(paste as! XKCANVAS)
        case "Navigation":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKNAV", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKNAV
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadNavBarFromDB(paste as! XKNAV)
        case "Tab":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKTAB", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTAB
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadTabBarFromDB(paste as! XKTAB)
        case "tableview":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKTABLEVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKTABLEVIEW
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadTableViewFromDB(paste as! XKTABLEVIEW, controller: (controller as! EditorViewController))
        case "picker":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKPICKERVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKPICKERVIEW
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadPickerViewFromDB(paste as! XKPICKERVIEW, controller: (controller as! EditorViewController))
        case "mapview":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKMAPVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKMAPVIEW
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadMapViewFromDB(paste as! XKMAPVIEW)
        case "webview":
            paste = NSEntityDescription.insertNewObjectForEntityForName("XKWEBVIEW", inManagedObjectContext: CoreDataUtil.sharedInstance.managedObjectContext!) as! XKWEBVIEW
            passAttributeAndShip(newSource, paste: paste!, page: page)
            return WidgetUtil.sharedInstance.loadWebViewFromDB(paste as! XKWEBVIEW)
        default:
            break
        }
        print("gG")
        return nil
    }
    
}
