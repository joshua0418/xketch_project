#import "DollarDefaultGestures.h"
#import "DollarPointCloud.h"
#import "DollarPoint.h"

DollarPointCloud * MakePointCloud(NSString *name, NSArray *points) {
    return [[DollarPointCloud alloc] initWithName:name points:points];
}

DollarPoint * MakePoint(float x, float y, int id) {
    return [[DollarPoint alloc] initWithId:@(id) x:x y:y];
}

@implementation DollarDefaultGestures

+ (id)defaultPointClouds {
    static NSArray *defaultPointClouds = nil;
    if (!defaultPointClouds) {
        defaultPointClouds = [self pointClouds];
    }
    return defaultPointClouds;
}

+ (NSMutableArray *)pointClouds {
    NSMutableArray *pointClouds = [NSMutableArray array];
    
    pointClouds[0] = MakePointCloud(@"line", @[
                                               MakePoint(12,347,1),
                                               MakePoint(119,347,1)
                                               ]);
    
    pointClouds[1] = MakePointCloud(@"delete", @[
                                                 MakePoint(160,100,1),
                                                 MakePoint(160,175,1)
                                                 ]);
    
    pointClouds[2] = MakePointCloud(@"rect", @[
                                               MakePoint(112,209,1),
                                               MakePoint(215,212,1),
                                               
                                               MakePoint(215,212,2),
                                               MakePoint(220,296,2),
                                               
                                               MakePoint(220,296,3),
                                               MakePoint(117,294,3),
                                               
                                               MakePoint(117,294,4),
                                               MakePoint(112,209,4)
                                               ]);
    
    pointClouds[3] = MakePointCloud(@"image", @[
                                                MakePoint(112,209,1),
                                                MakePoint(215,212,1),
                                                
                                                MakePoint(215,212,2),
                                                MakePoint(220,296,2),
                                                
                                                MakePoint(220,296,3),
                                                MakePoint(117,294,3),
                                                
                                                MakePoint(117,294,4),
                                                MakePoint(112,209,4),
                                                
                                                MakePoint(190,232,5),
                                                MakePoint(156,260,5),
                                                
                                                MakePoint(153,230,6),
                                                MakePoint(194,254,6)
                                                ]);


    
    pointClouds[4] = MakePointCloud(@"slider", @[
                                                 MakePoint(86,258,1),
                                                 MakePoint(236,260,1),
                                                 
                                                 MakePoint(162,210,2),
                                                 MakePoint(160,296,2)
                                                 ]);
    
    pointClouds[5] = MakePointCloud(@"page", @[
                                               MakePoint(66,274,1),
                                               MakePoint(88,260,1),
                                               MakePoint(112,270,1),
                                               MakePoint(99,298,1),
                                               MakePoint(75,308,1),
                                               MakePoint(66,274,1),
                                               
                                               MakePoint(150,268,2),
                                               MakePoint(163,254,2),
                                               MakePoint(196,262,2),
                                               MakePoint(177,292,2),
                                               MakePoint(152,298,2),
                                               MakePoint(150,268,2),
                                               
                                               MakePoint(236,266,3),
                                               MakePoint(252,252,3),
                                               MakePoint(281,256,3),
                                               MakePoint(280,284,3),
                                               MakePoint(249,298,3),
                                               MakePoint(236,266,3)
                                               ]);
    
    pointClouds[6] = MakePointCloud(@"webview", @[
                                                  MakePoint(80,266,1),
                                                  MakePoint(95,335,1),
                                                  
                                                  MakePoint(95,335,2),
                                                  MakePoint(141,274,2),
                                                  
                                                  MakePoint(141,274,3),
                                                  MakePoint(166,345,3),
                                                  
                                                  MakePoint(166,345,4),
                                                  MakePoint(192,277,4)
                                                  ]);
    
    pointClouds[7] = MakePointCloud(@"picker", @[
                                                  MakePoint(134,192,1),
                                                  MakePoint(136,314,1),
                                                  
                                                  MakePoint(155,198,2),
                                                  MakePoint(196,204,2),
                                                  MakePoint(226,246,2),
                                                  MakePoint(196,270,2),
                                                  MakePoint(144,280,2)
                                                  ]);
    
    pointClouds[8] = MakePointCloud(@"tableview", @[
                                                     MakePoint(109,206,1),
                                                     MakePoint(230,211,1),
                                                     
                                                     MakePoint(165,209,2),
                                                     MakePoint(170,290,2)
                                                     ]);
    
    pointClouds[9] = MakePointCloud(@"mapview", @[
                                                   MakePoint(70,295,1),
                                                   MakePoint(102,217,1),
                                                   
                                                   MakePoint(102,217,2),
                                                   MakePoint(132,272,2),
                                                   
                                                   MakePoint(132,272,3),
                                                   MakePoint(180,224,3),
                                                   
                                                   MakePoint(180,224,4),
                                                   MakePoint(194,308,4)
                                                   ]);
    
    pointClouds[10] = MakePointCloud(@"group", @[
                                                 MakePoint(102,304,1),
                                                 MakePoint(82,241,1),
                                                 
                                                 MakePoint(82,241,2),
                                                 MakePoint(131,188,2),
                                                 
                                                 MakePoint(131,188,3),
                                                 MakePoint(198,206,3),
                                                 
                                                 MakePoint(198,206,4),
                                                 MakePoint(225,265,4),
                                                 
                                                 MakePoint(225,265,5),
                                                 MakePoint(194,314,5),
                                                 
                                                 MakePoint(194,314,6),
                                                 MakePoint(102,304,6)
                                                 ]);
    
    //    pointClouds[x] = MakePointCloud(@"switch", @[]);
    
    return pointClouds;
}

@end