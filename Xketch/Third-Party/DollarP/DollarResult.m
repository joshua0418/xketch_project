#import "DollarResult.h"

@implementation DollarResult

@synthesize name, score, size, origin, endPoint, stroke_count;

+ (DollarResult*)initByFrame:(CGRect)frame {
    DollarResult *result = [[DollarResult alloc] init];
    result.origin = frame.origin;
    result.size = frame.size;
    return result;
}


@end