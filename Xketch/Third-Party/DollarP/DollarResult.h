#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DollarResult : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic) float score;

@property (nonatomic) CGSize size;
@property (nonatomic) CGPoint origin;
@property (nonatomic) CGPoint endPoint;
@property (nonatomic) int stroke_count;

+ (DollarResult*)initByFrame:(CGRect)frame;
@end