//
//  ZoomAnimator.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/11.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

@objc protocol ZoomAnimatorProtocol {
    func viewForTransition() -> UIView
}

class ZoomAnimator: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning, UINavigationControllerDelegate {
    
    var pageNum: Int = -1
    var isPinchIn: Bool = false
    var interactive: Bool = false   // for gesture detect
    var datasource: ZoomAnimatorProtocol?
    let animationDuration = 0.7
    
    private var navigationController: UINavigationController
    private var fromView: UIView?
    private var toView: UIView?
    private var fromFrame: CGRect?
    private var toFrame: CGRect?
    private var transitionView: UIView?
    private var transitionContext: UIViewControllerContextTransitioning?
    private var fromViewController: UIViewController?
    private var toViewController: UIViewController?
    private var shouldCompleteTransition: Bool = false
    private let completionThreshold: CGFloat = 0.7
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return animationDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        
        if let viewController = toViewController {
            toView = (viewController as! ZoomAnimatorProtocol).viewForTransition()
        }
        if let viewController = fromViewController {
            fromView = (viewController as! ZoomAnimatorProtocol).viewForTransition()
        }
        
        toViewController?.view.frame = transitionContext.finalFrameForViewController(toViewController!)
        toViewController?.updateViewConstraints()
        
        let container = self.transitionContext!.containerView()
        
        // add toViewController to Transition Container
        if let view = toViewController?.view {
            container!.addSubview(view)
        }
        toViewController?.view.layoutIfNeeded()
        
        // Calculate animation frames within container view
        fromFrame = container!.convertRect(fromView!.bounds, fromView: fromView)
        toFrame = container!.convertRect(toView!.bounds, fromView: toView)
        
        // == send which screen now(transitionView) in toViewController center == CGRect(x: 321, y: 206, width: 259, height: 460)
        transitionView = fromView?.superview?.snapshotViewAfterScreenUpdates(false)
        
        if let view = transitionView {
            view.clipsToBounds = true
            view.frame = fromFrame!
            container!.addSubview(view)
        }
        
        if isPinchIn {
            self.animatePinchInTransition()
        }
        else {
            self.animatePinchOutTransition()
        }
    }
    
    func animatePinchInTransition(){
        print("PinchIn")
        toViewController?.view.alpha = 0
        toView?.hidden = true
        fromView?.alpha = 0 // for grasp effect
        
        let duration = transitionDuration(transitionContext!)
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            
            self.toViewController?.view.alpha = 1
            if (self.interactive == false){
                self.transitionView?.frame = self.toFrame!
            }
            
            }) { (Bool finished) -> Void in
                if self.interactive == false {
                    self.pinchInTransitionComplete()
                }
        }
    }
    
    func animatePinchOutTransition(){
        print("PinchOut")
        toViewController?.view.alpha = 0
        toView?.hidden = true
        fromView?.alpha = 1
    
        let duration = transitionDuration(transitionContext!)
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
            
            self.toViewController?.view.alpha = 1
            self.toViewController?.viewWillAppear(true)
//            self.transitionView?.frame = self.toFrame!
            print("animation time")
            
            }) { (Bool finished) -> Void in
                if finished {
                    print("complete time")
                }
//                self.transitionView?.removeFromSuperview()
//                self.toView?.hidden = false
//                self.fromViewController?.view.alpha = 0
//                self.toView?.hidden = false
//                self.fromView?.alpha = 0
//                
//                if (self.transitionContext!.transitionWasCancelled()){
//                    self.toViewController?.view.removeFromSuperview()
//                    self.transitionContext!.completeTransition(false)
//                } else {
//                    self.transitionContext!.completeTransition(true)
//                }

        }
    }
    
    func pinchInTransitionComplete() {
        if (self.transitionView?.superview == nil){
            return
        }
        self.fromViewController?.view.alpha = 1
        self.toView?.hidden = false
        self.fromView?.alpha = 1
        self.fromViewController?.view.hidden = false
        self.transitionView?.removeFromSuperview()
        
        if (self.transitionContext!.transitionWasCancelled()){
            self.toViewController?.view.removeFromSuperview()
            self.transitionContext!.completeTransition(false)
        } else {
            self.transitionContext!.completeTransition(true)
        }
    }
    
    func pinchOutTransitionComplete(){
//        self.fromViewController?.view.alpha = 0
//        self.toView?.hidden = false
//        self.fromView?.alpha = 1
//        self.fromViewController?.view.hidden = true
        self.transitionView?.removeFromSuperview()
        
        if (self.transitionContext!.transitionWasCancelled()){
            self.toViewController?.view.removeFromSuperview()
            self.transitionContext!.completeTransition(false)
        } else {
            self.transitionContext!.completeTransition(true)
        }
    }
    
    func handlePinchAction (sender: UIPinchGestureRecognizer) {
        if self.transitionView != nil {
            switch sender.state {
            case .Began:
                interactive = true
                print("sw- interactive:\(interactive)")
                break
            case .Changed:
                self.transitionView?.transform = CGAffineTransformScale(self.transitionView!.transform, sender.scale, sender.scale)
                sender.scale = 1
                
                // calculate current scale of transitionView
                let scale = self.transitionView!.frame.size.width / self.fromFrame!.size.width
                
                // Check if we should complete or restore transition when gesture is ended
                self.shouldCompleteTransition = (scale < completionThreshold);
                //            println("scale\(1-scale)")
                updateInteractiveTransition(1 - scale)
                
                break
            case .Ended, .Cancelled:
                if self.transitionContext != nil {
                    var animationFrame = toFrame
                    let cancelAnimation = (self.shouldCompleteTransition == false && sender.velocity >= 0) || sender.state == UIGestureRecognizerState.Cancelled
                    print("cancel: \(cancelAnimation)")
                    
                    if (cancelAnimation){
                        animationFrame = fromFrame
                        cancelInteractiveTransition()
                    } else {
                        finishInteractiveTransition()
                    }
                    
                    // calculate current scale of transitionView
                    let finalScale = animationFrame!.width / self.fromFrame!.size.width
                    let currentScale = (transitionView!.frame.size.width / self.fromFrame!.size.width)
                    let delta = finalScale - currentScale
                    var normalizedVelocity = sender.velocity / delta
                    
                    // add upper and lower bound on normalized velocity
                    normalizedVelocity = normalizedVelocity > 20 ? 20 : normalizedVelocity
                    normalizedVelocity = normalizedVelocity < -20 ? -20 : normalizedVelocity
                    
                    //println("---\nvelocity \(gesture.velocity)")
                    //println("normal \(delta)")
                    //println("velocity normal \(normalizedVelocity)")
                    
                    // no need to normalize the velocity for low velocities
                    if sender.velocity < 3 && sender.velocity > -3 {
                        normalizedVelocity = sender.velocity
                    }
                    
                    let duration = transitionDuration(transitionContext!)
                    
                    UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: normalizedVelocity, options: UIViewAnimationOptions(), animations: { () -> Void in
                        
                        // set a new transform to reset the rotation to 0 but maintain the current scale
                        self.transitionView?.transform = CGAffineTransformMakeScale(currentScale, currentScale)
                        
                        if let frame = animationFrame {
                            self.transitionView?.frame = frame
                        }
                        
                        }, completion: { (Bool finished) -> Void in
                            self.pinchInTransitionComplete()
                            self.interactive = false
                    })
                }
                break
            default:
                break
            }
        }
    }
    
    
    // MARK: - UINavigationControllerDelegate
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if (fromVC.conformsToProtocol(ZoomAnimatorProtocol) && toVC.conformsToProtocol(ZoomAnimatorProtocol)){
            isPinchIn = toVC is StoryboardViewController ? true : false
            return isPinchIn == true ? self : nil
//            return self
        }
        
        return nil
    }
    
    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if isPinchIn == false {
            return self
        }
        return nil
    }
    
}
