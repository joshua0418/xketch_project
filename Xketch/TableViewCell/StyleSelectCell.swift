//
//  StyleSelectCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/26.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class StyleSelectCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setAttributeValue(cell: StyleSelectCell, attr:String, value: AnyObject, item_index: Int) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        cell.attributeValue.imageView?.contentMode = .ScaleAspectFit
        cell.attributeValue.setImage(nil, forState: .Normal)
        cell.attributeValue.setBackgroundImage(nil, forState: .Normal)
        switch value {
        case is NSNumber:
            if attr == "alignment" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "align_left"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "align_center"), forState: .Normal)
                case 2:
                    cell.attributeValue.setImage(UIImage(named: "align_right"), forState: .Normal)
                case 3:
                    cell.attributeValue.setImage(UIImage(named: "align_justify"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
                cell.attributeValue.backgroundColor = UIColor.clearColor()
                cell.attributeValue.setTitle("", forState: UIControlState.Normal)
            }
            else if attr == "borderStyle" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "border_stylex"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "border_style1"), forState: .Normal)
                case 2:
                    cell.attributeValue.setImage(UIImage(named: "border_style2"), forState: .Normal)
                case 3:
                    cell.attributeValue.setImage(UIImage(named: "border_style3"), forState: .Normal)
                case 4:
                    cell.attributeValue.setImage(UIImage(named: "border_style4"), forState: .Normal)
                case 5:
                    cell.attributeValue.setImage(UIImage(named: "border_style5"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
                cell.attributeValue.backgroundColor = UIColor.clearColor()
                cell.attributeValue.setTitle("", forState: UIControlState.Normal)
            }
            else if attr == "keyboardStyle" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "keyboard_stylex"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "keyboard_style1"), forState: .Normal)
                case 2:
                    cell.attributeValue.setImage(UIImage(named: "keyboard_style2"), forState: .Normal)
                case 3:
                    cell.attributeValue.setImage(UIImage(named: "keyboard_style3"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
                cell.attributeValue.backgroundColor = UIColor.clearColor()
                cell.attributeValue.setTitle("", forState: UIControlState.Normal)
            }
            else if attr == "style" {
                print(currentWidget?.widgetName)
                if currentWidget?.widgetName == "TextField" {
                    switch (value as! NSNumber).integerValue {
                    case 0:
                        cell.attributeValue.setImage(UIImage(named: "TextField_Style0"), forState: .Normal)
                    case 1:
                        cell.attributeValue.setImage(UIImage(named: "TextField_Style1"), forState: .Normal)
                    case 2:
                        cell.attributeValue.setImage(UIImage(named: "TextField_Style2"), forState: .Normal)
                    case 3:
                        cell.attributeValue.setImage(UIImage(named: "TextField_Style3"), forState: .Normal)
                    default:
                        cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                    }
                    cell.attributeValue.backgroundColor = UIColor.clearColor()
                    cell.attributeValue.setTitle("", forState: UIControlState.Normal)
                }
                else if currentWidget?.widgetName == "Button" {
                    switch (value as! NSNumber).integerValue {
                    case 0:
                        cell.attributeValue.setImage(UIImage(named: "Custom"), forState: .Normal)
                    case 1:
                        cell.attributeValue.setImage(UIImage(named: "Button_Style0"), forState: .Normal)
                    case 2:
                        cell.attributeValue.setImage(UIImage(named: "Button_Style1"), forState: .Normal)
                    case 3:
                        cell.attributeValue.setImage(UIImage(named: "Button_Style2"), forState: .Normal)
                    case 4:
                        cell.attributeValue.setImage(UIImage(named: "Button_Style3"), forState: .Normal)
                    case 5:
                        cell.attributeValue.setImage(UIImage(named: "Button_Style4"), forState: .Normal)
                    case 6:
                        cell.attributeValue.setImage(UIImage(named: "Button_Style5"), forState: .Normal)
                    default:
                        cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                    }
                    cell.attributeValue.backgroundColor = UIColor.clearColor()
                    cell.attributeValue.setTitle("", forState: UIControlState.Normal)
                }
                else if currentWidget?.widgetName == "Horizontal-Line" {
                    switch (value as! NSNumber).integerValue {
                    case 0:
                        cell.attributeValue.setImage(UIImage(named: "HR0"), forState: .Normal)
                    case 1:
                        cell.attributeValue.setImage(UIImage(named: "HR1"), forState: .Normal)
                    case 2:
                        cell.attributeValue.setImage(UIImage(named: "HR2"), forState: .Normal)
                    case 3:
                        cell.attributeValue.setImage(UIImage(named: "HR3"), forState: .Normal)
                    case 4:
                        cell.attributeValue.setImage(UIImage(named: "HR4"), forState: .Normal)
                    case 5:
                        cell.attributeValue.setImage(UIImage(named: "HR5"), forState: .Normal)
                    default:
                        cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                    }
                    cell.attributeValue.backgroundColor = UIColor.clearColor()
                    cell.attributeValue.setTitle("", forState: UIControlState.Normal)
                }
                else if currentWidget?.widgetName == "Image" {
                    switch (value as! NSNumber).integerValue {
                    case 0:
                        cell.attributeValue.setImage(UIImage(named: "image_style0"), forState: .Normal)
                    case 1:
                        cell.attributeValue.setImage(UIImage(named: "image_style1"), forState: .Normal)
                    case 2:
                        cell.attributeValue.setImage(UIImage(named: "image_style2_1"), forState: .Normal)
                    case 3:
                        cell.attributeValue.setImage(UIImage(named: "image_style3_1"), forState: .Normal)
                    case 4:
                        cell.attributeValue.setImage(UIImage(named: "image_style4"), forState: .Normal)
                    default:
                        cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                    }
                    cell.attributeValue.backgroundColor = UIColor.clearColor()
                }
                else if currentWidget?.widgetName == "Navigation" {    // 等圖
                    cell.attributeValue.backgroundColor = UIColor.clearColor()
                    switch (value as! NSNumber).integerValue {
                    case 0:
                        cell.attributeValue.setImage(UIImage(named: "Custom"), forState: .Normal)
                        cell.attributeValue.setTitle("", forState: UIControlState.Normal)
                    case 1:
                        cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                        cell.attributeValue.setTitle("Done", forState: .Normal)
                        cell.attributeValue.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.whiteColor()
                    case 2:
                        cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                        cell.attributeValue.setTitle("Cancel", forState: .Normal)
                        cell.attributeValue.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.whiteColor()
                    case 3:
                        cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                        cell.attributeValue.setTitle("Edit", forState: .Normal)
                        cell.attributeValue.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.whiteColor()
                    case 4:
                        cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                        cell.attributeValue.setTitle("Save", forState: .Normal)
                        cell.attributeValue.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.whiteColor()
                    case 5:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item5"), forState: .Normal)
                    case 6:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item6"), forState: .Normal)
                    case 7:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item7"), forState: .Normal)
                    case 8:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item8"), forState: .Normal)
                    case 9:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item9"), forState: .Normal)
                    case 10:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item10"), forState: .Normal)
                    case 11:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item11"), forState: .Normal)
                    case 12:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item12"), forState: .Normal)
                    case 13:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item13"), forState: .Normal)
                    case 14:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item14"), forState: .Normal)
                    case 15:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item15"), forState: .Normal)
                    case 16:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item16"), forState: .Normal)
                    case 17:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item17"), forState: .Normal)
                    case 18:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item18"), forState: .Normal)
                    case 19:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "nav_item19"), forState: .Normal)
                    case 20:
                        cell.attributeValue.setTitle("Undo", forState: .Normal)
                        cell.attributeValue.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.whiteColor()
                    case 21:
                        cell.attributeValue.setTitle("Redo", forState: .Normal)
                        cell.attributeValue.setTitleColor(UIColor.iOSblueColor(), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.whiteColor()
                    default:
                        cell.attributeValue.setTitle("\(value as! NSNumber)", forState: UIControlState.Normal)
                        cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                        cell.attributeValue.backgroundColor = UIColor.redColor()
                    }
                    cell.tag = item_index
                }
                else if currentWidget?.widgetName == "Tab" {
                    switch (value as! NSNumber).integerValue {
                    case 0:
                        cell.attributeValue.setImage(UIImage(named: "Custom"), forState: .Normal)
                        cell.attributeValue.backgroundColor = UIColor.clearColor()
                        cell.attributeValue.setTitle("", forState: UIControlState.Normal)
                    case 1:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item1"), forState: .Normal)
                    case 2:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item2"), forState: .Normal)
                    case 3:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item2"), forState: .Normal)
                    case 4:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item2"), forState: .Normal)
                    case 5:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item5"), forState: .Normal)
                    case 6:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item6"), forState: .Normal)
                    case 7:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item5"), forState: .Normal)
                    case 8:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item8"), forState: .Normal)
                    case 9:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item9"), forState: .Normal)
                    case 10:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item10"), forState: .Normal)
                    case 11:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item5"), forState: .Normal)
                    case 12:
                        cell.attributeValue.setTitle("", forState: .Normal)
                        cell.attributeValue.setImage(UIImage(named: "tab_item12"), forState: .Normal)
                    default:
                        cell.attributeValue.setTitle("\(value as! NSNumber)", forState: UIControlState.Normal)
                        cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                        cell.attributeValue.backgroundColor = UIColor.redColor()
                    }
                    cell.attributeValue.backgroundColor = UIColor.clearColor()
                    cell.attributeValue.setTitle("", forState: UIControlState.Normal)
                    cell.tag = item_index
                }
            }
            else if attr == "contentMode" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "content_mode0"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "content_mode1"), forState: .Normal)
                case 2:
                    cell.attributeValue.setImage(UIImage(named: "content_mode2"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
                cell.attributeValue.backgroundColor = UIColor.clearColor()
            }
            else if attr == "tableStyle" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "tableview_stylex"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "tableview_style1"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
                cell.attributeValue.backgroundColor = UIColor.clearColor()
                cell.attributeValue.setTitle("", forState: UIControlState.Normal)
            }
            else if attr == "cellStyle" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "cell_style0"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "cell_style1"), forState: .Normal)
                case 2:
                    cell.attributeValue.setImage(UIImage(named: "cell_style2"), forState: .Normal)
                case 3:
                    cell.attributeValue.setImage(UIImage(named: "cell_style3"), forState: .Normal)
                case 4:
                    cell.attributeValue.setImage(UIImage(named: "cell_style4"), forState: .Normal)
                case 5:
                    cell.attributeValue.setImage(UIImage(named: "cell_style5"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
            }
            else if attr == "fontStyle" {
                switch (value as! NSNumber).integerValue {
                case 0:
                    cell.attributeValue.setImage(UIImage(named: "font_style0"), forState: .Normal)
                case 1:
                    cell.attributeValue.setImage(UIImage(named: "font_style1"), forState: .Normal)
                case 2:
                    cell.attributeValue.setImage(UIImage(named: "font_style2"), forState: .Normal)
                case 3:
                    cell.attributeValue.setImage(UIImage(named: "font_style3"), forState: .Normal)
                case 4:
                    cell.attributeValue.setImage(UIImage(named: "font_style4"), forState: .Normal)
                case 5:
                    cell.attributeValue.setImage(UIImage(named: "font_style5"), forState: .Normal)
                default:
                    cell.attributeValue.setImage(UIImage(named: "image_default"), forState: .Normal)
                }
            }
            else {
                cell.attributeValue.setImage(nil, forState: UIControlState.Normal)
                cell.attributeValue.backgroundColor = UIColor.redColor()
                cell.attributeValue.setTitle("\(value as! NSNumber)", forState: UIControlState.Normal)
            }
            
        default:
            print("Not yet design")
        }
    }

}
