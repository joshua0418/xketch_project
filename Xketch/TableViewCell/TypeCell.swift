//
//  TypeCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/29.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class TypeCell: UITableViewCell {
    @IBOutlet var widgetImage: UIImageView!
    @IBOutlet var widgetName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
