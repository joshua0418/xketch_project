//
//  StepCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/24.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class StepCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UILabel!
    @IBOutlet var stepper: UIStepper!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAttributeValue(cell: StepCell, attr:String, value: AnyObject) {
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        cell.attributeName.adjustsFontSizeToFitWidth = true
        switch value {
        case is NSNumber:
            stepper.value = (value as! NSNumber).doubleValue
            switch entity {
            case is XKSEGMENT:
                cell.attributeValue.text = "\((value as! NSNumber).integerValue)"
                stepper.minimumValue = 1
                stepper.maximumValue = (entity as! XKSEGMENT).itemCount.doubleValue
            case is XKTAB:
                cell.attributeValue.text = "\((value as! NSNumber).integerValue)"
                stepper.minimumValue = 1
                stepper.maximumValue = (entity as! XKTAB).itemCount.doubleValue
            case is XKCANVAS:
                cell.attributeValue.text = "\((value as! NSNumber).integerValue)"
                stepper.minimumValue = 1
            default:
                cell.attributeValue.text = (value as! NSNumber).stringValue
            }
        default:
            print("Not yet design")
        }
    }

    @IBAction func stepValueAction(sender: UIStepper) {
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        switch entity {
        case is XKPICKERVIEW:
//            var widget = currentWidget?.contentView.subviews[0] as! UIPickerView
            let count = Int(sender.value) - (entity as! XKPICKERVIEW).columnCount.integerValue
            var datasource = (entity as! XKPICKERVIEW).dataSource.allObjects as! [Dictionary<String, AnyObject>]
            if count > 0 {
                datasource.append(["index": NSNumber(integer: datasource.count) , "data": ["C\(datasource.count + 1) Item"]])
            }
            else if count < 0 {
                datasource.removeLast()
            }
            datasource.sortInPlace {($0["index"] as! NSNumber).integerValue < ($1["index"] as! NSNumber).integerValue}
            (entity as! XKPICKERVIEW).columnCount = NSNumber(double: sender.value)
            attributeValue.text = "\(Int(sender.value))"
            entity.setValue(NSSet(array: datasource), forKey: "dataSource")
            NSNotificationCenter.defaultCenter().postNotificationName("updatePickerDataSource", object: nil, userInfo: ["entity": entity])
        case is XKSEGMENT:
            let widget = currentWidget?.contentView.subviews[0] as! UISegmentedControl
            entity.setValue(NSNumber(integer: Int(sender.value)), forKey: "selected")
            attributeValue.text = NSNumber(integer: Int(sender.value)).stringValue
            widget.selectedSegmentIndex = Int(sender.value - 1)
        case is XKTAB:
            let widget = currentWidget?.contentView.subviews[0] as! UITabBar
            entity.setValue(NSNumber(integer: Int(sender.value)), forKey: "selected")
            attributeValue.text = NSNumber(integer: Int(sender.value)).stringValue
            let items = widget.items as [UITabBarItem]!
            widget.selectedItem = items[Int(sender.value - 1)]
        case is XKCANVAS:
            let widget = currentWidget?.contentView.subviews[0] as! CanvasView
            attributeValue.text = NSNumber(integer: Int(sender.value)).stringValue
            widget.strokeSize = CGFloat(sender.value)
        default:
            print("step set error.")
        }
        CoreDataUtil.sharedInstance.save()
    }
}
