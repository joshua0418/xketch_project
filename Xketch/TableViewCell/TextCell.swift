//
//  TextCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/24.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell, UITextFieldDelegate {
    var itemNum = 0
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setAttributeValue(cell: TextCell, attr:String, value: AnyObject, item_index: Int) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        switch value {
        case is NSNumber:
            cell.attributeValue.text = (value as! NSNumber).stringValue
            cell.attributeValue.keyboardType = UIKeyboardType.NumberPad
        case is String:
            cell.attributeValue.text = value as? String
            if attr !=  "url" {
                itemNum = item_index
                cell.attributeValue.keyboardType = UIKeyboardType.Default
            }
            else {
                cell.attributeValue.keyboardType = UIKeyboardType.URL
            }
        default:
            print("Not yet design")
        }
    }
    
    // MARK: TextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        let tableView = textField.superview?.superview?.superview?.superview as! UITableView
        let cell = textField.superview?.superview as? TextCell
//        let window_height = tableView.window?.frame.size.height
        let cell_location_Y:CGFloat = 117.0 + cell!.frame.origin.y
        let keyboard_height:CGFloat = 352.0
        if cell_location_Y > keyboard_height {
            tableView.setContentOffset(CGPoint(x: 0, y: abs(keyboard_height - cell_location_Y)), animated: true)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        let tableView = textField.superview?.superview?.superview?.superview as! UITableView
        let cell = textField.superview?.superview as? TextCell
        let key = cell?.attributeName.text
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        switch entity {
        case is XKLABEL:
            let widget = currentWidget?.contentView.subviews[0] as! UILabel
            widget.text = textField.text
            entity.setValue(textField.text, forKey: key!)
        case is XKBUTTON:
            let widget = currentWidget?.contentView.subviews[0] as! UIButton
            widget.setTitle(textField.text, forState: .Normal)
            entity.setValue(textField.text, forKey: key!)
        case is XKTEXTFIELD:
            let widget = currentWidget?.contentView.subviews[0] as! UITextField
            widget.text = textField.text
            entity.setValue(textField.text, forKey: key!)
        case is XKSEGMENT:
            let widget = currentWidget?.contentView.subviews[0] as! UISegmentedControl
            if key == "text" {
                var items: Array<XKSEGMENT_ITEM> = (entity as! XKSEGMENT).items.allObjects as! [XKSEGMENT_ITEM]
                items.sortInPlace {$0.index.integerValue < $1.index.integerValue}
                items[itemNum].text = textField.text!
                widget.setTitle(textField.text, forSegmentAtIndex: itemNum)
            }
        case is XKTEXTVIEW:
            let widget = currentWidget?.contentView.subviews[0] as! UITextView
            widget.text = textField.text
            entity.setValue(textField.text, forKey: key!)
        case is XKCANVAS:
            let widget = currentWidget?.contentView.subviews[0] as! CanvasView
            widget.strokeSize = CGFloat((textField.text! as NSString).doubleValue)
            entity.setValue(NSNumber(double: (textField.text! as NSString).doubleValue), forKey: key!)
        case is XKWEBVIEW:
            let widget = currentWidget?.contentView.subviews[0] as! UIWebView
            let request:NSURLRequest = NSURLRequest(URL: NSURL(string: textField.text!)!)
            widget.loadRequest(request)
            entity.setValue(textField.text, forKey: key!)
        case is XKPICKERVIEW:
            let widget = currentWidget?.contentView.subviews[0] as! UIPickerView
            entity.setValue(NSNumber(integer: (textField.text! as NSString).integerValue), forKey: key!)
            var datasource:Array<Array<String>> = (entity as! XKPICKERVIEW).dataSource.allObjects as! [[String]]
            while (entity as! XKPICKERVIEW).columnCount.integerValue > datasource.count {
                datasource.append(["Column\(datasource.count + 1) TestItem"])
            }
            while (entity as! XKPICKERVIEW).columnCount.integerValue < datasource.count {
                datasource.removeLast()
            }
            entity.setValue(NSSet(array: datasource), forKey: "dataSource")
            NSNotificationCenter.defaultCenter().postNotificationName("updatePickerDataSource", object: nil, userInfo: ["entity": entity])
            widget.reloadAllComponents()
        case is XKNAV:
            let widget = currentWidget?.contentView.subviews[0] as! UINavigationBar
            guard let items = widget.items as [UINavigationItem]? else {
                return
            }
            let topItem = items[0]
            if key == "title" {
                topItem.title = textField.text
                entity.setValue(textField.text, forKey: key!)
            }
            else if key == "text" {

                var items = Array((entity as! XKNAV).items) as! [XKNAV_ITEM]
                if itemNum == 0 {
                    items = items.filter({$0.side == "left"}) 
                    items[0].text = textField.text!
                    topItem.leftBarButtonItem?.title = textField.text
                }
                else {
                    items = items.filter({$0.side == "right"}) 
                    items[0].text = textField.text!
                    topItem.rightBarButtonItem?.title = textField.text
                }
                
            }
        case is XKTAB:
            let widget = currentWidget?.contentView.subviews[0] as! UITabBar
            guard let tabItems = widget.items as [UITabBarItem]? else {
                return
            }
            tabItems[itemNum].title = textField.text
            var items = Array((entity as! XKTAB).items) as! [XKTAB_ITEM]
            items = items.filter({$0.index == self.itemNum + 1}) 
            items[0].text = textField.text!
        default:
            print("GG")
        }
        tableView.setContentOffset(CGPoint(x: 0, y: -44), animated: true)
        CoreDataUtil.sharedInstance.save()
    }
}
