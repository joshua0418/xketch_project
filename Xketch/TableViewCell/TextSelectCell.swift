//
//  TextSelectCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/26.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class TextSelectCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setAttributeValue(cell: TextSelectCell, attr:String, value: AnyObject) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        switch value {
        case is NSNumber:
            if attr == "alpha" {
                let alpha:NSNumber = (value as! NSNumber).doubleValue * 100
                cell.attributeValue.text = String(format: "%.0f", alpha.doubleValue)
            }
            else {
                cell.attributeValue.text = (value as! NSNumber).stringValue
            }
        case is String:
            cell.attributeValue.text = value as? String
        default:
            print("Not yet design")
        }
    }

}
