//
//  ColorSelectCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/26.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class ColorSelectCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAttributeValue(cell: ColorSelectCell, attr:String, value: AnyObject) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        switch value {
        case is UIColor:
            cell.attributeValue.layer.borderColor = UIColor.grayColor().CGColor
            cell.attributeValue.layer.borderWidth = 1
            cell.attributeValue.backgroundColor = value as? UIColor
        default:
            print("Not yet design")
        }
    }


}
