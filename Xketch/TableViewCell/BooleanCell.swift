//
//  BooleanCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/24.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class BooleanCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAttributeValue(cell: BooleanCell, attr:String, value: AnyObject) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        switch value {
        case is NSNumber:
            if value as! NSNumber == 1 {
                cell.attributeValue.setOn(true, animated: true)
            }
            else {
                cell.attributeValue.setOn(false, animated: true)
            }
            
        default:
            print("Not yet design")
        }
    }
    
    @IBAction func switchAction(sender: UISwitch) {
        let cell = sender.superview?.superview as? BooleanCell
        let key = cell?.attributeName.text
        let entity = currentWidget?.CDObject as! NATIVE_WIDGET
        entity.setValue(NSNumber(bool: sender.on), forKey: key!)
        CoreDataUtil.sharedInstance.save()

        if key == "enabled" {
            (currentWidget?.contentView.subviews[0] as! UIControl).enabled = sender.on
        }
        else if key == "state" {
            (currentWidget?.contentView.subviews[0] as! UISwitch).setOn(sender.on, animated: true)
        }
        else if key == "editable" {
            (currentWidget?.contentView.subviews[0] as! CanvasView).isEditable = sender.on
        }
        else if key == "blur" {
            if let imageView = currentWidget?.contentView.subviews[0] as? UIImageView {
                if #available(iOS 8.0, *) {
                    let effectView = imageView.subviews[0] as! UIVisualEffectView
                    effectView.hidden = !sender.on
                } else {
                    // Fallback on earlier versions
                }
            }
            else {
                (currentWidget?.contentView.subviews[0] as! UICollectionView).reloadData()
            }
        }
    }
    

}
