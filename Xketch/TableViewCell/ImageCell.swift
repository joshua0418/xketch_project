//
//  ImageCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/24.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAttributeValue(cell: ImageCell, attr:String, value: AnyObject?) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        switch value {
        case is NSSet:
            let datas = (value as! NSSet).allObjects as! [NSData]
            cell.attributeValue.setImage(UIImage(data: datas[0]) , forState: UIControlState.Normal)
            print("images{0}")
        case is NSData:
            if let data = value as? NSData {
                cell.attributeValue.setImage(UIImage(data: data), forState: .Normal)
            }
        default:
            cell.attributeValue.setImage(UIImage(named: "image_default"), forState: UIControlState.Normal)
            print("Not yet design")
        }
        

    }

}
