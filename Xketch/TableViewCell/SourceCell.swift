//
//  SourceCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/12.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class SourceCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
