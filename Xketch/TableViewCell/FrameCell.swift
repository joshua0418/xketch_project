//
//  FrameCell.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/26.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import MapKit

class FrameCell: UITableViewCell {
    var currentWidget:XKView?
    @IBOutlet var attributeName: UILabel!
    @IBOutlet var attributeValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAttributeValue(cell: FrameCell, attr:String, value: AnyObject) {
        cell.attributeName.adjustsFontSizeToFitWidth = true
        cell.attributeValue.adjustsFontSizeToFitWidth = true
        switch value {
        case is NSValue:
            let type = String.fromCString(value.objCType) ?? ""
            if type.hasPrefix("{CGRect") {
                var rect:CGRect = value.CGRectValue
                rect.origin.x += resizeIconSize / 2
                rect.origin.y += resizeIconSize / 2
                rect.size.width -= resizeIconSize
                rect.size.height -= resizeIconSize
                cell.attributeValue.text = "(\(Int(rect.origin.x)),\(Int(rect.origin.y))) , \(Int(rect.size.width))x\(Int(rect.size.height))"
            }
        case is NSData:
            let location = NSKeyedUnarchiver.unarchiveObjectWithData(value as! NSData) as! CLLocation
            cell.attributeValue.text = String(format: "%f,%f", location.coordinate.latitude, location.coordinate.longitude)
        default:
            print("Not yet design")
        }
    }

}
