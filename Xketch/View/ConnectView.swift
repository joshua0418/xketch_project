//
//  ConnectView.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/16.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit
import SpriteKit

protocol ConnectViewProtocol {
    func hideRightView()
    func setConnectInfo(Source Source: Int, Target: Int)
    func enterPage(pageNum pageNum: Int)
    func updatePageViewLocation(pageNum pageNum: Int, frame: CGRect)
    func reDrawConnectLine()
}

class ConnectView: UIView {
    var delegate: ConnectViewProtocol?
    
    var strokeSize:CGFloat! = 3.0
    var startPoint: CGPoint?
    var endPoint: CGPoint?
    var isConnect: Bool = false
    var isCut: Bool = true
    var detectView: UIView?
    var scrollView: StoryboardScrollView?
    var underView: UIView?
    var targetView: UIView?
    var touchOffset: CGPoint?
    var longPressTimer: NSTimer?
    var cutLineTimer: NSTimer?
    var secondsInterval: Double = 0.0
    var linksPoint: Array<Dictionary<String, AnyObject>> = [[:]]
    var contentOffset: CGPoint?
    
    func startLongPressCounter() {
        let interval = CFAbsoluteTimeGetCurrent() - secondsInterval
        if interval >= 0.2 {
            isConnect = false
            if let longPressTimer = self.longPressTimer {
                longPressTimer.invalidate()
                self.longPressTimer = nil
                if let underView = self.underView {
                    let previewImage = (underView as! StoryboardPageView).previewImage
                    previewImage.onFloating(UIColor.darkGrayColor())
                    isConnect = false
                    startPoint = nil
                    endPoint = nil
                    setNeedsDisplay()
                }
            }
        }
    }
    
    func startCutLineCounter() {
        let interval = CFAbsoluteTimeGetCurrent() - secondsInterval
        if interval >= 0.1 {
            if let cutLineTimer = self.cutLineTimer {
                cutLineTimer.invalidate()
                self.cutLineTimer = nil
                isCut = false
            }
        }
    }
    
//    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
//        if scrollView?.scrollEnabled == false {
//            println("no Scroll")
//            return self
//        }
//        else {
//            println("Scroll")
//            var view = self.superview?.hitTest(self.convertPoint(point, fromView: self), withEvent: event)
//            return view
//        }
//    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first as UITouch? else {
            return
        }
        startPoint = touch.locationInView(self)
        guard let subviews = detectView?.subviews as [UIView]? else {
            return
        }
        secondsInterval = CFAbsoluteTimeGetCurrent()
        if let startPoint = self.startPoint {
            if let view = self.getUnderViewByPoint(self, point: startPoint, subviews: subviews) {
                self.underView = view
                longPressTimer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: "startLongPressCounter", userInfo: nil, repeats: true)
                touchOffset = CGPoint(x: startPoint.x - view.frame.origin.x, y: startPoint.y - view.frame.origin.y)
            }
            else {
                cutLineTimer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: "startCutLineCounter", userInfo: nil, repeats: true)
                isCut = true
            }
        }
        
        isConnect = true
        delegate?.hideRightView()
        print("touch start")
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first as UITouch? else {
            return
        }
        endPoint = touch.locationInView(self)
        
        if let longPressTimer = self.longPressTimer {
            if CGPoint.distanceBetweenPoint(startPoint!, toPoint: endPoint!) > 15 {
                longPressTimer.invalidate()
                self.longPressTimer = nil
            }
        }
        
        if isConnect {
            guard let views = detectView?.subviews as [UIView]? else {
                return
            }
            let subviews = views.filter({$0 is StoryboardPageView})
            self.targetView = self.getUnderViewByPoint(self, point: endPoint!, subviews: subviews)
            if let targetView = self.targetView  {
                subviews.map({ ( view) -> Void in
                    (view as! StoryboardPageView).previewImage.onSinking()
                })
                if let underView = self.underView {
                    if targetView != underView {
                        (targetView as! StoryboardPageView).previewImage.onFloating(UIColor.redColor())
                    }
                }
            }
            else {
                subviews.map({ ( view) -> Void in
                    (view as! StoryboardPageView).previewImage.onSinking()
                })
            }
            setNeedsDisplay()
//            if isCut {
//                setNeedsDisplay()
//                println("cut state")
//                contentOffset = scrollView?.contentOffset
//            }
//            else {
//                println("drag scrollview state")
//                //                startPoint = nil
//                scrollView?.scrollEnabled = true
//                scrollView?.contentOffset = CGPoint(x: contentOffset!.x - (endPoint!.x - startPoint!.x), y: contentOffset!.y - (endPoint!.y - startPoint!.y))
//            }
        }
        else {
            if let underView = self.underView {
                underView.frame = CGRect(origin: CGPoint(x: endPoint!.x - touchOffset!.x, y: endPoint!.y - touchOffset!.y), size: underView.frame.size)
                startPoint = nil
                endPoint = nil
                linksPoint.removeAll(keepCapacity: false)
                delegate?.updatePageViewLocation(pageNum: underView.tag, frame: underView.frame)
                delegate?.reDrawConnectLine()
                setNeedsDisplay()
            }
        }
//        println("touch move")
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first as UITouch!
        endPoint = touch.locationInView(self)
        
        if let underView = self.underView {
            if startPoint != nil && endPoint != nil {
                if CGPoint.distanceBetweenPoint(startPoint!, toPoint: endPoint!) < 10 {
                    delegate?.enterPage(pageNum: underView.tag)
                }
            }
            let previewImage = (underView as! StoryboardPageView).previewImage
            previewImage.onSinking()
            if let targetView = self.targetView  {
                delegate?.setConnectInfo(Source: underView.tag, Target: targetView.tag)
            }
        }
        
        if let views = (detectView?.subviews as [UIView]!) {
            let subviews = views.filter({$0 is StoryboardPageView})
            subviews.map({ (let view) -> Void in
                (view as! StoryboardPageView).previewImage.onSinking()
            })
        }
        
//        scrollView?.scrollEnabled = false
        isConnect = false
        startPoint = nil
        endPoint = nil
        longPressTimer?.invalidate()
        longPressTimer = nil
        underView = nil
        targetView = nil
        setNeedsDisplay()
        
        print("touch end")
    }
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, strokeSize)
        
        for link in linksPoint {
            let start = (link["startPoint"] as! NSValue).CGPointValue()
            let end = (link["endPoint"]as! NSValue).CGPointValue()
            let m = link["m"] as! CGFloat
            
            UIColor.orangeColor().set()
            CGContextMoveToPoint(context, start.x, start.y)
            CGContextAddLineToPoint(context, end.x, end.y)
            CGContextStrokePath(context)
            
            UIColor.redColor().set()
            let d: CGFloat = 12
            let XLimit = 1 / sqrt(1 + m * m)
            let YLimit = m / sqrt(1 + m * m)
            let x = end.x > start.x ? end.x - d * XLimit : end.x + d * XLimit
            let y = end.x > start.x ? end.y - d * YLimit : end.y + d * YLimit
            let dm: CGFloat = -((x - end.x) / (y - end.y))
            CGContextMoveToPoint(context, end.x, end.y)   // A
            CGContextAddLineToPoint(context, x - d * (1 / sqrt(1 + dm * dm)), y - d * (dm / sqrt(1 + dm * dm)))    // C
            CGContextAddLineToPoint(context, x, y)   // D
            CGContextAddLineToPoint(context, x + d * (1 / sqrt(1 + dm * dm)), y + d * (dm / sqrt(1 + dm * dm)))    // B
            CGContextFillPath(context)
        }
        
        if startPoint != nil && endPoint != nil{
            UIColor.darkGrayColor().set()
            CGContextFillEllipseInRect(context, CGRectMake(startPoint!.x-5, startPoint!.y-5, 10, 10))
            UIColor.redColor().set()
            CGContextFillEllipseInRect(context, CGRectMake(endPoint!.x-5, endPoint!.y-5, 10, 10))
            UIColor.iOSblueColor().set()
            CGContextMoveToPoint(context, startPoint!.x, startPoint!.y)
            CGContextAddLineToPoint(context, endPoint!.x, endPoint!.y)
            CGContextStrokePath(context)
            
            if isCut {
                for (index, link) in linksPoint.enumerate() {
                    let start = (link["startPoint"] as! NSValue).CGPointValue()
                    let end = (link["endPoint"] as! NSValue).CGPointValue()
                    let leftLink_x = start.x < end.x ? start.x : end.x
                    let rightLink_x = start.x > end.x ? start.x : end.x
                    
                    let m = link["m"] as! CGFloat
                    let b = link["b"] as! CGFloat
                    
                    let T1 = m * startPoint!.x + b - startPoint!.y
                    let T2 = m * endPoint!.x + b - endPoint!.y
                    
                    if leftLink_x < startPoint!.x && rightLink_x > startPoint!.x {
                        if (T1 * T2) < 0 {
                            if let action = link["action"] as! ACTION? {
                                if index < linksPoint.count {
                                    linksPoint.removeAtIndex(index)
                                    CoreDataUtil.sharedInstance.deleteAction(action)
                                    isCut = false
//                                    setNeedsDisplay()
                                }
                            }
                        }
                    }
                }
            }
            
        }
    }
    
}
