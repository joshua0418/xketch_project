//
//  CanvasView.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/5.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class CanvasView: UIView {
    var strokeSize:CGFloat! = 1.0
    var strokeColor:UIColor! = UIColor.redColor()
    var stylusType:Int! = 0
    var paths: [UIBezierPath] = []
    var paths_color: [UIColor] = []
    var bezierPath: UIBezierPath = UIBezierPath()
    var isEditable: Bool!
    var datasource: XKViewProtocol?
  /*
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        bezierPath = UIBezierPath()
        bezierPath.lineCapStyle = kCGLineCapRound
        bezierPath.lineWidth = strokeSize
        bezierPath.miterLimit = 0.0
        
        let touch = touches.allObjects[0] as UITouch
        bezierPath.moveToPoint(touch.locationInView(self))
        paths.append(bezierPath)
        paths_color.append(strokeColor)
        
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.allObjects[0] as UITouch
        bezierPath.addLineToPoint(touch.locationInView(self))
        setNeedsDisplay()

    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        if let widget = datasource?.currentWidget() {
            let entity = widget.CDObject as XKCANVAS
            entity.paths = NSSet(array: paths)
            CoreDataUtil.sharedInstance.save()
        }
    }
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, strokeSize)
        for var i=0; i<paths.count; i++ {
//            paths_color[i].setStroke()
            UIColor.blackColor().set()
            paths[i].strokeWithBlendMode(kCGBlendModeNormal, alpha: 1.0)
        }
    }
*/
    
}
