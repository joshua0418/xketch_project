//
//  GestureView.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/11/13.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class Stroke: NSObject {
    var points: Array<NSValue>?
    var color: UIColor?
}

class GestureView: UIView, UIGestureRecognizerDelegate {
    
    enum ActiveMode {
        case Sketch, Selection, Move
    }
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var delegate: XKViewProtocol?
    var currentTouches: Dictionary<NSValue,Stroke>!
    var completeStrokes: Array<Stroke>!
    var timer:NSTimer!
    var currentMode:ActiveMode = .Sketch
    var containerView: ScreenView!
    var currentWidget: XKView?
    var selectedWidgets: Array<XKView>! = []
    var prevPoint:CGPoint?
    var originalPoint:CGPoint?
    var currentFrame:CGRect?
    var resizeType:String?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    // MARK:
    
    func setup() {
        currentTouches = [:]
        completeStrokes = []
    }
    
    // MARK: AutoAlign
    
    var widgetAlignPointX: Array<Dictionary<String, CGFloat>> = []
    var widgetAlignPointY: Array<Dictionary<String, CGFloat>> = []
    var holdPointX: CGFloat?
    var holdPointY: CGFloat?
    var touchPointX: CGFloat?
    var touchPointY: CGFloat?
    var holdX: Bool! = false
    var holdY: Bool! = false
    let snap_range:CGFloat = 4.0
    let offset:CGFloat = 7.0
    
    func updateWidgetAlignPoints(target: UIView?) {
        widgetAlignPointX.removeAll(keepCapacity: false)
        widgetAlignPointY.removeAll(keepCapacity: false)
        if let _ = target {
            for widget in containerView!.subviews.filter({$0 as? UIView != target}) {
                let dictX:Dictionary<String, CGFloat> = ["left":widget.frame.origin.x + resizeIconSize/2 , "center":widget.center.x, "right":(widget.frame.origin.x - resizeIconSize/2 + widget.frame.size.width)]
                let dictY:Dictionary<String, CGFloat> = ["top":widget.frame.origin.y + resizeIconSize/2 , "center":widget.center.y , "bottom":(widget.frame.origin.y - resizeIconSize/2 + widget.frame.size.height)]
                widgetAlignPointX.append(dictX)
                widgetAlignPointY.append(dictY)
            }
        }
        else {
            for widget in containerView!.subviews {
                let dictX:Dictionary<String, CGFloat> = ["left":widget.frame.origin.x + resizeIconSize/2 , "center":widget.center.x, "right":(widget.frame.origin.x - resizeIconSize/2 + widget.frame.size.width)]
                let dictY:Dictionary<String, CGFloat> = ["top":widget.frame.origin.y + resizeIconSize/2 , "center":widget.center.y, "bottom":(widget.frame.origin.y - resizeIconSize/2 + widget.frame.size.height)]
                widgetAlignPointX.append(dictX)
                widgetAlignPointY.append(dictY)
            }
        }
        print(widgetAlignPointX)
    }
    
    func clearScreen()
    {
        containerView?.beginPoint = []
        containerView?.endPoint = []
        containerView?.setNeedsDisplay()
    }
    
    // MARK: Touch Event Handler
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        if let timer = timer {
            timer.invalidate()
        }
        
        let touch = touches.first as UITouch!
        
        let key = NSValue(nonretainedObject: touch)
        let location:CGPoint = touch.locationInView(self)
        self.prevPoint = location
        
        let stroke = Stroke()
        stroke.points = [NSValue(CGPoint: location)]
        stroke.color = UIColor.blackColor()
        
        currentTouches[key] = stroke
        if let underViews = self.getWidgetByPoint(location) {
            if underViews.count == 1 {
                currentWidget = underViews[0]
            }
            else if underViews.count == 0 {
                if currentWidget != nil {
                    appDelegate.insertLog(command: "UnSelect", object: currentWidget!.CDObject.objectWithID(), project: delegate!._currentPage()!.project, page: delegate!._currentPage()!)
                }
                currentWidget = nil
            }
            else {
                if currentWidget != nil {
                    if !underViews.contains(currentWidget!) {
                        let views = underViews.filter {$0 != self.currentWidget}
                        currentWidget = views.count > 0 ? views[0] : currentWidget
                    }
                }
                else {
                    currentWidget = underViews[0]
                }
            }
        }
        if currentWidget != nil {
            updateWidgetAlignPoints(currentWidget)
            containerView.panView = currentWidget
            self.originalPoint = currentWidget!.frame.origin
        }
        
        switch currentMode {
        case .Sketch:
            NSLog("Sketch Mode(Begin)")
        case .Selection:
            if currentWidget != nil {
                resizeType = currentWidget!.getResizeTypeByLocation(prevPoint!)
                if resizeType != "No Resize" {
                    currentFrame = currentWidget!.frame
                    self.clearAll()
                    print("Switch to Resize Mode")
                }
                else {
                    var canvas:CanvasView?
                    if currentWidget!.widgetName == "Canvas" {
                        canvas = currentWidget?.contentView.subviews[0] as? CanvasView
                    }
                    
                    let entity = currentWidget?.CDObject as! NATIVE_WIDGET
                    
                    if currentWidget!.widgetName == "Navigation" || (entity is XKIMAGEVIEW && (entity as! XKIMAGEVIEW).style.integerValue == 3) {
                        currentMode = .Sketch
                        print("Switch to Sketch Mode(Navigation or Image Background Mode)")
                    }
                    else {
                        if canvas?.isEditable == true {
                            currentMode = .Selection
                            canvas?.touchesBegan(touches, withEvent: event)
                            self.clearAll()
                        }
                        else {
                            currentMode = currentWidget!.widgetName != "Tab" ? .Move : .Selection
                            self.clearAll()
                            print("Switch to Move Mode")
                        }
                        self.clearSelectedWidgets()
                        currentWidget!.isResize == true ? currentWidget!.showResize() : currentWidget!.hideResize()
                        currentWidget!.showSelection()
                        delegate?.showMenuView()
                        selectedWidgets.append(currentWidget!)
                        NSNotificationCenter.defaultCenter().postNotificationName("refreshTable", object: nil)
                    }
                }
                
            }
            else
            {
                currentMode = .Sketch
                self.clearSelectedWidgets()
                print("Switch to Sketch Mode (Screen)")
            }
            NSLog("Selection Mode(Begin)")
        default:
            NSLog("WTF Mode(Begin)")
        }
        
        self.setNeedsDisplay()
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesMoved(touches, withEvent: event)
        
        switch currentMode {
        case .Sketch:
            let touch = touches.first as UITouch!
            let key = NSValue(nonretainedObject: touch)
            let location:CGPoint = touch.locationInView(self)
            currentTouches[key]!.points!.append(NSValue(CGPoint:location))
//            NSLog("Sketch Mode(Move)")
        case .Selection:
            if resizeType != "No Resize" {
                let touch = touches.first as UITouch!
                let point:CGPoint = touch.locationInView(self)
                let wChange = point.x - prevPoint!.x
                let hChange = point.y - prevPoint!.y
                if currentWidget!.isResize == true && self.selectedWidgets.contains(currentWidget!) {
                    currentWidget!.setFrameByResizeType(currentFrame!, variation_X:wChange , variation_Y: hChange, type: resizeType!)
                }
//                println("Selection Mode(Resize)")
            }
            else {
                if currentWidget!.widgetName == "Canvas" {
                    let canvas = currentWidget?.contentView.subviews[0] as! CanvasView
                    if canvas.isEditable == true {
                        canvas.touchesMoved(touches, withEvent: event)
                    }
                }
            }
            
        case .Move:
            let touch = touches.first as UITouch!
//            var key = NSValue(nonretainedObject: touch)
            let location:CGPoint = touch.locationInView(self)
            let offset_X:CGFloat = self.prevPoint!.x - location.x
            let offset_Y:CGFloat = self.prevPoint!.y - location.y
            
            if currentWidget!.isMove == true {
                if touchPointX != nil {
                    let offsetX = touchPointX! - location.x
                    if abs(offsetX) > snap_range {
                        currentWidget!.frame.origin.x += offsetX < 0 ? offset : -offset
                        touchPointX = nil
                        holdX = false
                        clearScreen()
                    }
                }
                
                if touchPointY != nil {
                    let offsetY = touchPointY! - location.y
                    if abs(offsetY) > snap_range {
                        currentWidget!.frame.origin.y += offsetY < 0 ? offset : -offset
                        touchPointY = nil
                        holdY = false
                        clearScreen()
                    }
                }
                
                // check isHold
                if holdX == true {
                    currentWidget!.frame.origin.y = holdY == false ? self.originalPoint!.y - offset_Y : currentWidget!.frame.origin.y
                    // edge check
                    if currentWidget!.frame.origin.y < (-resizeIconSize/2 + statusBarHeight) {
                        currentWidget!.frame.origin.y = -resizeIconSize/2  + statusBarHeight
                    }
                    else if (currentWidget!.frame.origin.y + currentWidget!.frame.size.height) > (containerView.frame.size.height + resizeIconSize/2) {
                        currentWidget!.frame.origin.y = containerView.frame.size.height + resizeIconSize/2 - currentWidget!.frame.size.height
                    }
                }
                if holdY == true {
                    currentWidget!.frame.origin.x = holdX == false ? self.originalPoint!.x - offset_X : currentWidget!.frame.origin.x
                    // edge check
                    if currentWidget!.frame.origin.x < -resizeIconSize/2 {
                        currentWidget!.frame.origin.x = -resizeIconSize/2
                    }
                    else if (currentWidget!.frame.origin.x + currentWidget!.frame.size.width) > (containerView.frame.size.width + resizeIconSize/2) {
                        currentWidget!.frame.origin.x = containerView.frame.size.width + resizeIconSize/2 - currentWidget!.frame.size.width
                    }
                }
                if holdX == false && holdY == false {
                    if (self.originalPoint!.x - offset_X) > -resizeIconSize/2 {
                        currentWidget!.frame.origin.x = self.originalPoint!.x - offset_X
                        if (self.originalPoint!.x - offset_X + currentWidget!.frame.size.width) > (containerView.frame.size.width + resizeIconSize/2) {
                            currentWidget!.frame.origin.x = containerView.frame.size.width + resizeIconSize/2 - currentWidget!.frame.size.width
                        }
                    }
                    else {
                        currentWidget!.frame.origin.x = -resizeIconSize/2
                    }
                    
                    if (self.originalPoint!.y - offset_Y) > (-resizeIconSize/2 + statusBarHeight) {
                        currentWidget!.frame.origin.y = self.originalPoint!.y - offset_Y
                        if (self.originalPoint!.y - offset_Y + currentWidget!.frame.size.height) > (containerView.frame.size.height + resizeIconSize/2) {
                            currentWidget!.frame.origin.y = containerView.frame.size.height + resizeIconSize/2 - currentWidget!.frame.size.height
                        }
                    }
                    else {
                        currentWidget!.frame.origin.y = -resizeIconSize/2  + statusBarHeight
                    }
                }
                
                let senderAlignPointX: Array<CGFloat>! = [currentWidget!.frame.origin.x + resizeIconSize/2, currentWidget!.center.x, (currentWidget!.frame.origin.x - resizeIconSize/2 + currentWidget!.frame.size.width)]
                let senderAlignPointY: Array<CGFloat>! = [currentWidget!.frame.origin.y + resizeIconSize/2, currentWidget!.center.y, (currentWidget!.frame.origin.y - resizeIconSize/2 + currentWidget!.frame.size.height)]
                
                // check X axis
                for (pos,x) in senderAlignPointX.enumerate() {
                    for (index, dictX) in widgetAlignPointX.enumerate() {
                        if holdX == false {
                            if (abs(dictX["right"]! - x)) <= snap_range && pos != 1 {
                                touchPointX = location.x
                                holdPointX = dictX["right"]
                                holdPointY = widgetAlignPointY[index]["top"]!
                                holdX = true
                                if pos == 0 {
                                    currentWidget!.frame.origin.x = dictX["right"]! - resizeIconSize/2
                                }
                                else {
                                    currentWidget!.frame.origin.x = dictX["right"]! - currentWidget!.frame.size.width + resizeIconSize/2
                                }
                                containerView.addPoint(CGPoint(x:holdPointX!, y: holdPointY!), axis: 0)
                            }
                            if (abs(dictX["left"]! - x)) <= snap_range && pos != 1 {
                                touchPointX = location.x
                                holdPointX = dictX["left"]
                                holdPointY = widgetAlignPointY[index]["top"]!
                                holdX = true
                                if pos == 2 {
                                    currentWidget!.frame.origin.x = dictX["left"]! - currentWidget!.frame.size.width + resizeIconSize/2
                                }
                                else {
                                    currentWidget!.frame.origin.x = dictX["left"]! - resizeIconSize/2
                                }
                                containerView.addPoint(CGPoint(x:holdPointX!, y: holdPointY!), axis: 0)
                            }
                            if (abs(dictX["center"]! - x)) <= snap_range && pos == 1 {
                                touchPointX = location.x
                                holdPointX = dictX["center"]
                                holdPointY = widgetAlignPointY[index]["top"]!
                                holdX = true
                                currentWidget!.center.x = dictX["center"]!
                                containerView.addPoint(CGPoint(x:holdPointX!, y: holdPointY!), axis: 0)
                            }
                        }
                    }
                }
                
                // check Y axis
                for (pos,y) in senderAlignPointY.enumerate() {
                    for (index, dictY) in widgetAlignPointY.enumerate() {
                        if holdY == false {
                            if abs(dictY["bottom"]! - y) <= snap_range && pos != 1 {
                                touchPointY = location.y
                                holdPointY = dictY["bottom"]
                                holdPointX = widgetAlignPointX[index]["left"]!
                                holdY = true
                                if pos == 0 {
                                    currentWidget!.frame.origin.y = dictY["bottom"]! - resizeIconSize/2
                                }
                                else {
                                    currentWidget!.frame.origin.y = dictY["bottom"]! - currentWidget!.frame.size.height + resizeIconSize/2
                                }
                                containerView.addPoint(CGPoint(x:holdPointX!, y: holdPointY!), axis: 1)
                            }
                            if abs(dictY["top"]! - y) <= snap_range && pos != 1 {
                                touchPointY = location.y
                                holdPointY = dictY["top"]
                                holdPointX = widgetAlignPointX[index]["left"]!
                                holdY = true
                                if pos == 2 {
                                    currentWidget!.frame.origin.y = dictY["top"]! - currentWidget!.frame.size.height + resizeIconSize/2
                                }
                                else {
                                    currentWidget!.frame.origin.y = dictY["top"]! - resizeIconSize/2
                                }
                                containerView.addPoint(CGPoint(x:holdPointX!, y: holdPointY!), axis: 1)
                            }
                            if abs(dictY["center"]! - y) <= snap_range && pos == 1 {
                                touchPointY = location.y
                                holdPointY = dictY["center"]
                                holdPointX = widgetAlignPointX[index]["left"]!
                                holdY = true
                                currentWidget!.center.y = dictY["center"]!
                                containerView.addPoint(CGPoint(x:holdPointX!, y: holdPointY!), axis: 1)
                            }
                        }
                    }
                }
            }
            
        }
        
        self.setNeedsDisplay()
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        if currentWidget != nil {
            if currentWidget!.widgetName == "Canvas" {
                let canvas = currentWidget?.contentView.subviews[0] as! CanvasView
                if canvas.isEditable == true {
                    canvas.touchesEnded(touches, withEvent: event)
                }
            }
        }
        self.endTouches(touches)
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        
        self.endTouches(touches!)
    }
    
    func endTouches(touches: NSSet) {
        for touch in touches {
            let key = NSValue(nonretainedObject: touch)
            let stroke:Stroke? = currentTouches[key]
            stroke?.color = self.randomColor()
            
            if stroke?.points?.count == 1 {    // point-stroke switch to selection mode
                if currentWidget != nil {
                    if selectedWidgets.count == 0 {
                        currentWidget!.isResize == true ? currentWidget!.showResize() : currentWidget!.hideResize()
                        currentWidget!.showSelection()
                        delegate?.showMenuView()
                        selectedWidgets.append(currentWidget!)
                        currentMode = .Selection
                        appDelegate.insertLog(command: "Select", object: currentWidget!.CDObject.objectWithID(), project: delegate!._currentPage()!.project, page: delegate!._currentPage()!)
                        print("Switch to Selection Mode by touch widget")
                    }
                    else {
                        self.clearSelectedWidgets()
                        currentWidget!.isResize == true ? currentWidget!.showResize() : currentWidget!.hideResize()
                        currentWidget!.showSelection()
                        delegate?.showMenuView()
                        selectedWidgets.append(currentWidget!)
                        currentMode = .Selection
                        appDelegate.insertLog(command: "Select", object: currentWidget!.CDObject.objectWithID(), project: delegate!._currentPage()!.project, page: delegate!._currentPage()!)
                        print("Switch to Selection Mode by touch widget")
                    }
                }
                else {
                    currentMode = .Sketch
                    self.clearSelectedWidgets()
                    print("Switch to Sketch Mode by touch screen")
                }
                self.clearAll()
            }
            else {
                switch currentMode {
                case .Sketch:
                    if stroke != nil {
                        // check stroke is complete?
                        var flag = false
                        let start: CGPoint! = stroke?.points![0].CGPointValue()
                        for value in stroke!.points! {
                            let end = value.CGPointValue()
                            if CGPoint.distanceBetweenPoint(start, toPoint: end) > 60 {
                                flag = true
                                break
                            }
                            else if currentWidget?.widgetName == "Navigation" {
                                flag = true
                                break
                            }
                        }
                        
                        if flag == false {
                            print("too short la !")
                            self.clearAll()
                        }
                        else {
                            completeStrokes.append(stroke!)
                            currentTouches.removeValueForKey(key)
                            timer = NSTimer.scheduledTimerWithTimeInterval(0.8, target: self, selector: Selector("startRecognize"), userInfo: nil, repeats: false)
                        }
                    }
                    NSLog("Sketch Mode(End)")
                case .Selection:
                    let element:NATIVE_WIDGET = currentWidget!.CDObject as! NATIVE_WIDGET
                    element.frame = NSValue(CGRect: currentWidget!.frame)
                    appDelegate.insertLog(command: "Select", object: currentWidget!.CDObject.objectWithID(), project: delegate!._currentPage()!.project, page: delegate!._currentPage()!)
                    CoreDataUtil.sharedInstance.save()
                    self.clearAll()
                    NSLog("Selection Mode(End)")
                case .Move:
                    let element:NATIVE_WIDGET = currentWidget!.CDObject as! NATIVE_WIDGET
                    element.frame = NSValue(CGRect: currentWidget!.frame)
                    appDelegate.insertLog(command: "Move", object: currentWidget!.CDObject.objectWithID(), project: delegate!._currentPage()!.project, page: delegate!._currentPage()!)
                    CoreDataUtil.sharedInstance.save()
                    self.clearAll()
                    currentMode = .Selection
                    
                    clearScreen()
                    holdX = false
                    holdY = false
                    touchPointX = nil
                    touchPointY = nil
                    holdPointX = nil
                    holdPointY = nil
                    print("Move Mode(End)")
                
                }
            }
            
        }
        NSNotificationCenter.defaultCenter().postNotificationName("refreshTable", object: nil)
        self.setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        CGContextSetLineWidth(context, 3.0)
        CGContextSetLineCap(context, CGLineCap.Round)
        
        for var i=0; i<completeStrokes.count; i++ {
            let stroke:Stroke = completeStrokes[i]
            self.drawStroke(stroke, inContext: context)
        }
        
        for touchValue in currentTouches {
            let stroke:Stroke! = currentTouches[touchValue.0]
            self.drawStroke(stroke, inContext: context)
        }
        
    }
    
    func drawStroke(stroke: Stroke, inContext context:CGContextRef) {
        stroke.color?.set()
        
        var points:Array<NSValue> = stroke.points!
        var point:CGPoint = points[0].CGPointValue()
        
        //        CGContextFillRect(context, CGRectMake(point.x-5, point.y-5, 10, 10))
        
        CGContextMoveToPoint(context, point.x, point.y)
        for (var i=1; i<points.count;i++) {
            point = points[i].CGPointValue()
            CGContextAddLineToPoint(context, point.x, point.y)
        }
        CGContextStrokePath(context);
    }
    
    func randomColor() -> UIColor {
        let hue = CGFloat(arc4random() % 128) / 256.0
        let saturation = CGFloat(arc4random() % 128) / 256.0 + CGFloat(0.5)
        let brightness = CGFloat(arc4random() % 128) / 256.0 + CGFloat(0.5)
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    
    func clearAll() {
        completeStrokes = []
        currentTouches = [:]
        
        for recognizer in self.gestureRecognizers! {
            if recognizer is DollarPGestureRecognizer {
                recognizer.reset()
            }
        }
        
        self.setNeedsDisplay()
    }
    // MARK:
    
    func startRecognize() {
        for recognizer in self.gestureRecognizers! {
            if recognizer is DollarPGestureRecognizer {
                (recognizer as! DollarPGestureRecognizer).recognize()
                self.clearAll()
            }
        }
    }
    
    func getWidgetByPoint(point:CGPoint) -> [XKView]? {
        var widgets: Array<XKView> = Array()
        for view in Array((self.containerView.subviews as! [XKView]).reverse()) {
            let frame = self.convertRect(view.frame, fromView: view.superview)
            
            if CGRectContainsPoint(frame, point) {
                widgets.append(view)
            }
        }
        return widgets
    }
    
    func clearSelectedWidgets() {
        for widget in selectedWidgets {
            widget.hideResize()
            widget.hideSelection()
        }
        delegate?.hideMenuView()
        selectedWidgets.removeAll(keepCapacity: false)
    }
}
