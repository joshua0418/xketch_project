//
//  StoryboardPageView.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/16.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

class StoryboardPageView: UIView {

    @IBOutlet var pageName: UITextField!
    @IBOutlet var previewImage: UIImageView!

}
