//
//  XKView.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/11/12.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

var resizeIconSize:CGFloat = 30.0

class XKView: UIView {
    
    var contentView: UIView!
    var widgetName: String!
    var alterableTypes: Array<String>! = []
    var isMove: Bool! = true
    var isResize: Bool! = true
    var originalFrame: CGRect!
    var CDObject: NSManagedObject!
    var resizeHorizontal: UIImageView! = UIImageView(image: UIImage(named: "resize_horizontal"))
    var resizeVertical: UIImageView! = UIImageView(image: UIImage(named: "resize_vertical"))
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
//    convenience init() {
//        self.init()
//        self.setup()
//    }
    
    override init(frame: CGRect) {
        originalFrame = frame
        let newframe:CGRect = CGRectMake(frame.origin.x - resizeIconSize/2, frame.origin.y - resizeIconSize/2 , frame.size.width + resizeIconSize, frame.size.height + resizeIconSize)
        super.init(frame: newframe)
        self.setup()
    }
    
    func setFrameByResizeType(currentFrame:CGRect, variation_X:CGFloat, variation_Y:CGFloat, type:String) {
        if type == "Horizontal" {
            var newWidth:CGFloat = 0
            if (currentFrame.origin.x + currentFrame.size.width + variation_X) > screenWidth + resizeIconSize/2 {
                newWidth = screenWidth - currentFrame.origin.x + resizeIconSize/2
            }
            else if (currentFrame.size.width + variation_X) < (currentFrame.origin.x + resizeIconSize/2) {
                newWidth = currentFrame.origin.x + resizeIconSize/2
            }
            else {
                newWidth = currentFrame.size.width + variation_X
            }
            self.frame = CGRectMake(currentFrame.origin.x, currentFrame.origin.y, newWidth, currentFrame.size.height )
        }
        else {
            var newHeight:CGFloat = 0
            if (currentFrame.origin.y + currentFrame.size.height + variation_Y) > screenHeight + resizeIconSize/2 {
                newHeight = screenHeight - currentFrame.origin.y + resizeIconSize/2
            }
            else if (currentFrame.size.height + variation_Y) < resizeIconSize * 2 {
                newHeight = resizeIconSize * 2
            }
            else {
                newHeight = currentFrame.size.height + variation_Y
            }
            self.frame = CGRectMake(currentFrame.origin.x, currentFrame.origin.y, currentFrame.size.width, newHeight)
        }
        reDefineFrame()
        reDefineResizeIcon()
    }
    
    func reDefineFrame() {
        contentView.frame = CGRectMake(resizeIconSize/2, resizeIconSize/2, self.frame.size.width - resizeIconSize, self.frame.size.height - resizeIconSize)
        let object:UIView = self.contentView.subviews[0] 
        object.frame = CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height)
        if CDObject is XKIMAGEVIEW {
            if object is UIImageView {
                if #available(iOS 8.0, *) {
                    let effectView = object.subviews[0] as! UIVisualEffectView
                    effectView.frame = object.frame
                    if (CDObject as! XKIMAGEVIEW).style.integerValue == 4 {
                        object.layer.cornerRadius = object.frame.size.width > object.frame.size.height ? object.frame.size.width / 2 : object.frame.size.height / 2
                    }
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }
    
    func reDefineResizeIcon() {
        resizeHorizontal.frame = CGRect(origin: CGPoint(x: contentView.frame.size.width, y: contentView.frame.size.height/2), size: CGSize(width: resizeIconSize, height: resizeIconSize))
        resizeVertical.frame = CGRect(origin: CGPoint(x: contentView.frame.size.width/2 , y: contentView.frame.size.height), size: CGSize(width: resizeIconSize, height: resizeIconSize))
        isResize == true ? showResize() : hideResize()
    }
    
    func setup() {
        self.userInteractionEnabled = true
        
        contentView = UIView(frame: CGRectMake(resizeIconSize/2, resizeIconSize/2, self.frame.size.width - resizeIconSize, self.frame.size.height - resizeIconSize))
//        ==debug==
//        self.layer.borderColor = UIColor.redColor().CGColor
//        self.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.blueColor().CGColor
        contentView.layer.borderWidth = 0
        self.addSubview(contentView)
        
        reDefineResizeIcon()
        resizeHorizontal.layer.borderWidth = 1
        resizeVertical.layer.borderWidth = 1
        resizeHorizontal.backgroundColor = UIColor.grayColor()
        resizeVertical.backgroundColor = UIColor.grayColor()
        resizeHorizontal.alpha = 0.8
        resizeVertical.alpha = 0.8
        resizeHorizontal.layer.cornerRadius = 15
        resizeVertical.layer.cornerRadius = 15
        self.addSubview(resizeHorizontal)
        self.addSubview(resizeVertical)
    }
    
    func showSelection() {
        contentView.layer.borderWidth = 1
    }
    
    func hideSelection() {
        contentView.layer.borderWidth = 0
    }
    
    func showResize() {
        resizeHorizontal.hidden = false
        resizeVertical.hidden = false
    }
    
    func hideResize() {
        resizeHorizontal.hidden = true
        resizeVertical.hidden = true
    }
    
    func resetLayout(resizeable resizeable: Bool) {
        isResize = resizeable
        reDefineFrame()
        reDefineResizeIcon()
    }
    
    func getResizeTypeByLocation(location:CGPoint) -> String {
        let array:Array<UIImageView> = [resizeHorizontal, resizeVertical]
        for resize in array {
            var frame = self.superview!.convertRect(resize.frame, fromView: resize.superview)
            frame.origin.y -= resizeIconSize/2
            
            if CGRectContainsPoint(frame, location) {
                if resize.isEqual(resizeHorizontal) {
                    return "Horizontal"
                }
                else {
                    return "Vertical"
                }
            }
        }
        return "No Resize"
    }
}
