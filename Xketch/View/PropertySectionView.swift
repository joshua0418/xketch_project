//
//  PropertySectionView.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/30.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import UIKit

protocol PropertySectionViewProtocol {
    func removeSectionView(section: Int)
}

class PropertySectionView: UIView {
    var delegate: PropertySectionViewProtocol?
    var datasource: PropertySectionViewProtocol?

    @IBOutlet var sectionName: UILabel!
    @IBOutlet var removeButton: UIButton!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func sectionDeleteAction(sender: UIButton) {
        delegate?.removeSectionView(self.tag)
        NSNotificationCenter.defaultCenter().postNotificationName("refreshTable", object: nil)
    }
}
