//
//  ScreenView.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/11/20.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import UIKit
import CoreData

class ScreenView: UIView {
    
    var beginPoint: Array<NSValue>? = []
    var endPoint: Array<NSValue>? = []
    var panView: XKView?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.layer.borderWidth = 1
    }
    
    func isExistWidget(name:String) -> Bool {
        for widget in self.subviews as! [XKView] {
            if widget.widgetName == name {
                return true
            }
        }
        return false
    }
    
    func getBelowWidget(result:DollarResult) -> XKView? {
        let result_RT_x = result.origin.x + result.size.width
        let result_RB_y = result.origin.y + result.size.height
        
        // check for navigation bar
        var widgets = self.subviews as! [XKView]
        widgets = widgets.filter {$0.CDObject is XKNAV}
        
        for widget in widgets {
            let widget_RT_x = widget.frame.origin.y + widget.frame.size.width
            let widget_RB_y = widget.frame.origin.y + widget.frame.size.height
            
            if result_RT_x > widget.frame.origin.y && result_RT_x < widget_RT_x &&
                result_RB_y > widget.frame.origin.y && result_RB_y < widget_RB_y {
                    return widget
            }
            else if result_RT_x > widget.frame.origin.x && result_RT_x < widget_RT_x &&
                result.origin.y > widget.frame.origin.y && result.origin.y < widget_RB_y {
                    return widget
            }
            else if result.origin.x > widget.frame.origin.x && result.origin.x < widget_RT_x &&
                result_RB_y > widget.frame.origin.y && result_RB_y < widget_RB_y {
                    return widget
            }
            else if result.origin.x > widget.frame.origin.x && result.origin.x < widget_RT_x &&
                result.origin.y > widget.frame.origin.y && result.origin.y < widget_RB_y {
                    return widget
            }
        }
        return nil
    }
    
    func getFullScreenRect() -> CGRect {
        var nav:CGFloat = 0
        var tab:CGFloat = 0
        for widget in self.subviews as! [XKView] {
            if widget.widgetName == "Navigation" {
                nav = navigationBarHeight
            }
            else if widget.widgetName == "Tab" {
                tab = tabBarHeight
            }
        }
        return CGRectMake(0, (statusBarHeight + nav), screenWidth, (screenHeight - statusBarHeight - nav - tab))
    }
    
    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 1)
        UIColor.redColor().setStroke()
        if let beginPoint = beginPoint {
            for var i=0; i<beginPoint.count; i++ {
                let point = beginPoint[i].CGPointValue()
                CGContextMoveToPoint(context, point.x, point.y)
                if let endPoint = endPoint {
                    let point = endPoint[i].CGPointValue()
                    CGContextAddLineToPoint(context, point.x, point.y)
                }
            }
        }
        CGContextStrokePath(context);
    }

    
    func addPoint(widgetPosition: CGPoint, axis: Int) {
        beginPoint?.append(NSValue(CGPoint: widgetPosition))
        if axis == 0 {
            endPoint?.append(NSValue(CGPoint: CGPoint(x: widgetPosition.x, y: (panView!.frame.origin.y + panView!.frame.size.height))))
        }
        else {
            endPoint?.append(NSValue(CGPoint: CGPoint(x: (panView!.frame.origin.x + panView!.frame.size.width), y: widgetPosition.y)))
        }
        self.setNeedsDisplay()
    }
    
}
