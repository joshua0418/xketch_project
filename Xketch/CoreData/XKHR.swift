//
//  XKHR.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/28.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKHR: Xketch.NATIVE_WIDGET {

    @NSManaged var style: NSNumber

    func updateAttributes() {
        attributes.append("style")
    }
}
