//
//  XKMAPVIEW.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKMAPVIEW: Xketch.NATIVE_WIDGET {

    @NSManaged var coordinate: NSData
    @NSManaged var markDataSource: AnyObject
    @NSManaged var markerImage: NSData
    @NSManaged var zoomLevel: NSNumber

    func updateAttributes() {
        attributes.append("coordinate")
//        attributes.append("markDataSource")
//        attributes.append("markerImage")
//        attributes.append("zoomLevel")
    }
}
