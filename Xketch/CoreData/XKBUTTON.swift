//
//  XKBUTTON.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/2.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKBUTTON: NATIVE_WIDGET {

    @NSManaged var style: NSNumber
    @NSManaged var borderStyle: NSNumber
    @NSManaged var image: NSData
    @NSManaged var text: String

    func updateAttributes() {
        attributes.append("style")
        attributes.append("borderStyle")
        attributes.append("image")
        attributes.append("text")
    }
}
