//
//  XKSEGMENT_ITEM.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKSEGMENT_ITEM: Xketch.NATIVE_WIDGET {

    @NSManaged var alignment: NSNumber
    @NSManaged var index: NSNumber
    @NSManaged var image: NSData
    @NSManaged var text: String
    @NSManaged var segment: XKSEGMENT
    
    func updateAttributes() {
        attributes.removeAll(keepCapacity: false)
        attributes.append("image")
        attributes.append("text")
    }
}
