//
//  XKLABEL.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/2.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKLABEL: NATIVE_WIDGET {

    @NSManaged var fontStyle: NSNumber
    @NSManaged var text: String
    @NSManaged var alignment: NSNumber
    
    func updateAttributes() {
        attributes.append("fontStyle")
        attributes.append("text")
        attributes.append("alignment")
    }
    
}
