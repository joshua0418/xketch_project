//
//  XKPICKERVIEW.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKPICKERVIEW: Xketch.NATIVE_WIDGET {

    @NSManaged var columnCount: NSNumber
    @NSManaged var dataSource: NSSet

    func updateAttributes() {
        attributes.append("columnCount")
        attributes.append("dataSource")
    }
}
