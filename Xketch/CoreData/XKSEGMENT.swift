//
//  XKSEGMENT.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKSEGMENT: Xketch.NATIVE_WIDGET {

    @NSManaged var itemCount: NSNumber
    @NSManaged var selected: NSNumber
    @NSManaged var items: NSSet

    func updateAttributes() {
//        attributes.append("itemCount")
        attributes.append("selected")
        attributes.append("items")
    }
    
    func insertSubAttributes() {
        attributes.removeLast()
        attributes.append("text")
        attributes.append("image")
    }
}
