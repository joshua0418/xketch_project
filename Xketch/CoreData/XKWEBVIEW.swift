//
//  XKWEBVIEW.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKWEBVIEW: Xketch.NATIVE_WIDGET {

    @NSManaged var url: String

    func updateAttributes() {
        attributes.append("url")
    }
}
