//
//  XKTABLEVIEW_SECTION.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKTABLEVIEW_SECTION: Xketch.NATIVE_WIDGET {

    @NSManaged var index: NSNumber
    @NSManaged var alignment: NSNumber
    @NSManaged var title: String
    @NSManaged var table: NSManagedObject

    func updateAttributes() {
        attributes.removeAll(keepCapacity: false)
        attributes.append("alignment")
        attributes.append("title")
    }
}
