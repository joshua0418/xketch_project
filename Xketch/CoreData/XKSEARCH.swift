//
//  XKSEARCH.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/9.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKSEARCH: Xketch.NATIVE_WIDGET {

    @NSManaged var placeholder: String
    @NSManaged var style: NSNumber
    @NSManaged var buttonStyle: NSNumber
    @NSManaged var showCancel: NSNumber

}
