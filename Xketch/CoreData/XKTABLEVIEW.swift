//
//  XKTABLEVIEW.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKTABLEVIEW: Xketch.NATIVE_WIDGET {

    @NSManaged var cellHeight: NSNumber
    @NSManaged var cellStyle: NSNumber
    @NSManaged var sectionHeight: NSNumber
    @NSManaged var separator: NSNumber
    @NSManaged var tableStyle: NSNumber
    @NSManaged var cell: NSSet
    @NSManaged var section: NSSet
    
    func updateAttributes() {
//        attributes.append("cellHeight")
        attributes.append("cellStyle")
//        attributes.append("sectionHeight")
//        attributes.append("separator")
        attributes.append("tableStyle")
//        attributes.append("cell")
//        attributes.append("section")
    }
}
