//
//  XKTABLEVIEW_CELL.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKTABLEVIEW_CELL: Xketch.NATIVE_WIDGET {

    @NSManaged var index: NSNumber
    @NSManaged var images: AnyObject
    @NSManaged var subTitles: AnyObject
    @NSManaged var titles: AnyObject
    @NSManaged var table: NSManagedObject

    func updateAttributes() {
        attributes.removeAll(keepCapacity: false)
        attributes.append("images")
        attributes.append("subTitles")
        attributes.append("titles")
    }
}
