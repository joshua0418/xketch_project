//
//  PROJECT.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/2.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class PROJECT: NSManagedObject {
    
    @NSManaged var name: String
    @NSManaged var pages: NSSet
    @NSManaged var index: NSNumber

    override func awakeFromInsert() {
        super.awakeFromInsert()
        self.incrementalCounter = 1
        if let count = NSUserDefaults.standardUserDefaults().integerForKey("ProjectCount") as Int? {
            self.index = count
            var indexCount = count
            NSUserDefaults.standardUserDefaults().setInteger(++indexCount, forKey: "ProjectCount")
        }
        else {
            NSUserDefaults.standardUserDefaults().setInteger(0, forKey: "ProjectCount")
            self.index = 0
        }
        
    }
    
}
