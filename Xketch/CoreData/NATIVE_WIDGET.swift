//
//  NATIVE_WIDGET.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/2.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class NATIVE_WIDGET: NSManagedObject {

    @NSManaged var alpha: NSNumber
    @NSManaged var name: String
    @NSManaged var frame: NSValue
    @NSManaged var fontColor: AnyObject
    @NSManaged var backgroundColor: AnyObject
    @NSManaged var enabled: NSNumber
    @NSManaged var page: PAGE
    @NSManaged var action: ACTION?
    @NSManaged var zIndex: NSNumber
    
    var attributes: Array<String>! = []
    
    func setTextAttribute() {
        attributes = ["alpha", "backgroundColor", "fontColor"]
    }
    
    func setGraphicAttribute() {
        attributes = ["alpha", "backgroundColor"]
    }
    
    func setActionAttribute() {
        attributes = ["alpha", "backgroundColor", "enabled", "fontColor"]
    }
}
