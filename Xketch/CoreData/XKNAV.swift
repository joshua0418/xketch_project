//
//  XKNAV.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/7.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKNAV: Xketch.NATIVE_WIDGET {

    @NSManaged var fontStyle: NSNumber
    @NSManaged var title: String
    @NSManaged var items: NSSet

    func updateAttributes() {
//        attributes.append("fontStyle")
        attributes.append("title")
        attributes.append("items")
    }
}
