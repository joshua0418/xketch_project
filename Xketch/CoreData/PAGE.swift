//
//  PAGE.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/2.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class PAGE: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var index: NSNumber
    @NSManaged var preview: NSData
    @NSManaged var location: NSValue
    @NSManaged var project: PROJECT
    @NSManaged var widgets: NSSet
    @NSManaged var actions: NSSet
    
}
