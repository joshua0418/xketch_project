//
//  XKCANVAS.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/28.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKCANVAS: Xketch.NATIVE_WIDGET {

    @NSManaged var editable: NSNumber
    @NSManaged var strokeSize: NSNumber
    @NSManaged var strokeColor: AnyObject
    @NSManaged var stylus: NSNumber
    @NSManaged var paths: NSSet?

    func updateAttributes() {
//        attributes.append("strokeSize")
//        attributes.append("strokeColor")
//        attributes.append("stylus")
//        attributes.append("editable")
    }
}
