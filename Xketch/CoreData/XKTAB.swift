//
//  XKTAB.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/7.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKTAB: Xketch.NATIVE_WIDGET {

    @NSManaged var itemCount: NSNumber
    @NSManaged var selected: NSNumber
    @NSManaged var items: NSSet

    func updateAttributes() {
//        attributes.append("itemCount")
        attributes.append("selected")
        attributes.append("items")
    }
}
