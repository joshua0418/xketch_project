//
//  XKTEXTVIEW.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKTEXTVIEW: Xketch.NATIVE_WIDGET {

    @NSManaged var alignment: NSNumber
    @NSManaged var text: String

    func updateAttributes() {
        attributes.append("alignment")
        attributes.append("text")
    }
}
