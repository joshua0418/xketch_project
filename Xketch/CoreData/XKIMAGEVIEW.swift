//
//  XKIMAGEVIEW.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKIMAGEVIEW: Xketch.NATIVE_WIDGET {

    @NSManaged var style: NSNumber
    @NSManaged var contentMode: NSNumber
    @NSManaged var image: NSSet
    @NSManaged var imageSize: NSValue
    @NSManaged var blur: NSNumber

    func updateAttributes() {
        attributes.append("style")
        attributes.append("image")
        attributes.append("contentMode")
        attributes.append("blur")
    }
}
