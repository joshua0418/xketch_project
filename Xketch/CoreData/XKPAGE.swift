//
//  XKPAGE.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKPAGE: Xketch.NATIVE_WIDGET {

    @NSManaged var currentPageColor: AnyObject
    @NSManaged var pageCount: NSNumber
    @NSManaged var pagesColor: AnyObject

    func updateAttributes() {
        attributes.append("currentPageColor")
        attributes.append("pageCount")
        attributes.append("pagesColor")
    }
}
