//
//  XKNAV_ITEM.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/7.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKNAV_ITEM: Xketch.NATIVE_WIDGET {

    @NSManaged var style: NSNumber
    @NSManaged var side: String
    @NSManaged var text: String
    @NSManaged var image: NSData
    @NSManaged var nav: XKNAV

    func updateAttributes() {
        attributes.removeAll(keepCapacity: false)
        attributes.append("style")
        attributes.append("text")
        attributes.append("image")
    }
}
