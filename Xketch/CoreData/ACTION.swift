//
//  ACTION.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/3/2.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class ACTION: NSManagedObject {

    @NSManaged var event: NSNumber
    @NSManaged var targetPage: PAGE
    @NSManaged var type: NSNumber
    @NSManaged var triggerWidget: NATIVE_WIDGET

}
