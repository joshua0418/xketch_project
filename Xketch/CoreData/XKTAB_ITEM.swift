//
//  XKTAB_ITEM.swift
//  Xketch
//
//  Created by Toby Hsu on 2015/1/7.
//  Copyright (c) 2015年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKTAB_ITEM: Xketch.NATIVE_WIDGET {

    @NSManaged var index: NSNumber
    @NSManaged var style: NSNumber
    @NSManaged var text: String
    @NSManaged var image: NSData
    @NSManaged var tab: XKTAB

    func updateAttributes() {
        attributes.removeAll(keepCapacity: false)
        attributes.append("style")
        attributes.append("text")
        attributes.append("image")
    }
}
