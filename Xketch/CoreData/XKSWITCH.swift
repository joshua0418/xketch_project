//
//  XKSWITCH.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/18.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKSWITCH: Xketch.NATIVE_WIDGET {

    @NSManaged var state: NSNumber
    @NSManaged var tintColor: AnyObject

    func updateAttributes() {
        attributes = attributes.filter({$0 != "fontColor"})
        attributes.append("state")
        attributes.append("tintColor")
    }
}
