//
//  XKSLIDER.swift
//  Xketch
//
//  Created by Toby Hsu on 2014/12/2.
//  Copyright (c) 2014年 Toby Hsu. All rights reserved.
//

import Foundation
import CoreData

class XKSLIDER: NATIVE_WIDGET {

    @NSManaged var currentValue: NSNumber
    @NSManaged var maxTrackColor: AnyObject
    @NSManaged var minTrackColor: AnyObject
    @NSManaged var thumbColor: AnyObject

    func updateAttributes() {
        attributes = attributes.filter({$0 != "fontColor"})
        attributes.append("currentValue")
        attributes.append("maxTrackColor")
        attributes.append("minTrackColor")
//        attributes.append("thumbColor")
    }
}
